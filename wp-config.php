<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/var/www/sandinh.io/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'sdio');

/** MySQL database username */
define('DB_USER', 'sdio');

/** MySQL database password */
define('DB_PASSWORD', 'Sd!0');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_RggM}*}b)PpEL:!_-xd(l1?{fepL)*EAgcp)/Q;Zw-n8z |Nf1!N=M&S]W]jk`I');
define('SECURE_AUTH_KEY',  'a;N);x#%o]*`%^kW,I)iMz.Wb<CZRB Ekx%8=H2 mKmVFzYi}WwWp` )::DyJoKP');
define('LOGGED_IN_KEY',    'a<OZK,8piazu=$RN3I{[VG_7YSX+;-wm*1OgNahq03)rvN;J8+$}#3(x@-dlL`_+');
define('NONCE_KEY',        '$Z(%.V]f.VQ{l~Z|~.7ueK:+;{Ypv)#-|@P?mO0NVU]69T54F9sO/S-8P 6J(yu:');
define('AUTH_SALT',        'ap#J+g5zB,ff@|H=-RYJQ$!rmAQ4HLP[Bj/6ZD>!Y#M*B$dk{_95vm-!|qU#w)^:');
define('SECURE_AUTH_SALT', '/dD!NnI<BN2ZMBxG=RIMFMh+1S8(Pvg-0LiI$Wy,Ew3><)C_LP2|{pL~+Dxx,JUS');
define('LOGGED_IN_SALT',   'pQ,5N&PK@7WEzA.pk5~4[,{GqKUL+1!vsj-iG[br{v]Et9DbC!oCN)+d?c-Nkp){');
define('NONCE_SALT',       'R./79vlj.G;E2Um&{&j){|}tZj{7%`5y-{|9=K6{Rmb6=SLz:`I9no~8sz--%c!=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'fbk77w_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
