<?php

/**
 * This file is loaded in addition to the default functions.php
 * file, immediately before it.
 *
 * Use this file to adjust the functionality of Camyno to suit your needs.
 * nearly all of the functions used in Camyno can be overwritten simply by
 * defining them within this file.
 */

/**
 * Load the child themes stylesheet
 * comment this out or remove it altogether
 * to prevent the stylesheet from loading
 */
function cv_load_child_theme_stylesheet() {
   wp_enqueue_style( 'camyno-child-theme', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'cv_load_child_theme_stylesheet', 99 );

