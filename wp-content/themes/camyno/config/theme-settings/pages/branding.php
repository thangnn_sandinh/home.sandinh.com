<?php

if ( ! class_exists('CV_Branding_Settings') ) :

/**
 * Branding settings page
 *
 * @package    WordPress
 * @subpackage Canvys
 * @since      Version 1.0
 */
class CV_Branding_Settings extends CV_Settings_Page {

   /**
    * Function to create page configuration
    *
    * @return void
    */
   public function __construct() {

      global $canvys;

      $this->config = array(
         'tab_title' => __( 'Branding', 'canvys' ),
         'slug' => 'branding',
         'priority' => 40,
         'defaults' => array(
            'favicon' => null,
            'enable_touch_support' => false,
            'touch_icon' => null,
            'enable_metro_support' => false,
            'metro_color' => '#000000',
            'metro_icon' => null,
            'description' => null,
            'keywords' => null,
         ),
      );

   }

   /**
    * Loading additional scripts to the settings page
    *
    * @return void
    */
   public function additional_scripts() { ?>
      <script id="cv-theme-settings-visual-script">
         (function($) {
            $(document).ready( function() {

               $('#branding-enable_touch_support').on( 'change', function() {
                  var $this = $(this), $controlsWrap = $('#cv-branding-touch-controls-wrap');
                  if ( $this.prop('checked') ) {
                     $controlsWrap.slideDown();
                  }
                  else {
                     $controlsWrap.slideUp();
                  }
               }).trigger('change');

               $('#branding-enable_metro_support').on( 'change', function() {
                  var $this = $(this), $controlsWrap = $('#cv-branding-metro-controls-wrap');
                  if ( $this.prop('checked') ) {
                     $controlsWrap.slideDown();
                  }
                  else {
                     $controlsWrap.slideUp();
                  }
               }).trigger('change');

            });
         })(jQuery);
      </script>
   <?php }

   /**
    * Rendering the inner page
    *
    * @param array $input The user specified input
    * @return void
    */
   public function render_inner_page( $input ) {
      $name = 'cv_theme_settings[' . $this->config['slug'] . ']';
      $input = $this->extract_input( $input ); ?>

      <div class="option-wrap">
         <label for="branding-favicon" class="option-title"><?php _e( 'Favicon', 'canvys' ); ?></label>
         <input type="text" class="widefat cv-image-with-preview" style="max-width:200px;" id="branding-favicon" value="<?php echo $input['favicon']; ?>" name="<?php echo $name; ?>[favicon]" />
         <a class="button cv-select-image"><?php _e( 'Select Image', 'canvys' ); ?></a>
         <p class="option-description"><?php _e( 'Website general Favicon, upload a 16x16 PNG or ICO file to be used as your websites favicon.', 'canvys' ); ?></p>

         <div class="option-spacer"></div>

         <p><label for="branding-enable_touch_support">
            <input type="checkbox" id="branding-enable_touch_support" value="1" <?php checked( $input['enable_touch_support'] ); ?> name="<?php echo $name; ?>[enable_touch_support]" />
            <span><?php _e( 'Enable touch icon support for iOS 2.0+ and Android 2.1+', 'canvys' ); ?></span>
         </label></p>

         <p><label for="branding-enable_metro_support">
            <input type="checkbox" id="branding-enable_metro_support" value="1" <?php checked( $input['enable_metro_support'] ); ?> name="<?php echo $name; ?>[enable_metro_support]" />
            <span><?php _e( 'Enable IE 10 + Metro tile icon support', 'canvys' ); ?></span>
         </label></p>

      </div>

      <div class="option-wrap" id="cv-branding-touch-controls-wrap">
         <label for="branding-touch_icon" class="option-title"><?php _e( 'Touch Icon support for iOS 2.0+ & Android 2.1+', 'canvys' ); ?></label>
         <input type="text" class="widefat cv-image-with-preview" style="max-width:200px;" id="branding-touch_icon" value="<?php echo $input['touch_icon']; ?>" name="<?php echo $name; ?>[touch_icon]" />
         <a class="button cv-select-image"><?php _e( 'Select Image', 'canvys' ); ?></a>
         <p class="option-description"><?php _e( 'Select an image with dimensions of exactly 152px X 152px, the file type also must be ".png".', 'canvys' ); ?></p>
      </div>

      <div class="option-wrap" id="cv-branding-metro-controls-wrap">
         <label class="option-title" for="branding-metro_color"><?php _e( 'IE 10 + Metro Tile Color', 'canvys' ); ?></label>
         <input type="text" class="cv-color-picker" id="branding-metro_color" value="<?php echo $input['metro_color']; ?>" name="<?php echo $name; ?>[metro_color]" />
         <p class="option-description"><?php _e( 'Select the color to be used for the metro tile for your website.', 'canvys' ); ?></p>
         <div class="option-spacer"></div>
         <label for="branding-metro_icon" class="option-title"><?php _e( 'IE 10 + Metro Tile Icon', 'canvys' ); ?></label>
         <input type="text" class="widefat cv-image-with-preview" style="max-width:200px;" id="branding-metro_icon" value="<?php echo $input['metro_icon']; ?>" name="<?php echo $name; ?>[metro_icon]" />
         <a class="button cv-select-image"><?php _e( 'Select Image', 'canvys' ); ?></a>
         <p class="option-description"><?php _e( 'Select an image with dimensions of exactly 144px X 144px, the file type also must be ".png".', 'canvys' ); ?></p>
      </div>

      <div class="option-wrap">
         <label for="branding-description" class="option-title"><?php _e( 'Website Description', 'canvys' ); ?></label>
         <textarea class="widefat" rows="8" id="branding-description" name="<?php echo $name; ?>[description]"><?php echo $input['description']; ?></textarea>
         <p class="option-description"><?php _e( 'Short description of your website and its content, used by search engines.', 'canvys' ); ?></p>
      </div>

      <div class="option-wrap">
         <label for="branding-keywords" class="option-title"><?php _e( 'Website Keywords', 'canvys' ); ?></label>
         <textarea class="widefat" rows="8" id="branding-keywords" name="<?php echo $name; ?>[keywords]"><?php echo $input['keywords']; ?></textarea>
         <p class="option-description"><?php _e( 'Comma delimited list of keywords describing your website, used by search engines.', 'canvys' ); ?></p>
      </div>

   <?php }

   /**
    * Sanitizing the page specific input
    *
    * @param array $input The user specified input
    * @return array
    */
   public static function sanitize_input( $input ) {
      return array(
         'favicon'               => isset( $input['favicon'] ) ? cv_filter( $input['favicon'], 'url' ) : null,
         'enable_touch_support'  => isset( $input['enable_touch_support'] ) && $input['enable_touch_support'] ? true : false,
         'touch_icon'            => isset( $input['touch_icon'] ) ? cv_filter( $input['touch_icon'], 'url' ) : null,
         'enable_metro_support'  => isset( $input['enable_metro_support'] ) && $input['enable_metro_support'] ? true : false,
         'metro_icon'            => isset( $input['metro_icon'] ) ? cv_filter( $input['metro_icon'], 'url' ) : null,
         'metro_color'           => isset( $input['metro_color'] ) ? cv_filter( $input['metro_color'], 'hex' ) : '#000000',
         'description'           => isset( $input['description'] ) ? cv_filter( $input['description'], 'text' ) : null,
         'keywords'              => isset( $input['keywords'] ) ? cv_filter( $input['keywords'], 'text' ) : null,
      );
   }

}
endif;