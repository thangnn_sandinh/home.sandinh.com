<?php

if ( ! class_exists('CV_Advanced_Settings') ) :

/**
 * Advanced settings page
 *
 * @package    WordPress
 * @subpackage Canvys
 * @since      Version 1.0
 */
class CV_Advanced_Settings extends CV_Settings_Page {

   /**
    * Function to create page configuration
    *
    * @return void
    */
   public function __construct() {

      global $canvys;

      $this->config = array(
         'tab_title' => __( 'Adanced', 'canvys' ),
         'slug' => 'advanced',
         'priority' => 100,
         'defaults' => array(
            'css'    => null,
            'js'     => null,
            'header' => null,
         ),
      );

   }

   /**
    * Loading additional styles to the settings page
    *
    * @return void
    */
   public function additional_styles() { ?>
      <style id="cv-theme-settings-advanced-style">
         .css-selector {
            font-family: 'Oxygen Mono';
            background: rgba(0,0,0,0.025);
            border-radius: 3px;
            padding: 5px;
         }
      </style>
   <?php }

   /**
    * Rendering the inner page
    *
    * @param array $input The user specified input
    * @return void
    */
   public function render_inner_page( $input ) {
      $name = 'cv_theme_settings[' . $this->config['slug'] . ']';
      $input = $this->extract_input( $input ); ?>

      <!-- <div class="option-wrap">
         <strong for="advanced-css" class="option-title" style="margin-bottom:0;cursor:pointer;">
            <i class="icon-plus"></i> <?php _e( 'Notable CSS Selectors', 'canvys' ); ?>
         </strong>
         <p><strong><?php _e( 'Outer Container:', 'canvys' ); ?></strong> <span class="css-selector">#container</span></p>
         <p><strong><?php _e( 'Inner Containers:', 'canvys' ); ?></strong> <span class="css-selector">.wrap</span></p>
         <p><strong><?php _e( 'Header:', 'canvys' ); ?></strong> <span class="css-selector">#header</span></p>
         <p><strong><?php _e( 'Banner:', 'canvys' ); ?></strong> <span class="css-selector">#top-banner</span></p>
         <p><strong><?php _e( 'Footer:', 'canvys' ); ?></strong> <span class="css-selector">#footer</span></p>
         <p><strong><?php _e( 'Socket:', 'canvys' ); ?></strong> <span class="css-selector">#socket</span></p>
         <div class="option-spacer"></div>
         <p><strong><?php _e( 'Main Menu Top Level Item', 'canvys' ); ?></strong></p>
         <p><em>#header .primary-menu > li</em></p>
      </div> -->

      <div class="option-wrap">
         <label for="advanced-css" class="option-title"><?php _e( 'Quick CSS', 'canvys' ); ?></label>
         <textarea class="monospace-font widefat" rows="8" id="advanced-css" name="<?php echo $name; ?>[css]"><?php echo $input['css']; ?></textarea>
         <p class="option-description"><?php printf( __( 'CSS will be added to the %s section of every page.', 'canvys' ), '&lt;head /&gt;' ); ?></p>
      </div>

      <div class="option-wrap">
         <label for="advanced-js" class="option-title"><?php _e( 'Quick JavaScript', 'canvys' ); ?></label>
         <textarea class="monospace-font widefat" rows="8" id="advanced-js" name="<?php echo $name; ?>[js]"><?php echo $input['js']; ?></textarea>
         <p class="option-description"><?php printf( __( 'JavaScript will be added immediately before the closing %s tag.', 'canvys' ), '&lt;body /&gt;' ); ?></p>
      </div>

      <div class="option-wrap">
         <label for="advanced-header" class="option-title"><?php _e( 'Additional Header HTML', 'canvys' ); ?></label>
         <textarea class="monospace-font widefat" rows="8" id="advanced-header" name="<?php echo $name; ?>[header]"><?php echo $input['header']; ?></textarea>
         <p class="option-description"><?php printf( __( 'Add any additional HTML to the %s section of every page.', 'canvys' ), '&lt;head /&gt;' ); ?></p>
      </div>

   <?php }

   /**
    * Sanitizing the page specific input
    *
    * @param array $input The user specified input
    * @return array
    */
   public static function sanitize_input( $input ) {
      $allowed_html = array(
         'script' => array(
            'id' => array(),
            'class' => array(),
         ),
         'style' => array(
            'id' => array(),
            'class' => array(),
         ),
      );
      return array(
        'css'    => isset( $input['css'] ) ? stripslashes( $input['css'] ) : null,
        'js'     => isset( $input['js'] ) ? stripslashes( $input['js'] ) : null,
        'header' => isset( $input['header'] ) ? $input['header'] : null,
      );
   }

}
endif;