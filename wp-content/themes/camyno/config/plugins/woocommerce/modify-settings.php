<?php

global $canvys;

/* ===================================================================== *
 * Remove default settings
 * ===================================================================== */

if ( ! function_exists( 'cv_woocommerce_filter_settings' ) ) :

$wc_settings_pages = array(
   'general', 'page', 'catalog', 'inventory', 'shipping', 'tax', 'product',
);

foreach ( $wc_settings_pages as $page ) {
   add_filter( "woocommerce_{$page}_settings", 'cv_woocommerce_filter_settings' );
}

/**
 * Function to remove options from the WooCommerce settings pages
 *
 * @param array $options Existing options array
 * @return array
 */
function cv_woocommerce_filter_settings( $options ) {

   $remove = array( 'woocommerce_enable_lightbox', 'woocommerce_frontend_css' );

   foreach ( $options as $key => $option ) {

      if ( isset( $option['id'] ) && in_array( $option['id'], $remove ) ) {
         unset( $options[$key] );
      }

   }

   return $options;

}
endif;