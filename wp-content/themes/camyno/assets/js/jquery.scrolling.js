(function($) {
   "use strict";

   var $document = $(document),
       $window = $(window),
       $headerMarker = document.getElementById('header-marker') ? $('#header-marker') : false,
       $header = document.getElementById('header') ? $('#header') : false,
       $logo = $header ? $header.find('#header-logo') : false;

   /* ===================================================================== *
    * Utility Functions
    * ===================================================================== */

   function CVScrollingEnabled() {
      if ( $('body').hasClass('not-responsive') ) return true;
      return $window.width() >= 640;
   }

   function CVHeaderHeight() {
      var headerHeight = -1;
      if ( CVScrollingEnabled() && $headerMarker ) {
         if ( $headerMarker.hasClass('is-collapsing') ) {
            headerHeight = 57;
         }
         else {
            headerHeight = 85;
         }
      }
      return headerHeight;
   }

   /* ===================================================================== *
    * Sticky Header
    * ===================================================================== */

   var CVStickyHeader = function() {
      this.init();
      return this;
   };

   CVStickyHeader.prototype = {

      init: function() {

         // Make sure sticky header has been enabled
         if ( ! $headerMarker || ! $header ) { return; }

         var self = this;
         self.toggle();
         $window.resize( $.proxy( self.toggle, self ) );

      },

      toggle: function() {
         if ( CVScrollingEnabled() ) {
            if ( ! $document.data( 'CVStickyHeader' ) ) {
               $document.data( 'CVStickyHeader', true );
               this.create();
            }
         }
         else if ( $document.data( 'CVStickyHeader' ) ) {
            $document.data( 'CVStickyHeader', false );
            this.destroy();
         }
      },

      create: function() {
         $headerMarker.waypoint( function( direction ) {
            switch ( direction ) {
               case 'down':
                  $header.addClass('is-stuck');
                  $headerMarker.css( 'height', $header.outerHeight() );
                  if ( $headerMarker.hasClass('transparency-active') ) {
                     $headerMarker.css( 'margin-bottom', -$header.outerHeight() );
                  }
                  break;
               case 'up':
                  $header.removeClass('is-stuck');
                  $headerMarker.css( 'height', '0px' );
                  if ( $headerMarker.hasClass('transparency-active') ) {
                     $headerMarker.css( 'margin-bottom', '0px' );
                  }
                  break;
            }
         });
      },

      destroy: function() {
         $headerMarker.waypoint('destroy');
         $header.removeClass('is-stuck');
         $headerMarker.css( 'height', '0px' );
         $headerMarker.css( 'margin-bottom', '0px' );
      },

   };

   /* ===================================================================== *
    * Collapsing Header
    * ===================================================================== */

   var CVCollapsingHeader = function() {
      this.init();
      return this;
   };

   CVCollapsingHeader.prototype = {

      init: function() {

         // Make sure collapsing header has been enabled
         if ( ! $headerMarker || ! $headerMarker.hasClass('is-collapsing') ) { return; }

         var self = this;
         self.toggle();
         $window.resize( $.proxy( self.toggle, self ) );

      },

      toggle: function() {
         if ( CVScrollingEnabled() ) {
            if ( ! $document.data( 'CVCollapsingHeader' ) ) {
               $document.data( 'CVCollapsingHeader', true );
               this.create();
            }
         }
         else if ( $document.data( 'CVCollapsingHeader' ) ) {
            $document.data( 'CVCollapsingHeader', false );
            this.destroy();
         }
      },

      create: function() {
         var self = this;
         $window.on( 'scroll', $.proxy( self.calculateHeight, self ) );
      },

      destroy: function() {
         var self = this;
         $window.off( 'scroll', $.proxy( self.calculateHeight, self ) );
      },

      calculateHeight: function() {
         var self = this,
             scrollTop = document.documentElement.scrollTop || document.body.scrollTop,
             topOffset = $headerMarker.offset().top,
             originalHeight = 85;

         // Make sure scroll position is not negative
         if ( 0 > scrollTop ) { return; }

         // Check if we`re above the header marker
         if ( scrollTop < topOffset ) {
            if ( originalHeight !== $header.css('height') ) {
               self.setHeight( originalHeight );
            }
            return;
         }

         var headerMarkerHeight = $headerMarker.outerHeight(),
             distance = scrollTop > (topOffset+(headerMarkerHeight/3)) ? headerMarkerHeight/3 : scrollTop - topOffset,
             percent = 1 - ( distance / headerMarkerHeight );

         // Apply the height
         self.setHeight( originalHeight*percent );

      },

      setHeight: function( height ) {
         $header.css({
            height: height+'px',
            lineHeight: height+'px',
         });
         $logo.css({
            height: height+'px',
         });
      }

   };

   /* ===================================================================== *
    * Transparent Header
    * ===================================================================== */

   var CVTransparentHeader = function() {
      this.init();
      return this;
   };

   CVTransparentHeader.prototype = {

      init: function() {

         // Make sure header transparency has been enabled
         if ( ! $headerMarker || ! $headerMarker.hasClass('transparency-active') ) { return; }

         var self = this;
         self.toggle();
         $window.resize( $.proxy( self.toggle, self ) );
      },

      toggle: function() {
         if ( CVScrollingEnabled() ) {
            if ( ! $document.data( 'CVTransparentHeader' ) ) {
               $document.data( 'CVTransparentHeader', true );
               this.create();
            }
         }
         else if ( $document.data( 'CVTransparentHeader' ) ) {
            $document.data( 'CVTransparentHeader', false );
            this.destroy();
         }
      },

      create: function() {
         $headerMarker.waypoint( function( direction ) {
            switch ( direction ) {
               case 'down':
                  $header.removeClass('is-transparent');
                  break;
               case 'up':
                  $header.addClass('is-transparent');
                  break;
            }
         }, { offset: -5 });
      },

      destroy: function() {
         $headerMarker.waypoint('destroy');
         $header.addClass('is-transparent');
      },

   };

   /* ===================================================================== *
    * Parallax Scrolling
    * ===================================================================== */

   var CVParallaxScrolling = function() {
      $window.resize( function() {
         if ( CVScrollingEnabled() && $window.width() >= 1100 ) {
            $('.cv-parallax-content').each( function() {
               var $this = $(this), $content = $this.children().eq(0),
                   offset = 70 > $this.offset().top ? 0 : CVHeaderHeight();
               $window.on( 'scroll', this, function() {
                  var scrollTop = ( document.documentElement.scrollTop || document.body.scrollTop ) + offset,
                      thisHeight = $this.outerHeight(),
                      topOffset = $this.offset().top,
                      bottomOffset = topOffset + thisHeight;
                  if ( ! ( scrollTop > topOffset && scrollTop < bottomOffset ) ) {
                     if ( 1 !== $content.css('opacity') ) {
                        $content.css({ opacity: 1, y: '0px' });
                     }
                     return;
                  }
                  var distance = scrollTop - topOffset,
                      opacity = 1 - ( distance / thisHeight ),
                      translateY = distance / 2 + 'px';
                  $content.css({ opacity: opacity, y: translateY });
               });
            });
         }
      }).trigger('resize');
   };

   /* ===================================================================== *
    * Sticky Menu
    * ===================================================================== */

   var CVStickyMenu = function() {
      this.init();
      return this;
   };

   CVStickyMenu.prototype = {

      init: function() {

         // Make sure there is a sticky menu on the page
         if ( ! document.getElementById('cv-sticky-nav-marker') ) { return; }

         var self = this;
         self.toggle();
         $window.resize( $.proxy( self.toggle, self ) );

         // Make sure sticky menu width does not exceed layout
         if ( ! $('body').hasClass('container-layout-free') ) {
            $window.resize( function() {
               var width = $('.wrap-all').width();
               $('#cv-sticky-nav').css( 'width', width );
            }).trigger('resize');
         }

      },

      toggle: function() {
         if ( CVScrollingEnabled() ) {
            if ( ! $document.data( 'CVStickyMenu' ) ) {
               $document.data( 'CVStickyMenu', true );
               this.create();
            }
         }
         else if ( $document.data( 'CVStickyMenu' ) ) {
            $document.data( 'CVStickyMenu', false );
            this.destroy();
         }
      },

      create: function() {

         var self = this,
             $stickyMarker = $('#cv-sticky-nav-marker'),
             $stickyNav = $('#cv-sticky-nav');

         // Activate sticky menu waypoint
         $stickyMarker.waypoint( function( direction ) {
            switch ( direction ) {
               case 'down':
                  if ( $header && $header.hasClass('is-stuck') ) {
                     $header.addClass('sticky-menu-active');
                  }
                  $stickyNav.addClass('is-stuck').css('margin-top', CVHeaderHeight()+1 );
                  $stickyMarker.css( 'height', $stickyNav.outerHeight() );
                  break;
               case 'up':
                  if ( $header ) {
                     $header.removeClass('sticky-menu-active');
                  }
                  $stickyNav.removeClass('is-stuck').css('margin-top', '0px' );
                  $stickyMarker.css( 'height', '0px' );
                  break;
            }
         }, { offset: CVHeaderHeight() } );

         // Activate scrollspy
         var menuHeight = $stickyNav.outerHeight() + CVHeaderHeight();
         $('#body').children().waypoint( function( direction ) {
            if ( 'down' === direction ) {
               self.scrollSpy( $(this) );
            }
         }, {
            offset: menuHeight,
         });
         $('#body').children().waypoint( function( direction ) {
            if ( 'up' === direction ) {
               self.scrollSpy( $(this) );
            }
         }, {
            offset: function() {
               return - ( $(this).height() - menuHeight );
            },
         });
         $('body').waypoint( function() {
            self.scrollSpy( $(this) );
         }, {
            offset: $header ? -10 : 0,
         });

      },

      destroy: function() {

         // Remove sticky menu waypoint
         var $stickyMarker = $('#cv-sticky-nav-marker'), $stickyNav = $('#cv-sticky-nav');
         $stickyMarker.waypoint('destroy');
         $header.removeClass('sticky-menu-active');
         $stickyNav.removeClass('is-stuck').css('margin-top', '0px' );
         $stickyMarker.css( 'height', '0px' );

         // Remove scrollspy
         $('#body').children().waypoint('destroy');
         $('body').waypoint('destroy');

      },

      scrollSpy: function( $section ) {
         var id = $section.attr('id') ? '#'+$section.attr('id') : null, $stickyNav = $('#cv-sticky-nav');
         if ( id && $stickyNav.find('[href="'+id+'"]').length ) {
            $stickyNav.find('[href="'+id+'"]').addClass('is-active').siblings().removeClass('is-active');
         }
         else {
            $stickyNav.find('a').removeClass('is-active');
         }
      },

   };

   $document.ready( function() {

      // Activate the sticky header
      new CVStickyHeader();

      // Activate the collapsing header
      new CVCollapsingHeader();

      // Activate header transparency
      new CVTransparentHeader();

      // Activate parallax scrolling
      new CVParallaxScrolling();

      // Activate sticky menus
      new CVStickyMenu();

   });

})(jQuery);