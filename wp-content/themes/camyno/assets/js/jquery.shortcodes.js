(function($) {
   "use strict"

   /* ===================================================================== *
    * Utility Functions
    * ===================================================================== */

   function CVSanitize( setting, allowed, standard ) {

      if ( setting === false || setting === true ) {
         return setting;
      }

      var response = false;

      if ( allowed ) {

         $.each( allowed, function( index, value ) {
            if ( setting === value ) {
               response = setting;
            }
         });

         if ( ! response && standard ) {
            response = standard;
         }
         else if ( ! response ) {
            response = allowed[0];
         }

      }
      else {
         response = setting;
      }

      switch ( response ) {
         case 'true':
            response = true;
            break;
         case 'false':
            response = false;
            break;
      }

      return response;

   }

   function CVMakeBool( value ) {
      if ( 'true' === value ) {
         return true;
      }
      return false;
   }

   /* ===================================================================== *
    * Content Sections
    * ===================================================================== */

   var CVContentSection = function( element ) {
      this.$element = $(element);
      this.$innerContainer = this.$element.find('.cv-wrap-wrapper');
      this.init();
      return this;
   };

   CVContentSection.prototype = {

      init: function() {

         var self = this;

         // Check if a background image was supplied
         if ( self.$element.hasClass('is-loading-bg-image') ) {
            var src = self.$element.css('background-image'),
                url = src.match(/\((.*?)\)/)[1].replace(/('|")/g,''),
                img = new Image();
            img.onload = function() { self.$element.removeClass('is-loading-bg-image'); }
            img.src = url;
            if (img.complete) img.onload();
         }

         // Check if section has a scrolling background
         if ( self.$element.attr('data-bg-scrolling') ) {

            // Set initial position
            self.$element.css( 'backgroundPosition', '0px 0px' );

            // Start Background Scrolling
            setTimeout( function() {
               self.$element.addClass('is-animating-background');
               self.startScrollingBG();
            });

         }

         // Do not continue if page sliding is active
         if ( $('html').hasClass('full-page-slider-active') ) {
           self.$element.css('opacity', 1); return;
         }

         // Check if section has min-height attribute
         if ( self.$innerContainer.data('min-height') ) {

            // Set the height initially
            self.setSize();

            // Update height when screen resizes
            $(window).resize( $.proxy( self.setSize, self ) );

         }

         // Check if top padding potentially needs to be adjusted
         if ( document.getElementById('header')
         && $('#header').hasClass('transparency-active') ) {

            // Set the top padding initially
            self.setTopPadding();

            // Update top padding when screen resizes
            $(window).resize( $.proxy( self.setTopPadding, self ) );

         }

         // Check if section has a video background
         if ( self.$element.hasClass('has-video-bg') ) {

            // Mobile check
            if ( navigator.userAgent.match(/(Android|iPod|iPhone|iPad|IEMobile|Opera Mini)/) ) {

               // Reveal image fallback
               if ( self.$element.find('.bg-video-image-fallback').length ) {
                  self.$element.addClass('bg-video-disabled');
                  self.$element.find('.bg-video-image-fallback').css('display','block');
               }

               // Remove the video
               self.$element.find('.bg-video-wrapper').remove();

            }

            // Good to go
            else {

               // Activate the video background
               self.activateVideoBG();

            }

         }

         // Reveal element after loaded
         self.$element.css( 'opacity', 1 );

      },

      setSize: function() {

         var self = this;

         var appliedHeight = $(window).height() * ( parseInt( self.$innerContainer.attr('data-min-height') ) / 100 ),
             headerSize = document.getElementById('header') ? parseInt( $('#header').outerHeight() ) + parseInt( $('#header').offset().top ) : 0,
             elementOffset = self.$element.offset().top;

         if ( elementOffset < ( headerSize + 10 ) ) {
            appliedHeight -= elementOffset;
         }

         // Apply the height
         self.$innerContainer.css( 'height', appliedHeight + 'px' );

      },

      setTopPadding: function() {
         var self = this, headerSize = parseInt( $('#header').outerHeight() );
         self.$innerContainer.css( 'padding-top', '' );
         if ( self.$innerContainer.offset().top < headerSize-5 ) {
            var originalPadding = parseInt( self.$innerContainer.css('padding-top') ),
                headerHeight = parseInt( $('#header').outerHeight() ),
                appliedPadding = originalPadding + headerHeight + 'px';
            self.$innerContainer.css( 'padding-top', appliedPadding );
         }
      },

      activateVideoBG: function() {

         var self = this,
             videoID = self.$element.attr('id')+'-video_bg',
             videoBG = document.getElementById(videoID);

         if ( videoBG.readyState >= videoBG.HAVE_FUTURE_DATA ) {
            self.videoBGLoaded();
         }
         else {
            $(videoBG).on( 'canplay', function() {
               self.videoBGLoaded();
               $(videoBG).off('canplay');
            });
         }

      },

      videoBGLoaded: function() {

         var self = this;

         // Center video initially
         self.scaleVideoBG();

         // Recenter video when screen resizes
         $(window).resize( $.proxy( self.scaleVideoBG, self ) );

         // Fade in the video
         self.$element.addClass('bg-video-loaded');

      },

      scaleVideoBG: function() {

         var self = this,
             $video = self.$element.find('.bg-video'),
             boxWidth = self.$element.outerWidth(),
             boxHeight = self.$element.outerHeight(),
             videoWidth = $video.width(),
             videoHeight = $video.height();

         // Calculate new height and width
         var initW = videoWidth;
         var initH = videoHeight;
         var ratio = initH / initW;

         videoWidth = boxWidth;
         videoHeight = boxWidth * ratio;

         if ( videoHeight < boxHeight ) {
            videoHeight = boxHeight;
            videoWidth = videoHeight / ratio;
         }

         $video.css( 'width', videoWidth+'px' );

      },

      startScrollingBG: function() {

         var self = this, interval,
             direction = self.$element.attr('data-bg-scrolling'),
             speed = self.$element.attr('data-bg-scrolling-speed'),
             x = parseInt( self.$element.css('backgroundPosition').split(' ')[0] ),
             y = parseInt( self.$element.css('backgroundPosition').split(' ')[1] );

         /* Calculate the speed */
         switch (speed) {
            case 'slow': interval = 100; break;
            case 'fast': interval = 500; break;
            case 'very-fast': interval = 1000; break;
            default: interval = 250;
         }

         /* Calculate new position */
         switch (direction) {

            case 'left': x-= interval; break;
            case 'right': x+= interval; break;

            case 'up': y-= interval; break;
            case 'up-left': x-= interval; y-= interval; break;
            case 'up-right': x+= interval; y-= interval; break;

            case 'down': y+= interval; break;
            case 'down-left': x-= interval; y+= interval; break;
            case 'down-right': x+= interval; y+= interval; break;

         }

         self.$element.css( 'background-position', x+'px '+y+'px' );
         setTimeout( $.proxy( self.startScrollingBG, self ), 7500 );

      },

   };

   /* ===================================================================== *
    * Fullwidth Maps
    * ===================================================================== */

   var CVFullwidthMap = function( element ) {
      this.$element = $(element);
      this.init();
      return this;
   };

   CVFullwidthMap.prototype = {

      init: function() {

         var self = this;

         // Do not continue if page sliding is active
         if ( $('html').hasClass('full-page-slider-active') ) { return; }

         // Make sure section has min-height attribute
         if ( ! self.$element.data('height') ) { return; }

         // Set the height initially
         self.setSize();

         // Update height when screen resizes
         $(window).resize( $.proxy( self.setSize, self ) );

         // Reveal the map
         self.$element.css('opacity', 1);

      },

      setSize: function() {

         var self = this;

         var appliedHeight = $(window).height() * parseInt( self.$element.data('height') ) / 100,
             headerSize = document.getElementById('header') ? parseInt( $('#header').outerHeight() + $('#header').offset().top ) : 0,
             elementOffset = self.$element.offset().top;

         if ( elementOffset < headerSize + 10 ) {
            appliedHeight -= elementOffset;
         }

         // Apply the height
         self.$element.css( 'height', appliedHeight + 'px' );

      },

   };

   /* ===================================================================== *
    * Scaling Typography
    * ===================================================================== */

   var CVScalingTypography = function( element ) {
      this.$element = $(element);
      this.init();
      return this;
   };

   CVScalingTypography.prototype = {

      init: function() {

         var self = this;

         // Set the max font size
         self.max        = self.$element.data('max') ? self.$element.data('max') : 40;
         self.min        = self.$element.data('min') ? self.$element.data('min') : 20;
         self.multiplier = self.$element.data('multiplier') ? self.$element.data('multiplier') : 18;

         // Set the font size initially
         self.setSize();

         // Update font size when screen resizes
         self.$element.resize( $.proxy( self.setSize, self ) );

         // Reveal the text
         self.$element.css( 'opacity', 1 );

      },

      setSize: function() {

         var self = this;

         // Apply the font size
         self.$element.css( 'font-size', Math.max( Math.min( self.$element.width() / self.multiplier, parseFloat( self.max ) ), parseFloat( self.min ) ) + 'px' );

      },

   };

   /* ===================================================================== *
    * Media Flags
    * ===================================================================== */

   var CVMediaFlag = function( element ) {
      this.$element = $(element);
      this.init();
      return this;
   };

   CVMediaFlag.prototype = {

      init: function() {

         var self = this;

         self.$columns = self.$element.children();
         self.$innerColumns = self.$element.find('.flag-content-inner, .flag-media-inner');

         if ( ! self.$columns.length ) return;

         // Set the font size initially
         self.setSize();

         // Resize again if content resizes
         self.$innerColumns.resize( $.proxy( self.setSize, self ) );

         // Update font size when screen resizes
         self.$element.resize( $.proxy( self.setSize, self ) );

      },

      setSize: function() {

         var self = this;

         self.$columns.css( 'height', '' );

         if ( 'none' === self.$columns.eq(0).css('float') ) {
            self.$element.css( 'opacity', 1 );
            return;
         }

         var captionHeights = self.$columns.map( function() {
            return $(this).height();
         }).get();

         var maxHeight = Math.max.apply( null, captionHeights );

         self.$columns.css( 'height', maxHeight );

         // Reveal the content
         self.$element.css( 'opacity', 1 );

      },

   };

   /* ===================================================================== *
    * Testimonial Groups
    * ===================================================================== */

   var CVTestimonialGroup = function( element ) {
      this.$element = $(element);
      this.init();
      return this;
   };

   CVTestimonialGroup.prototype = {

      init: function() {

         var self = this;

         if ( self.$element.hasClass('cv-slider') ) {

            // Set the font size initially
            self.matchHeights();

            // Update font size when screen resizes
            $(window).resize( $.proxy( self.matchHeights, self ) );

         }

      },

      matchHeights: function() {

         var self = this,
             $testimonials = self.$element.find('.testimonial-quote');

         // Make sure testimonials are all the same size
         if ( $testimonials.length ) {

            $testimonials.css( 'height', '' );

            var testimonialHeights = $testimonials.map( function() {
               return $(this).height();
            }).get();

            var maxHeight = Math.max.apply( null, testimonialHeights );

            $testimonials.css( 'height', maxHeight );

         }

      },

   };

   /* ===================================================================== *
    * Change Logs
    * ===================================================================== */

   var CVChangeLog = function( element ) {
      this.$element = $(element);
      this.init();
      return this;
   };

   CVChangeLog.prototype = {

      init: function() {

         var self = this;

         if ( self.$element.hasClass('has-update-notes') ) {

            // Attach events
            self.attachEvents();

         }

      },

      attachEvents: function() {

         var self = this,
             $toggle = self.$element.find('.update-notes-toggle'),
             $notes = self.$element.find('.update-notes-wrap');

         $toggle.on( 'click', function() {
            if ( $notes.hasClass('is-open') ) {
               $notes.css( 'height', '0px' );
            }
            else {
               $notes.css( 'height', parseInt( $notes.children().eq(0).outerHeight() ) + 1 );
            }
            $notes.toggleClass('is-open');
         });

      },

   };

   /* ===================================================================== *
    * Promo Boxes
    * ===================================================================== */

   var CVPromoBox = function( element ) {
      this.$element = $(element);
      this.init();
      return this;
   };

   CVPromoBox.prototype = {

      init: function() {

         var self = this;

         // Check if a background image was supplied
         if ( self.$element.hasClass('is-loading-bg-image') ) {
            var src = self.$element.css('background-image'),
                url = src.match(/\((.*?)\)/)[1].replace(/('|")/g,''),
                img = new Image();
            img.onload = function() { self.$element.removeClass('is-loading-bg-image'); }
            img.src = url;
            if (img.complete) img.onload();
         }

         // Set the top padding initially
         self.setTopPadding();

         // Update top padding when screen resizes
         $(window).resize( $.proxy( self.setTopPadding, self ) );

      },

      setTopPadding: function() {
         var self = this;
         self.$element.css( 'padding-top', '' );
         if ( self.$element.hasClass('is-fullwidth')
         &&   document.getElementById('header')
         && $('#header').hasClass('transparency-active')
         && self.$element.offset().top < $('#header').outerHeight() + $('#header').offset().top ) {
            var originalPadding = parseInt( self.$element.css('padding-top') ),
                headerHeight = parseInt( $('#header').outerHeight() ),
                appliedPadding = originalPadding + headerHeight + 'px';
            self.$element.css( 'padding-top', appliedPadding );
         }
      },

   };

   /* ===================================================================== *
    * Toggle Groups
    * ===================================================================== */

   var CVToggleGroup = function( element ) {
      this.$element = $(element);
      this.init();
      return this;
   };

   CVToggleGroup.prototype = {

      init: function() {

         // Create configuration object
         this.config = this.$element.data('config');
         this.config.firstOpen = CVMakeBool( this.config.firstOpen );
         this.config.allowMulti = CVMakeBool( this.config.allowMulti );
         this.config.filtered = this.$element.children('.toggle-filters').length,

         // Toggles object
         this.$toggles = this.$element.children('.toggle');

         // Attach all events
         this.attachEvents();

         var self = this;

         // Open the first toggle
         if ( self.config.firstOpen ) {
            self.$toggles.first().trigger('open-toggle');
         }

      },

      attachEvents: function() {

         var self = this;

         // Closing all toggles
         self.$element.on( 'close-all', function() {
            self.$toggles.trigger('close-toggle');
         });

         // Opening all toggles
         self.$element.on( 'open-all', function() {
            self.$toggles.trigger('open-toggle');
         });

         // Click event
         self.$toggles.children('h3').on( 'click', function() {
            var $this = $(this), $toggle = $this.parent();

            // Make sure the toggle is active
            if ( $toggle.hasClass('is-disabled') ) { return; }

            // Determine what to do
            if ( $toggle.hasClass('is-open') ) {
               $toggle.trigger('close-toggle');
            }
            else {
               $toggle.trigger('open-toggle');
               if ( ! self.config.allowMulti ) {
                  $toggle.siblings().trigger('close-toggle');
               }
            }
         });

         // Opening a toggle
         self.$toggles.on( 'open-toggle', function() {
            self.openToggle( $(this) );
         });

         // Closing a toggle
         self.$toggles.on( 'close-toggle', function() {
            self.closeToggle( $(this) );
         });

         // Filtering toggles
         if ( self.config.filtered ) {
            self.$element.children('.toggle-filters').find('a').on( 'click', function(e) {
               e.preventDefault();
               var $this = $(this), filter = $this.data('filter');
               if ( $this.parent().hasClass('is-active') ) { return; }
               $this.parent().addClass('is-active').siblings().removeClass('is-active');
               self.filterToggles( filter );
            });
         }

      },

      openToggle: function( $toggle ) {
         $toggle.addClass('is-open').children('div').css('height', $toggle.find('.toggle-content').outerHeight() );
      },

      closeToggle: function( $toggle ) {
         $toggle.removeClass('is-open').children('div').removeAttr('style');
      },

      filterToggles: function( filter ) {

         var self = this;

         // Close all toggles
         self.$element.trigger('close-all');

         // Filter the toggles
         setTimeout( function() {
            self.$toggles.each( function() {
               var $toggle = $(this), tags = $toggle.data('tags'), visible = false;
               if ( 'all' === filter ) { $toggle.removeClass('is-disabled'); return; }
               if ( tags ) {
                  tags = tags.split(',');
                  for ( var i=0; i<_.size( tags ); i++ ) {
                     if ( filter === tags[i] ) {
                        visible = true;
                     }
                  }
               }
               if ( visible ) {
                  $toggle.removeClass('is-disabled');
               }
               else {
                  $toggle.addClass('is-disabled');
               }
            });
         }, 250 );

      },

   };

   /* ===================================================================== *
    * Tab Groups
    * ===================================================================== */

   var CVTabGroup = function( element ) {
      this.$element = $(element);
      this.init();
      return this;
   };

   CVTabGroup.prototype = {

      init: function() {
         var self = this;
         self.$tabs = self.$element.children('.tabs').children();
         self.$panes = self.$element.children('.panes').children();
         self.attachEvents();
      },

      attachEvents: function() {
         var self = this;
         self.$tabs.children('a').on( 'click', function() {
            var $this = $(this), $tab = $this.parent(), index = $tab.index();
            if ( $tab.hasClass('is-active') ) { return; }
            self.changeTab( index );
         });
         self.$panes.children('.inner-pane-title').on( 'click', function() {
            var $this = $(this), $tab = $this.parent(), index = $tab.index();
            if ( $tab.hasClass('is-active') ) { return; }
            self.changeTab( index );
         });
      },

      changeTab: function( index ) {
         var self = this;
         self.$tabs.removeClass('is-active').eq(index).addClass('is-active');
         self.$panes.removeClass('is-active').eq(index).addClass('is-active');
      },

   };

   /* ===================================================================== *
    * Contact Forms
    * ===================================================================== */

   var CVContactForm = function( element ) {
      this.$element = $(element);
      this.init();
      return this;
   };

   CVContactForm.prototype = {

      init: function() {
         var self = this;

         // Find all required fields
         self.$fields = self.$element.find('[required]');

         // Attach events
         self.attachEvents();

         // Prevent standard browser validation
         self.$element.attr('novalidate', '');

      },

      attachEvents: function() {

         var self = this, $field;

         // Handle form submissions
         self.$element.on( 'submit', function(e) {

            var hasError = false;

            // Apply the submitted class
            self.$element.addClass( 'was-submitted' );

            self.$fields.each( function() {

               $field = $(this);

               if ( self.hasError( $field ) ) {
                  self.addErrorClass( $field );
                  hasError = true;
               }

            });

            if ( hasError ) {

               // Prevent default submission
               e.preventDefault();

               // Scroll to the form
               $('html, body').stop().animate({
                  scrollTop: (self.$element.offset().top-50)+'px'
               }, 500, 'easeInOutExpo' );

               // Prevent submission via AJAX
               return false;
            }

         });

         // Validate fields as they are edited
         self.$fields.on( 'keyup blur change', function() {

            // Make sure form has been submitted before
            if ( ! self.$element.hasClass('was-submitted') ) { return; }

            var $field = $(this);

            if ( self.hasError( $field ) ) {
               self.addErrorClass( $field );
            }
            else {
               self.removeErrorClass( $field );
            }

         });

         // Clearing the form
         self.$element.find('.clear-form').on( 'click', $.proxy( self.clearForm, self ) );

      },

      hasError: function( $field ) {

         var error = false,
             value = $field.val(),
             pattern;

         switch ( $field.attr('type') ) {

            case 'checkbox':
               if ( ! $field.prop('checked') ) { error = true; }
               break;

            case 'email':
               if ( ! value ) { error = true; }
               else {
                  var pattern = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/; // "
                  if ( ! pattern.test( value ) ) { error = true; }
               }
               break;

            case 'number':
               if ( ! value || ! $.isNumeric( value ) ) { error = true; }
               break;

            default:
               if ( ! value ) { error = true; }
               break;

         }

         return error;

      },

      clearForm: function() {

         var self = this;

         self.$element.removeClass('was-submitted').find('.has-error').removeClass('has-error');

         // Clear text inputs
         self.$element.find('input:not([type="checkbox"]), textarea').val('').trigger('change');

         // Set select boxes to firs option
         self.$element.find('select').each( function() {
            var $this = $(this);
            $this.val( $this.find('option').first().val() ).trigger('change');
         });

         // Return checkboxes to defaults
         self.$element.find('input[type="checkbox"]').each( function() {
            var $this = $(this), checked = 'checked' == $this.data('default') ? true : false;
            $this.prop( 'checked', checked ).trigger('change');
         });

      },

      addErrorClass: function( $field ) {
         $field.closest('.cv-field').addClass('has-error');
      },

      removeErrorClass: function( $field ) {
         $field.closest('.cv-field').removeClass('has-error');
      },

   };

   /* ===================================================================== *
    * Animated Numbers
    * ===================================================================== */

   var CVAnimatedNumber = function( element ) {
      var $element = $(element),
          number = $element.data('number');
      $element.find('.number-container').on( 'entrance', function() {
         $element.find('.odometer').html(number);
      });
   };

   /* ===================================================================== *
    * Progress Bars
    * ===================================================================== */

   var CVProgressBars = function( element ) {
      var $element = $(element);
      if ( ! $element.hasClass('has-animation') ) { return; }
      $element.find('.task-progress').on( 'entrance', function() {
         $(this).addClass('is-visible');
      });
   };

   /* ===================================================================== *
    * Buttons
    * ===================================================================== */

   var CVButton = function( element ) {
      this.$element = $(element);
      this.init();
      return this;
   };

   CVButton.prototype = {

      init: function() {
         var self = this;
         self.$element.addClass('js-color');
         self.color = self.$element.data('color');
         self.setStyles();
      },

      setStyles: function() {
         var self = this;

         /* Ghost Style */
         if ( self.$element.hasClass('is-ghost') ) {
            self.$element.on({
               mouseleave: function() {
                  self.$element.css({
                     borderColor: 'rgba(' + self.hexToRGB( self.color ) + ',0.75)',
                     backgroundColor: 'transparent',
                     color: self.color,
                  });
               },
               mouseenter: function() {
                  self.$element.css({
                     borderColor: self.color,
                     backgroundColor: 'rgba(' + self.hexToRGB( self.color ) + ',0.15)',
                     color: self.color,
                  });
               },
            }).trigger('mouseleave');
         }

         /* Glassy Style */
         else if ( self.$element.hasClass('is-glassy') ) {
            self.$element.on({
               mouseleave: function() {
                  self.$element.css({
                     borderColor: 'rgba(' + self.hexToRGB( self.color ) + ',0.25)',
                     backgroundColor: 'transparent',
                     color: self.color,
                  });
               },
               mouseenter: function() {
                  self.$element.css({
                     borderColor: 'rgba(' + self.hexToRGB( self.color ) + ',0.75)',
                     backgroundColor: 'rgba(' + self.hexToRGB( self.color ) + ',0.15)',
                     color: self.color,
                  });
               },
            }).trigger('mouseleave');
         }

         /* Filled Style */
         else {
            self.$element.on({
               mouseleave: function() {
                  self.$element.css({
                     borderColor: self.color,
                     backgroundColor: self.color,
                  });
               },
               mouseenter: function() {
                  self.$element.css({
                     borderColor: 'rgba(' + self.hexToRGB( self.color ) + ',0.85)',
                     backgroundColor: 'rgba(' + self.hexToRGB( self.color ) + ',0.75)',
                  });
               },
            }).trigger('mouseleave');
         }
      },

      hexToRGB: function( hex ) {
         var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
         hex = hex.replace(shorthandRegex, function(m, r, g, b) {
            return r + r + g + g + b + b;
         });
         var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
         return result ? parseInt(result[1], 16)+','+parseInt(result[2], 16)+','+parseInt(result[3], 16) : null;
      },

   };

   /* ===================================================================== *
    * Sliders
    * ===================================================================== */

   var CVSlider = function( element ) {
      this.$element = $(element);
      this.init();
      return this;
   };

   CVSlider.prototype = {

      init: function() {
         var self = this;

         if ( self.$element.hasClass( 'cv-fullwidth-slider') ) {

            // Set the height initially
            self.setupFullWidthSlider();

            // Update height when screen resizes
            $(window).resize( $.proxy( self.setupFullWidthSlider, self ) );

            // vertically center content
            if ( self.$element.attr('data-min-height') ) {
               self.$element.find('.cv-wrap-wrapper').addClass('v-align-middle');
            }

         }

         // Activate the slider
         self.activateSlider();

      },

      setOptions: function() {
         var self = this,
             config = self.$element.data('slider');

         var options = {
            arrows: self.$element.attr('data-controls') ? CVMakeBool( self.$element.attr('data-controls') ) : true,
            dots: self.$element.attr('data-pager') ? CVMakeBool( self.$element.attr('data-pager') ) : true,
            autoplay: self.$element.attr('data-auto') ? CVMakeBool( self.$element.attr('data-auto') ) : false,
            autoplaySpeed: self.$element.attr('data-delay') ? self.$element.attr('data-delay') : 4000,
            slide: self.$element.attr('data-slide-tag') ? self.$element.attr('data-slide-tag') : 'div',
            rtl: $('html').is('[dir="rtl"]'),
         }

         var mode = self.$element.attr('data-mode') ? self.$element.attr('data-mode') : 'fade';

         switch ( mode ) {
            case 'vertical': options.vertical = true; break;
            case 'fade': options.fade = true; break;
         }

         if ( self.$element.hasClass( 'cv-fullwidth-slider') ) {
            options.onAfterChange = function() {

               var $slide = self.$element.find('.slick-slide.slick-active');

               // Reset complete entrances
               $slide.siblings().find('[data-completed-entrance]').trigger('reset-entrance');

               // Trigger active animations
               $slide.find('[data-manual-trigger]').trigger('entrance');

            };
            options.onInit = function() {
               setTimeout( function() {
                  self.$element.find('[data-manual-trigger]').trigger('entrance');
               }, 250 );
            };
         }

         return options;
      },

      activateSlider: function() {
         var self = this;
         self.$element.slick( self.setOptions() );
      },

      setupFullWidthSlider: function() {

         var self = this,
             options = self.setOptions(),
             $captions = self.$element.find('.caption-wrap');

         // Set height of each slide
         if ( self.$element.attr('data-min-height') ) { self.setSlidesSize(); }

         // Make sure captions are all the same size
         if ( $captions.length ) {

            $captions.css( 'min-height', '' );

            var captionHeights = $captions.map( function() {
               return $(this).height();
            }).get();

            var maxHeight = Math.max.apply( null, captionHeights );

            $captions.css( 'min-height', maxHeight );

         }

         // Make sure all slides have equal top padding
         var topPaddings = self.$element.children().map( function() {
            return parseInt( $(this).css('padding-top') );
         }).get();

         var maxTopPadding = Math.max.apply( null, topPaddings );

         self.$element.children().css( 'padding-top', maxTopPadding + 'px' );

         // Check if next/prev nav needs to be repositioned
         if ( self.$element.closest('.slick-slider').find('.slick-next, .slick-prev').length ) {
            var $controls = self.$element.closest('.slick-slider').find('.slick-next, .slick-prev');
            if ( document.getElementById('header')
            && $('#header').hasClass('transparency-active')
            && self.$element.offset().top < ( $('#header').outerHeight() + $('#header').offset().top ) ) {
               var headerHeight = parseInt( $('#header').outerHeight() ) / 2;
               $controls.css({ y: headerHeight+'px' });
            }
            else {
               $controls.children().css({ y: '0px' });
            }
         }

      },

      setSlidesSize: function() {

         var self = this;

         var appliedHeight = $(window).height() * ( parseInt( self.$element.attr('data-min-height') ) / 100 ),
             headerSize = document.getElementById('header') ? parseInt( $('#header').outerHeight() ) + parseInt( $('#header').offset().top ) : 0,
             elementOffset = self.$element.offset().top;

         if ( elementOffset < ( headerSize + 10 ) ) {
            appliedHeight -= elementOffset;
         }

         // Apply the height
         self.$element.find('.cv-wrap-wrapper').css( 'height', appliedHeight + 'px' );

      },

   };

   /* ===================================================================== *
    * Carousel Elements
    * ===================================================================== */

   var CVCarousel = function( element ) {
      this.$element = $(element);
      this.init();
      return this;
   };

   CVCarousel.prototype = {

      init: function() {
         var self = this;

         // Activate the slider
         self.activateSlider();

      },

      setOptions: function() {

         var self = this;

         // Determine how many items per slide to show
         var numItems = self.$element.attr('data-columns') ? parseInt( self.$element.attr('data-columns') ) : 4;
         var scrollNumber = self.$element.attr('data-scroll') ? parseInt( self.$element.attr('data-scroll') ) : 1;
         var scrollAll = 1 === scrollNumber ? false : true;

         // Set up the initial options
         var options = {
            slidesToShow: numItems,
            slidesToScroll: scrollNumber,
            infinite: false,
            arrows: self.$element.attr('data-arrows') ? CVMakeBool( self.$element.attr('data-arrows') ) : true,
            dots: self.$element.attr('data-dots') ? CVMakeBool( self.$element.attr('data-dots') ) : true,
            autoplay: self.$element.attr('data-auto') ? CVMakeBool( self.$element.attr('data-auto') ) : false,
            autoplaySpeed: self.$element.attr('data-delay') ? self.$element.attr('data-delay') : 3000,
            slide: self.$element.attr('data-slide-tag') ? self.$element.attr('data-slide-tag') : 'div',
            rtl: $('html').is('[dir="rtl"]'),
         };

         // Set up the responsive settings
         switch (numItems) {
            case 8:
            case 7:
            case 6:
            case 5:
               options.responsive = [
                  {
                     breakpoint: 900,
                     settings: {
                        slidesToShow: 4,
                        slidesToScroll: scrollAll ? 5 : 1,
                     }
                  },
                  {
                     breakpoint: 700,
                     settings: {
                        slidesToShow: 3,
                        slidesToScroll: scrollAll ? 3 : 1,
                     }
                  },
                  {
                     breakpoint: 500,
                     settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                     }
                  }
               ];
               break;

            case 4:
               options.responsive = [
                  {
                     breakpoint: 900,
                     settings: {
                        slidesToShow: 3,
                        slidesToScroll: scrollAll ? 3 : 1,
                     }
                  },
                  {
                     breakpoint: 700,
                     settings: {
                        slidesToShow: 2,
                        slidesToScroll: scrollAll ? 2 : 1,
                     }
                  },
                  {
                     breakpoint: 500,
                     settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                     }
                  }
               ];
               break;

            case 3:
               options.responsive = [
                  {
                     breakpoint: 700,
                     settings: {
                        slidesToShow: 2,
                        slidesToScroll: scrollAll ? 2 : 1,
                     }
                  },
                  {
                     breakpoint: 500,
                     settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                     }
                  }
               ];
               break;

            case 2:
               options.responsive = [
                  {
                     breakpoint: 500,
                     settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                     }
                  }
               ];
               break;

         }

         return options;

      },

      activateSlider: function() {
         var self = this;
         self.$element.slick( self.setOptions() );
      }

   };

   /* ===================================================================== *
    * Masonry Layouts
    * ===================================================================== */

   var CVMasonryLayout = function( element ) {
      this.$element = $(element);
      this.init();
      return this;
   };

   CVMasonryLayout.prototype = {

      init: function() {

         var self = this;

         // Activate isotope initially
         self.activateIsotope();

         // Insert the filters list
         self.addFilters();

      },

      activateIsotope: function() {
         var self = this;
         self.$element.imagesLoaded( function() {
            self.$element.isotope();
         });
      },

      getTags: function() {
         var self = this, allTags = {};
         self.$element.children().each( function() {
            var $this = $(this), thisTags = $this.data('tags');
            if ( thisTags ) {
               thisTags = thisTags.split(',');
               for ( var i=0; i<_.size( thisTags ); i++ ) {
                  allTags[thisTags[i]] = thisTags[i];
               }
            }
         });
         return allTags;
      },

      addFilters: function() {

         var self = this,
             allTags = self.getTags();

         // Flatten the tags object
         allTags = _.flatten( allTags );

         // Make sure there is at least one tag
         if ( ! _.size( allTags ) ) { return; }

         // Filters container
         var $filters = $( '<ul>', { class: 'masonry-filters filter-list' } );

         // Add the filters label
         if ( self.$element.data('filter-label') ) {
            $filters.append( '<li class="filter-label"><span>'+self.$element.data('filter-label')+'</span></li>' );
         }

         // Add the `All` filter
         $filters.append( '<li class="is-active"><a data-filter="*">All</a></li>' );

         // Add the other filters
         for ( var i=0; i<_.size( allTags ); i++ ) {
            $filters.append( '<li><a data-filter="'+allTags[i]+'">'+allTags[i]+'</a></li>' );
         }

         // Insert the filters
         self.$element.before( $filters );

         // Apply the click event
         $filters.find('a').on( 'click', function() {
            var $this = $(this), filter = $this.data('filter');
            $this.parent().addClass('is-active').siblings().removeClass('is-active');
            self.$element.isotope({
               filter: function() {
                  var $this = $(this);
                  if ( '*' === filter || $this.hasClass('no-filter') ) { return true; }
                  var tags = $this.data('tags');
                  if ( tags ) {
                     tags = tags.split(',');
                     for ( var i=0; i<_.size( tags ); i++ ) {
                        if ( filter === tags[i]) { return true; }
                     }
                  }
                  return false;
               }
            });
         });

      },

   };

   /* ===================================================================== *
    * Tooltips
    * ===================================================================== */

   var CVTooltip = function( element ) {
      var $element = $(element),
          position = $element.data('position') ? $element.data('position') : 'top';
      $element.tooltipster({
         position: position,
         animation: 'fade',
      });
   };

   /* ===================================================================== *
    * Activate all plugins
    * ===================================================================== */

   $(document).on( 'dom-change', function() {

      // Activate min height sections
      $('#body .cv-content-section').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVContentSection' ) ) { return; }
         $this.data( 'CVContentSection', true );
         new CVContentSection( this );
      });

      // Activate min height sections
      $('.cv-fullwidth-map').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVFullwidthMap' ) ) { return; }
         $this.data( 'CVFullwidthMap', true );
         new CVFullwidthMap( this );
      });

      // Activate scaling typography
      $('.cv-media-flag').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVMediaFlag' ) ) { return; }
         $this.data( 'CVMediaFlag', true );
         new CVMediaFlag( this );
      });

      // Activate scaling typography
      $('.cv-scaling-typography').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVScalingTypography' ) ) { return; }
         $this.data( 'CVScalingTypography', true );
         new CVScalingTypography( this );
      });

      // Activate testimonial groups
      $('.cv-testimonial-group').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVTestimonialGroup' ) ) { return; }
         $this.data( 'CVTestimonialGroup', true );
         new CVTestimonialGroup( this );
      });

      // Activate Change Logs
      $('.cv-change-log').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVChangeLog' ) ) { return; }
         $this.data( 'CVChangeLog', true );
         new CVChangeLog( this );
      });

      // Activate promo boxes
      $('.cv-promo-box').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVPromoBox' ) ) { return; }
         $this.data( 'CVPromoBox', true );
         new CVPromoBox( this );
      });

      // Activate toggle groups
      $('.cv-toggle-group').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVToggleGroup' ) ) { return; }
         $this.data( 'CVToggleGroup', true );
         new CVToggleGroup( this );
      });

      // Activate tab groups
      $('.cv-tab-group').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVTabGroup' ) ) { return; }
         $this.data( 'CVTabGroup', true );
         new CVTabGroup( this );
      });

      // Activate contact forms
      $('.cv-form').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVContactForm' ) ) { return; }
         $this.data( 'CVContactForm', true );
         new CVContactForm( this );
      });

      // Activate animated numbers
      $('.cv-animated-number').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVAnimatedNumber' ) ) { return; }
         $this.data( 'CVAnimatedNumber', true );
         new CVAnimatedNumber( this );
      });

      // Activate progress bars
      $('.cv-progress-bars').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVProgressBars' ) ) { return; }
         $this.data( 'CVProgressBars', true );
         new CVProgressBars( this );
      });

      // Activate buttons
      $('.cv-button[data-color]').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVButton' ) ) { return; }
         $this.data( 'CVButton', true );
         new CVButton( this );
      });

      // Activate sliders
      $('.cv-slider').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVSlider' ) ) { return; }
         $this.data( 'CVSlider', true );
         new CVSlider( this );
      });

      // Activate carousels
      $('.cv-carousel').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVCarousel' ) ) { return; }
         $this.data( 'CVCarousel', true );
         new CVCarousel( this );
      });

      // Activate masonry layouts
      $('.masonry-layout').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVMasonryLayout' ) ) { return; }
         $this.data( 'CVMasonryLayout', true );
         new CVMasonryLayout( this );
      });

      // Activate tooltips
      $('.tooltip, [data-tooltip], [rel="tooltip"]').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVTooltip' ) ) { return; }
         $this.data( 'CVTooltip', true );
         new CVTooltip( this );
      });

   }).ready( function() {
      $(document).trigger('dom-change');
   });

})(jQuery);