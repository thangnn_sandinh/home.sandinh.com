<?php

if ( ! function_exists( 'cv_load_theme_assets' ) ) :

/**
 * Load required frotnend assets
 */
add_action( 'wp_enqueue_scripts', 'cv_load_theme_assets' );

/**
 * Function to load required assets
 *
 * @return void
 */
function cv_load_theme_assets() {

   global $canvys;

   // Google Maps API
   wp_register_script('cv-gmaps-api', 'http://maps.google.com/maps/api/js?sensor=true', null, THEME_VER, true );

   // Included JavaScript functionality
   wp_register_script('jquery-camyno', THEME_DIR . 'assets/js/compressed/jquery.camyno.min.js', array( 'underscore', 'jquery' ), THEME_VER, true );

   // FullPage.js API (Only loaded when page sliding is active)
   wp_register_script('jquery-pageSlide', THEME_DIR . 'assets/js/compressed/jquery.pageSlide.min.js', array( 'jquery-camyno' ), THEME_VER, true );

   // Include full page sliding
   if ( cv_is_page_slide_active() ) {
      wp_enqueue_script('jquery-pageSlide');
   }

   // Include theme functionality
   wp_enqueue_script('jquery-camyno');

   // Include WordPress HTML5 Element script
   wp_enqueue_script('wp-mediaelement');

   // Register CSS icons Stylesheet
   wp_register_style( 'cv-theme-icons', THEME_DIR . 'assets/css/icons.css');

   // Register SCSS based Stylesheet
   $stylesheet = cv_theme_setting( 'general', 'disable_responsive' ) ? 'style' : 'style-responsive';
   wp_register_style( 'cv-base-style', THEME_DIR . 'assets/css/' . $stylesheet . '.css');

   // Enqueue all Stylesheets
   wp_enqueue_style('cv-theme-icons');
   wp_enqueue_style('cv-base-style');

   // Make sure media styles are loaded
   wp_enqueue_style('wp-mediaelement');

}
endif;