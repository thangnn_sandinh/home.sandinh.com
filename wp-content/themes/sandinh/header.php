<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta content='True' name='HandheldFriendly' />
	<meta content='width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;' name='viewport' />
	<meta name="viewport" content="width=device-width" />

	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/sidr/stylesheets/jquery.sidr.dark.css">
	<script src="<?php echo get_template_directory_uri(); ?>/sidr/jquery.sidr.min.js"></script>
	<!--<script src="<?php echo get_template_directory_uri(); ?>/js/modal.js"></script>-->

</head>

<body <?php body_class(); ?>>
<div id="fb-root"></div>
	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">
			<!--<a id="simple-menu" href="#sidr" style="display: none;">Toggle menu</a>
			<div id="sidr" style="display: none;">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<?php /*wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'sidebar-menu', 'menu_id' => 'sidebar-menu' ) ); */?>
				</nav>
			</div>
-->
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="header-img">
				<img src="<?php echo get_template_directory_uri(); ?>/images/header.png" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
			</a>
			<div id="navigation" class="navbar">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<button class="menu-toggle"><?php _e( 'Menu', 'twentythirteen' ); ?></button>
					<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php _e( 'Skip to content', 'twentythirteen' ); ?></a>
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu', 'menu_id' => 'primary-menu' ) ); ?>
					<?php //get_search_form(); ?>
				</nav><!-- #site-navigation -->
			</div><!-- #navbar -->

			<!--<script>
			jQuery.noConflict();
			jQuery(document).ready(function() {
			  jQuery('#simple-menu').sidr();
			});
			</script>-->
		</header><!-- #masthead -->

		<div id="main" class="site-main">
