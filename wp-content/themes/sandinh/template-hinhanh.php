<?php
/**
 * Template Name: Hinh Anh
 *
 * Hinh Anh template
 *
 * @package sandinh
 * @subpackage
 * @since
 */

get_header(); ?>
    <div id="primary" class="content-area">
        <!--<div id="content" class="site-content" role="main">-->
            <?php
            while (have_posts()) : the_post();
                the_content();
            endwhile;
            ?>
        <!--</div>-->
        <!-- #content -->
    </div><!-- #primary -->

<?php get_footer(); ?>