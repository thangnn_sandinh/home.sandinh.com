<?php
/**
 * Template Name: choi game - Display game to player
 *
 * Choi Game page template
 *
 * @package    sandinh
 * @subpackage 
 * @since      
 */

get_header(); ?>
<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">
	<?php
	while ( have_posts() ) : the_post(); 
		the_content();
	endwhile;		
	?>			
	</div><!-- #content -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>