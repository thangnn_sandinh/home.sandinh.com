<?php
/**
 * Template Name: Ngan Hang
 *
 * template Ngan Hang
 *
 * @package    sandinh
 * @subpackage
 * @since
 */

get_header(); ?>
    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
            <?php
            $user_id = getXfUserId();

            if($user_id>0): ?>
            <?php
            while (have_posts()) : the_post();
                the_content();
            endwhile;
            ?>
            <?php else: ?>
                <div id="login-to-play">
                    <h2><?php echo _x('Đăng nhập'); ?></h2>
                    <?php if($_REQUEST['login']=='failed'): ?>
                        <?php if($_REQUEST['msg']=='empty'){ ?>
                            <p class="error"><?php echo _x('Tên đăng nhập và mật khẩu không được để trống.'); ?></p>
                        <?php }elseif($_REQUEST['reason']!=''){ ?>
                            <p class="error"><?php echo _x('Đăng nhập thất bại, tài khoản đang bị khóa với lý do: ').$_REQUEST['reason']; ?></p>
                        <?php }else{ ?>
                            <p class="error"><?php echo _x('Đăng nhập thất bại, vui lòng thử lại sau'); ?></p>
                        <?php } ?>
                    <?php endif; ?>
                    <?php
                    $args = array(
                        'echo'           => true,
                        'remember'       => true,
//					'redirect'       => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
                        'redirect'       => get_site_url().'/ngan-hang/',
                        'form_id'        => 'loginform',
                        'id_username'    => 'user_login',
                        'id_password'    => 'user_pass',
                        'id_remember'    => 'rememberme',
                        'id_submit'      => 'wp-submit',
                        'label_username' => __( 'Tên đăng nhập' ),
                        'label_password' => __( 'Mật khẩu' ),
                        'label_remember' => __( 'Nhớ đăng nhập' ),
                        'label_log_in'   => __( 'Đăng nhập' ),
                        'value_username' => '',
                        'value_remember' => false
						
                    );
                    wp_login_form($args);
                    ?>
                    <a class="simplemodal-register" href="<?php echo wp_registration_url(); ?>"><?php echo _x('Đăng ký'); ?></a>
                </div>
            <?php endif; ?>
        </div>
        <!-- #content -->
    </div><!-- #primary -->

<?php get_footer(); ?>