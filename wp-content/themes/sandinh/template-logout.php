<style>
	html{
		display: none;
	}
</style>
<?php
/**
 * Template Name: Logout
 *
 * Logout page template
 *
 * @package    sandinh
 * @subpackage 
 * @since      
 */
 
get_header(); ?>
<script type="text/javascript">
<!--
//window.location = "<?php //echo home_url(); ?>http://dev.sandinh.com"
window.location = "<?php echo home_url(); ?>"
//-->
</script>
	<div id="primary" class="content-area">
		<?php 
		wp_logout();
		//auth_redirect();
		?>
	</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
