<?php
/**
 * Template Name: Video
 *
 * Video template
 *
 * @package    sandinh
 * @subpackage
 * @since
 */

get_header(); ?>
    <div id="primary" class="content-area">
        <header class="entry-header">
            <div class="entry-meta">
                <h1 class="entry-title"><?php the_title(); ?></h1>
                <ul id="breadcrumbs">
                    <li><a href="<?php echo get_site_url(); ?>" title="<?php echo 'Trang chủ'; ?>"><?php echo 'Trang chủ'; ?></a></li>
                    <li> &gt; </li>
                    <li><?php the_title(); ?></li>
                </ul>
            </div><!-- .entry-meta -->
        </header><!-- .entry-header -->
        <div id="content" class="site-content" role="main">
            <?php dynamic_sidebar( 'videos-tab' ); ?>
            <?php
            while (have_posts()) : the_post();
                the_content();
            endwhile;
            ?>
        </div>
        <!-- #content -->
    </div><!-- #primary -->

<?php get_footer(); ?>