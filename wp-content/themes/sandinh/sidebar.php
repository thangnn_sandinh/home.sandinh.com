<?php
/**
 * The sidebar containing the secondary widget area
 *
 * Displays on posts and pages.
 *
 * If no active widgets are in this sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
	<div id="tertiary" class="sidebar-container" role="complementary">
		<div class="sidebar-inner">
			<div class="widget-area">
				<aside class="widget">
					<div class="vc_row wpb_row vc_row-fluid">
						<div class="wpb_column vc_column_container vc_col-sm-12">
							<div class="wpb_wrapper">							
							<?php

							if ( is_user_logged_in()) {
								global $current_user;
								get_currentuserinfo();

								$user_id = getXfUserId();
								if($user_id>0){
									$json_url = 'http://dev.sandinh.com/api/user/detail/'.sd_encrypt($user_id);
									// Initializing curl
									$ch = curl_init( $json_url );
									// Configuring curl options
									$options = array(
										CURLOPT_RETURNTRANSFER => true,
										CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
									);
									// Setting curl options
									curl_setopt_array( $ch, $options );
									// Getting results
									$result =  curl_exec($ch); // Getting JSON result string
									$list = json_decode($result);
//									print_r($list->b);
									curl_close($ch);
								}
								?>
								<div class="wpb_text_column wpb_content_element user-box">
									<div class="wpb_wrapper">
										<a href="http://dev.sandinh.com/account/personal-details<?php //echo site_url('ngan-hang'); ?>"><?php echo get_avatar($current_user->ID, 56); ?></a>
										<h4 class="account-name"><a href="http://dev.sandinh.com/account/personal-details<?php //echo site_url('ngan-hang'); ?>"><?php echo $current_user->display_name . "\n"; ?></a>
										</h4>

										<div class="account-detail-link"><a
												href="<?php echo site_url('ngan-hang'); ?>"><?php echo _x('Số dư:'); ?> <?php echo number_format_unchanged_precision($list->b, ',', '.'); ?></a>
										</div>
										<div class="logout"><a
												href="<?php echo wp_logout_url( home_url() ); ?>"><?php echo _x('Thoát'); ?></a>
										</div>
									</div>
								</div>
								<?php
							}else{
								?>
								<!--<div class="wpb_text_column wpb_content_element dang-ky-box">
								<div class="wpb_wrapper">
									<h3><a class="simplemodal-login"
											   href="/wp-login.php"><?php echo _x('Đăng nhập'); ?></a> / <a
												class="simplemodal-register"
												href="/wp-login.php?action=register"><?php echo _x('Đăng ký'); ?></a>
										</h3>
									<h3><a id='dang-nhap-btn' href="javascript: void()"><?php echo _x('Đăng nhập'); ?></a> / <a href="javascript: void()"><?php echo _x('Đăng ký'); ?></a></h3>
								</div>
							</div>
							<script>
								// Wait until the DOM has loaded before querying the document
								jQuery(document).ready(function(){
									jQuery('a#dang-nhap-btn').click(function(e){
										modal.open({content: jQuery('#login-form').html()});
										e.preventDefault();
									});
								});
							</script>
							<div id="login-form" style="display: none;">
							<?php //wp_login_form();
								?>
							<?php //do_action( 'wordpress_social_login' );
								?>
							<?php //echo do_shortcode('[wppb-register]');
								?>
							</div>-->
								<div class="wpb_text_column wpb_content_element dang-ky-box">
									<div class="wpb_wrapper">
										<h3><?php echo _x('Đăng nhập'); ?></h3>
									</div>
								</div>
								<div id="login-form">
									<?php if($_REQUEST['login']=='failed'): ?>
										<?php if($_REQUEST['msg']=='empty'){ ?>
											<p class="error"><?php echo _x('Tên đăng nhập và mật khẩu không được để trống.'); ?></p>
										<?php }elseif($_REQUEST['reason']!=''){ ?>
											<p class="error"><?php echo _x('Đăng nhập thất bại, tài khoản đang bị khóa với lý do: ').$_REQUEST['reason']; ?></p>
										<?php }else{ ?>
											<p class="error"><?php echo _x('Đăng nhập thất bại, vui lòng thử lại sau'); ?></p>
										<?php } ?>
									<?php endif; ?>
									<?php
									$args = array(
										'echo'           => true,
										'remember'       => true,
										//'redirect'       => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
										'redirect'       => get_site_url(),
										'form_id'        => 'loginform',
										'id_username'    => 'user_login',
										'id_password'    => 'user_pass',
										'id_remember'    => 'rememberme',
										'id_submit'      => 'wp-submit',
										'label_username' => __( 'Tên đăng nhập' ),
										'label_password' => __( 'Mật khẩu' ),
										'label_remember' => __( 'Nhớ đăng nhập' ),
										'label_log_in'   => __( 'Đăng nhập' ),
										'value_username' => '',
										'value_remember' => false
									);
									wp_login_form($args);
									?>
									<?php //do_action('wordpress_social_login'); ?>
									<!--<div class="wp-social-login-widget">

										<div class="wp-social-login-connect-with">Đăng nhập với</div>

										<div class="wp-social-login-provider-list">

											<a rel="nofollow" href="http://dev.sandinh.com/register/facebook?reg=1" title="Connect with Facebook" class="wp-social-login-provider wp-social-login-provider-facebook" data-provider="Facebook">
												<img alt="Facebook" title="Connect with Facebook" src="http://home.sandinh.com/wp-content/plugins/wordpress-social-login/assets/img/32x32/wpzoom/facebook.png">
											</a>

											a class="googleLogin GoogleLogin JsOnly" tabindex="110" data-client-id="640949268663-vhjdih6otib9og7om2p8gl5mc4dspnt0.apps.googleusercontent.com" href="http://dev.sandinh.com/register/google?code=__CODE__&amp;csrf=4GmRn67DlkQmersp" data-gapiattached="true"><span><img alt="Google" title="Connect with Google" src="http://home.sandinh.com/wp-content/plugins/wordpress-social-login/assets/img/32x32/wpzoom/google.png"></span></a>

											<a rel="nofollow" href="http://dev.sandinh.com/register/twitter?reg=1" title="Connect with Twitter" class="wp-social-login-provider wp-social-login-provider-twitter" data-provider="Twitter">
												<img alt="Twitter" title="Connect with Twitter" src="http://home.sandinh.com/wp-content/plugins/wordpress-social-login/assets/img/32x32/wpzoom/twitter.png">
											</a>

										</div>
										<div class="wp-social-login-widget-clearing"></div>
									</div>-->
<!--									<a href="http://dev.sandinh.com/register/facebook?reg=1" class="frm_fbLogin"><span>Connect Facebook</span></a>-->
									<p class="register"><?php echo _x('Chưa có tài khoản?'); ?> <a class="simplemodal-register" href="<?php echo wp_registration_url(); ?>"><?php echo _x('Đăng ký'); ?></a></p>
								</div>
								<?php
							}
							?>
							</div>
						</div>
					</div>
				</aside>
				<?php dynamic_sidebar( 'sidebar-2' ); ?>
				<aside class="widget">
					<div class="vc_row wpb_row vc_row-fluid box-title">
						<div class="cu-phu-box-title wpb_column vc_column_container vc_col-sm-12">
							<div class="wpb_wrapper">
								<div id="ultimate-heading5620aa131e172" class="uvc-heading ultimate-heading5620aa131e172 uvc-427 " data-hspacer="no_spacer" data-halign="center" style="text-align:center">
									<div class="uvc-heading-spacer no_spacer" style="top"></div>
									<div class="uvc-main-heading ult-responsive" data-ultimate-target=".uvc-heading.ultimate-heading5620aa131e172 h3" data-responsive-json-new="{&quot;font-size&quot;:&quot;&quot;,&quot;line-height&quot;:&quot;&quot;}">
										<h3 style="font-weight:normal;"><?php echo _x('Bảng xếp hạng Sân Đình'); ?></h3>
									</div>
								</div>
							</div>
						</div>
					</div>				
					<div class="vc_row wpb_row vc_row-fluid box-content">
						<div class="cu-phu-box-content wpb_column vc_column_container vc_col-sm-12">
							<div class="wpb_wrapper">
								<div class="wpb_text_column wpb_content_element  cu-phu-san-dinh-content">
									<div class="wpb_wrapper">
										<select id="bang-xep-hang-type">
											<option value="top-cu-phu"><?php echo _x('Top cự phú'); ?></option>
											<option value="top-kinh-nghiem"><?php echo _x('Top kinh nghiệm'); ?></option>
											<option value="top-cuoc-u"><?php echo _x('Top cước ù to'); ?></option>
										</select>
										<div id="bang-vang">
											<?php
											// JSON URL which should be requested
											$json_url = 'http://dev.sandinh.com/api/user/top';
											// Initializing curl
											$ch = curl_init( $json_url );
											// Configuring curl options
											$options = array(
												CURLOPT_RETURNTRANSFER => true,
												CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
											);
											// Setting curl options
											curl_setopt_array( $ch, $options );
											// Getting results
											$result =  curl_exec($ch); // Getting JSON result string
											$list = json_decode($result);
											curl_close($ch);
											?>
											<div id="top-cu-phu">
												<ul>
											<?php
											foreach($list->tb as $item){
												$values = explode('|',$item);
												echo '<li><span class="gamer">'.$values[1].'</span> <span class="amount">'.$values[2].'</span></li>';
											}
											?>
												</ul>
											</div>

											<div id="top-kinh-nghiem" style="display: none;">
												<ul>
											<?php
											foreach($list->te as $item){
												$values = explode('|',$item);
												echo '<li><span class="gamer">'.$values[1].'</span> <span class="amount">'.$values[2].'</span></li>';
											}
											?>
												</ul>
											</div>

											<div id="top-cuoc-u" style="display: none;">
												<ul>
											<?php
											foreach($list->tc as $item){
												$values = explode('|',$item);
												echo '<li><span class="gamer">'.$values[1].'</span> <span class="amount">'.$values[2].'</span></li>';
											}
											?>
												</ul>
											</div>
										</div>
										<script>
										jQuery('#bang-xep-hang-type').change(function() {
											if(jQuery('#bang-xep-hang-type').val()=='top-kinh-nghiem'){
												jQuery('#top-cu-phu').hide();
												jQuery('#top-kinh-nghiem').show();
												jQuery('#top-cuoc-u').hide();
											}else if(jQuery('#bang-xep-hang-type').val()=='top-cuoc-u'){
												jQuery('#top-cu-phu').hide();
												jQuery('#top-kinh-nghiem').hide();
												jQuery('#top-cuoc-u').show();
											}else{
												jQuery('#top-cu-phu').show();
												jQuery('#top-kinh-nghiem').hide();
												jQuery('#top-cuoc-u').hide();
											}
										});
										</script>
									</div>
								</div>
							</div>
						</div>						
					</div>
					<?php
					echo do_shortcode( '[FBW]' );
					?>
					<!--<div class="vc_row wpb_row vc_row-fluid box-content">
						<div class="cu-phu-box-content wpb_column vc_column_container vc_col-sm-12">
							<div class="wpb_wrapper">
								<div class="wpb_text_column wpb_content_element  cu-phu-san-dinh-content">
									<div class="wpb_wrapper">
										<?php
/*										echo do_shortcode( '[FBW]' );
										*/?>
									</div>
								</div>
							</div>
						</div>
					</div>-->
				</aside>
			</div><!-- .widget-area -->
		</div><!-- .sidebar-inner -->
	</div><!-- #tertiary -->
<?php endif; ?>