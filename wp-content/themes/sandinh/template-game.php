<?php
/**
 * Template Name: game - Display game
 *
 * Game page template
 *
 * @package    sandinh
 * @subpackage 
 * @since      
 */

get_header(); ?>
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=thangnn1510" async="async"></script>


	<div id="primary" class="content-area">
		<header id="header-game">
			<div class="game-icon">
				<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
					<?php the_post_thumbnail(); ?>
				<?php endif; ?>
			</div>
			<div class="game-detail">
				<h1 class="entry-title"><?php the_title(); ?></h1>
				<?php
				$rating = get_field("rating");
				if($rating) {
					$width = ($rating/5)*100; 
					echo '<div class="rating-wrapper"><span class="rating" style="width:'.$width.'%;"></span></div> <div class="rating-text">Đánh giá: '.$rating.'/5</div>';
				}
				?>
				<!-- Go to www.addthis.com/dashboard to customize your tools -->
				<div class="addthis_native_toolbox"></div>
			</div>
			<div class="game-buttons">
				<?php
				//get buttons of games
				$link_choi = get_field("link_choi");
				if( $link_choi ) {					
					echo '<a class="choi-ngay-btn" href="'.$link_choi.'"><img src="'.get_template_directory_uri().'/images/choi-ngay-btn.png" /></a>';
				}
				?>
				<div class="download-buttons">
				<?php
				$android_link = get_field("android_link");
				if( $android_link ) {					
					echo '<a class="android-btn" href="'.$android_link.'"><img src="'.get_template_directory_uri().'/images/android-btn.png" /></a>';
				}	
				
				$ios_link = get_field("ios_link");
				if( $ios_link ) {					
					echo '<a class="android-btn" href="'.$ios_link.'"><img src="'.get_template_directory_uri().'/images/ios-btn.png" /></a>';
				}				
				?>
				</div>
			</div>
		</header>
		<div id="content" class="site-content" role="main">
		<?php
		echo do_shortcode( '[tabby title="Giới thiệu"]' );
		while ( have_posts() ) : the_post(); 
			the_content();
		endwhile;
		
		$luat_choi = get_field("luat_choi");
		if( $luat_choi ) {					
			echo do_shortcode( '[tabby title="Luật chơi"]' );
			echo $luat_choi;
		}
		
		$huong_dan_choi = get_field("huong_dan_choi");
		if( $huong_dan_choi ) {					
			echo do_shortcode( '[tabby title="Hướng dẫn chơi"]' );
			echo $huong_dan_choi;
		}
		
		$choi_tren_mobile = get_field("choi_tren_mobile");
		if( $choi_tren_mobile ) {					
			echo do_shortcode( '[tabby title="Chơi trên di động"]' );
			echo $choi_tren_mobile;
		}
		
		echo do_shortcode( '[tabbyending]' );
		?>			
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>