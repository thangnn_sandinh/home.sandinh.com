<?php
/**
 * Template Name: Hội Làng
 *
 * Hội Làng template
 *
 * @package    sandinh
 * @subpackage
 * @since
 */

get_header(); ?>

	<div id="primary" class="content-area">
			<?php while ( have_posts() ) : the_post(); ?>
				<header class="entry-header">
						<div class="entry-meta">
							<h1 class="entry-title"><?php the_title(); ?></h1>
							<ul id="breadcrumbs">
                                <li><a href="http://home.sandinh.com" title="Trang chủ">Trang chủ</a></li>
                                <li> &gt; </li>
                                <li>Sự kiện hàng tuần</li>
                            </ul>
						</div><!-- .entry-meta -->
						<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
							<label>
								<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
								<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Nhập thông tin tìm kiếm ...', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
							</label>
							<input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
						</form>
					</header><!-- .entry-header -->
					<div id="content" class="site-content" role="main">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="entry-content">
								<?php the_content(); ?>
								<!-- Go to www.addthis.com/dashboard to customize your tools -->
								<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=thangnn1510" async="async"></script>
								<!-- Go to www.addthis.com/dashboard to customize your tools -->
								<div class="addthis_native_toolbox"></div>
								<?php //wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
							</div><!-- .entry-content -->
						</article>
						<div class="related-posts">
							<h3><?php echo _x( 'Các cước ù to nhất tuần', 'label' ) ?></h3>
							<?php
							$args=array(
								'tag' => $tags[0]->slug,
								'post__not_in' => array($post->ID),
								'posts_per_page'=> 20,
								'caller_get_posts'=> 1,
								'category_name' => 'hoi-lang'
							);
							$my_query = new WP_Query($args);
							if( $my_query->have_posts() ) {
								?>
								<ul>
									<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
										<li><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
									<?php endwhile;	?>
								</ul>
								<?php
							}
							wp_reset_query();
							?>
						</div>
						<?php comments_template(); ?>
					</div><!-- #content -->

			<?php endwhile; ?>
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>