(function(window, undefined){
    // Localise Globals
    var document = window.document;

    window.addEventListener('message', function(e) {
        if (e.origin.indexOf("home.sandinh.com") < 0 ) return;
        if (e.data.setIframeSize) {
            var iframe = document.getElementById('mainIframe');
            if (e.data.setIframeSize.height) {
                iframe.setAttribute('height', e.data.setIframeSize.height);
            }
            if (e.data.setIframeSize.width) {
                iframe.setAttribute('width', e.data.setIframeSize.width);
            }
        }
    }, false);

   jQuery(document).ready(function() {
       if (window.location.pathname === '/leader') {
           jQuery('#mainIframe').attr('src', '/api/gold-leader' + window.location.hash);
       }
   });
   jQuery(window).on('popstate',function(){
       if (window.location.pathname === '/leader') {
           var iframe = document.getElementById('mainIframe').contentWindow;
           iframe.postMessage({url: window.location.hash}, '*');
       }
   });
})(window);