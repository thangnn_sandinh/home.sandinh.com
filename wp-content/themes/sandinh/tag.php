<?php
/**
 * The template for displaying Tag pages
 *
 * Used to display archive-type pages for posts in a tag.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<header class="entry-header">
		<div class="entry-meta">
			<h3><?php printf( __( '%s', 'twentythirteen' ), single_tag_title( '', false ) ); ?></h3>
			<ul id="breadcrumbs">
				<li><a href="<?php echo get_site_url(); ?>" title="<?php echo 'Trang chủ'; ?>"><?php echo 'Trang chủ'; ?></a></li>
				<li> &gt; </li>
				<li><?php printf( __( '%s', 'twentythirteen' ), single_tag_title( '', false ) ); ?></li>
			</ul>
		</div><!-- .entry-meta -->
		<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
			<label>
				<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
				<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Nhập thông tin tìm kiếm ...', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
			</label>
			<input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
		</form>
	</header>

	<div id="content" class="site-content" role="main">

		<?php if ( have_posts() ) : ?>
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<article class="post">
					<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					<?php
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium');
					$url = $thumb['0'];
					if($url!=""): ?>
					<div class="entry-thumbnail">
						<a href="<?php the_permalink(); ?>" rel="bookmark"><img src="<?php echo $url; ?>" width=120 /></a>
					</div>
					<?php endif; ?>

					<div class="entry-excerpt"><?php the_excerpt(); ?></div>
					<a class="more-link" href="<?php the_permalink(); ?>"><?php echo _x( 'Chi tiết', 'label' ) ?></a>
				</article>
			<?php endwhile; ?>
			<?php //twentythirteen_paging_nav(); ?>
			<div class="custom-pagination">
				<?php //if(function_exists('wp_paginate')) {
				//wp_paginate();
				//}
				if(function_exists('wp_simple_pagination')) {
					wp_simple_pagination();
				}else {
					twentythirteen_paging_nav();
				}
				?>
			</div>
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>
	</div><!-- #content -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
