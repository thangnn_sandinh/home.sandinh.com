﻿<?php
		//Show only on home page
		if(is_front_page() || is_home()){
		?>
		<div class="tinh-nang-dac-sac">
			<div class="tab-tinh-nang xem-lai-content">
				<?php echo get_the_post_thumbnail( 3113, 'full' ); ?>
				<div class="tinh-nang-detail">
				<?php
				$xem_lai = get_post(3113);
				?>
				<h3><?php echo $xem_lai->post_title; ?></h3>
				<div class="summary">
					<?php echo $xem_lai->post_excerpt; ?>
				</div>
				<a href="<?php echo get_permalink(3113); ?>" class="xem-them">xem thêm</a>
				</div>
			</div>
			<div class="tab-tinh-nang cong-than-content" style="display: none;">
				<?php echo get_the_post_thumbnail( 3120, 'full' ); ?>
				<div class="tinh-nang-detail">
				<?php
				$xem_lai = get_post(3120);
				?>
				<h3><?php echo $xem_lai->post_title; ?></h3>
				<div class="summary">
					<?php echo $xem_lai->post_excerpt; ?>
				</div>
				<a href="<?php echo get_permalink(3120); ?>" class="xem-them">xem thêm</a>
				</div>

			</div>
			<div class="tab-tinh-nang khoa-cu-content" style="display: none;">
				<?php echo get_the_post_thumbnail( 3110, 'full' ); ?>
				<div class="tinh-nang-detail">
				<?php
				$khoa_cu = get_post(3110);
				?>
				<h3><?php echo $khoa_cu->post_title; ?></h3>
				<div class="summary">
					<?php echo $khoa_cu->post_excerpt; ?>
				</div>
				<a href="<?php echo get_permalink(3110); ?>" class="xem-them">xem thêm</a>
				</div>

			</div>
			<div class="tab-tinh-nang xin-choi-content" style="display: none;">
				<?php echo get_the_post_thumbnail( 3118, 'full' ); ?>
				<div class="tinh-nang-detail">
				<?php
				$xin_choi = get_post(3118);
				?>
				<h3><?php echo $xin_choi->post_title; ?></h3>
				<div class="summary">
					<?php echo $xin_choi->post_excerpt; ?>
				</div>
				<a href="<?php echo get_permalink(3118); ?>" class="xem-them">xem thêm</a>
				</div>

			</div>
			<div class="tab-tinh-nang tinh-nang-content" style="display: none;">
				<?php echo get_the_post_thumbnail( 3122, 'full' ); ?>
				<div class="tinh-nang-detail">
				<?php
				$tinh_nang = get_post(3122);
				?>
				<h3><?php echo $tinh_nang->post_title; ?></h3>
				<div class="summary">
					<?php echo $tinh_nang->post_excerpt; ?>
				</div>
				<a href="<?php echo get_permalink(3122); ?>" class="xem-them">xem thêm</a>
				</div>

			</div>
			<div class="tab-tinh-nang su-kien-content" style="display: none;">
				<?php echo get_the_post_thumbnail( 3390, 'full' ); ?>
				<div class="tinh-nang-detail">
					<?php
					$su_kien = get_post(3390);
					?>
					<h3><?php echo $su_kien->post_title; ?></h3>
					<div class="summary">
						<?php echo get_field( "tom_tat", 3390 ); ?>
					</div>
					<a href="<?php echo get_permalink(3390); ?>" class="xem-them">xem thêm</a>
				</div>

			</div>
			<div class="tab-tinh-nang khuyen-mai-content" style="display: none;">
				<?php echo get_the_post_thumbnail( 3408, 'full' ); ?>
				<div class="tinh-nang-detail">
					<?php
					$khuyen_mai = get_post(3408);
					?>
					<h3><?php echo $khuyen_mai->post_title; ?></h3>
					<div class="summary">
						<?php echo get_field( "tom_tat", 3408 ); ?>
					</div>
					<a href="<?php echo get_permalink(3408); ?>" class="xem-them">xem thêm</a>
				</div>
			</div>
			<ul class="images-tabs">
				<li class="btn-tinh-nang xem-lai-btn"><a data-link='xem-lai-content' class="hover" href="javascript: void();">Xem lại</a></li>
				<li class="btn-tinh-nang cong-than-btn"><a data-link='cong-than-content' href="javascript: void();">Công thần</a></li>
				<li class="btn-tinh-nang khoa-cu-btn"><a data-link='khoa-cu-content' href="javascript: void();">Khoa cử</a></li>
				<li class="btn-tinh-nang xin-choi-btn"><a data-link='xin-choi-content' href="javascript: void();">Xin chơi</a></li>
				<li class="btn-tinh-nang tinh-nang-btn"><a data-link='tinh-nang-content' href="javascript: void();">Tính năng</a></li>
				<li class="btn-tinh-nang su-kien-btn"><a data-link='su-kien-content' href="javascript: void();">Sự kiện</a></li>
				<li class="btn-tinh-nang khuyen-mai-btn"><a data-link='khuyen-mai-content' href="javascript: void();">Khuyến mại</a></li>
			</ul>
		</div>
			<script>
				//Change content of Tabs
				jQuery('.btn-tinh-nang a').hover(function(){
					var link = jQuery(this).data('link');
					jQuery('.tab-tinh-nang').hide();
					jQuery("."+link).show();
					jQuery('.btn-tinh-nang a').removeClass('hover');
					jQuery(this).addClass('hover');
				});
				// Change image of tabs thumbnail when hover
				/*jQuery('.btn-tinh-nang img').each(function(){
					var t=jQuery(this);
					var src1= t.attr('src'); // initial src
					var newSrc = src1.substring(0, src1.lastIndexOf('.')); // let's get file name without extension
					t.hover(function(){
						jQuery(this).attr('src', newSrc+ '-hover.' + /[^.]+$/.exec(src1)); //last part is for extension
					}, function(){
						jQuery(this).attr('src', newSrc + '.' + /[^.]+$/.exec(src1)); //removing '-over' from the name
					});
				});*/
			</script>
		<?php
			get_sidebar( 'main' );
		}
		?></div><!-- #main -->
		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php dynamic_sidebar( 'footer-widgets' ); ?>
		</footer><!-- #colophon -->
	</div><!-- #page -->
	<?php dynamic_sidebar('sidebar-ho-tro'); ?>
	<?php wp_footer(); ?>
</body>
</html>