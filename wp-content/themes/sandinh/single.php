<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<header class="entry-header">				
			<div class="entry-meta">
			<?php
			/*$cats = array();
			$category = 1;
			foreach(wp_get_post_categories(get_the_id()) as $c)
			{
				$cat = get_category($c);
				if(($cat->slug!='uncategorized')&&($cat->slug!='tong-hop')){
					$category_id = $cat->cat_ID;							
					$category_name = $cat->cat_name;							
					$category_slug = $cat->slug;							
				}
			}

			$category_link = get_category_link($category_id);*/

			$tags = get_the_tags( get_the_id() );
			if ( $tags ) {
				usort( $tags, '_usort_terms_by_ID' );
				//$tag = $tags[0]->slug;
			}

			?>
			<h3><?php echo $tags[0]->name; ?></h3>
			<ul id="breadcrumbs">
				<li><a href="<?php echo get_site_url(); ?>" title="<?php echo 'Trang chủ'; ?>"><?php echo 'Trang chủ'; ?></a></li>
				<li> &gt; </li>							
				<li><a href="<?php echo get_site_url().'/'.$tags[0]->slug; ?>" title="<?php echo $tags[0]->name; ?>"><?php echo $tags[0]->name; ?></a></li>
			</ul>		
			</div><!-- .entry-meta -->
			<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
				<label>
					<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
					<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Nhập thông tin tìm kiếm ...', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
				</label>
				<input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
			</form>
		</header><!-- .entry-header -->
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>			
				<?php get_template_part( 'content', get_post_format() ); ?>
				<?php //twentythirteen_post_nav(); ?>				
				<div class="related-posts">				
					<h3><?php echo _x( 'Tin liên quan', 'label' ) ?></h3>
					<?php					
					$args=array(
						'tag' => $tags[0]->slug,
						'post__not_in' => array($post->ID),
						'posts_per_page'=>4,
						'caller_get_posts'=>1
					);
					$my_query = new WP_Query($args);
					if( $my_query->have_posts() ) {
						?>
						<ul>
							<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
							<li><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>							
							<?php endwhile;	?>
						</ul>
						<?php
					}				
					wp_reset_query();				
					?>
				</div>
				<?php comments_template(); ?>
			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>