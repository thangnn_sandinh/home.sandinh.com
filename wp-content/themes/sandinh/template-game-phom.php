<?php
/**
 * Template Name: Game Phỏm
 *
 * Game Phỏm template
 *
 * @package    sandinh
 * @subpackage
 * @since
 */
get_header(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
			//while ( have_posts() ) : the_post();
			//echo '<h1>'.get_the_title().'</h1>';
			//the_content();
			//endwhile;
			$user_id = getXfUserId();
			?>
			<?php if($user_id>0): ?>
				<script src="<?php echo get_template_directory_uri(); ?>/js/swfobject.js"></script>
				<script src="<?php echo get_template_directory_uri(); ?>/js/sd/sd.js"></script>
			<?php
			// JSON URL which should be requested
			$json_url = 'http://dev.sandinh.com/api/user/get-name/'.$user_id;
			// Initializing curl
			$ch = curl_init( $json_url );
			// Configuring curl options
			$options = array(
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
			);
			// Setting curl options
			curl_setopt_array( $ch, $options );
			// Getting results
			$result =  curl_exec($ch); // Getting JSON result string

			$user = json_decode($result);
			// close curl resource to free up system resources
			curl_close($ch);
			?>
				<script>
					jQuery(document).ready(function(){
						SD.Game.init(
							<?php
                            $testers = array(10,11,16,17);
                            if(in_array($user_id, $testers)): ?>
							'http://home.sandinh.com/ppr/173/Ldr.swf',
							<?php else: ?>
							'http://home.sandinh.com/ppr/173/Ldr.swf',
							<?php endif; ?>
							{
								cid: 0,
								uid: <?php echo $user_id; ?>,
								name: "<?php echo $user->name; ?>",
								gender: "N",
								now: <?php echo time(); ?>,
								x: "i7na8DgmYBCQgNkV36EAWzP31DUPkCbAFiUV1P2iyxo="
							});
					})
				</script>

				<div id="flashObj">
					<div id="flashVersion"></div>
					<div id="chromeInstall" style="display:none">
						Cách đơn giản nhất để cài Flash là dùng trình duyệt Google Chrome (đã tích hợp sẵn Flash).<br/>
						<button type="button" onclick="location.href='http://www.google.com/intl/vi/chrome/browser/'"
								style="color:red;font-weight:bold;font-strength:large">BẤM VÀO ĐÂY
						</button>
						để cài Chrome (tiếng Việt)
					</div>
					<div style="margin-top:15px;">
						Nếu vẫn không làm được thì bạn hãy vào xem bài viết này: <a href="/threads/2241">Làm thế nào để cài Chrome</a><br/>
						Hoặc gọi điện thoại cho hỗ trợ theo số: 0963064161, 0912470101, hoặc 0904768646
					</div>
				</div>
			<?php else: ?>
<!--				Bạn phải đăng nhập mới vào chơi được!-->
			<div id="login-to-play">
				<h2><?php echo _x('Đăng nhập'); ?></h2>
				<?php
				$args = array(
					'echo'           => true,
					'remember'       => true,
					//'redirect'       => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
					'redirect'       => get_site_url().'/choi-chan/',
					'form_id'        => 'loginform',
					'id_username'    => 'user_login',
					'id_password'    => 'user_pass',
					'id_remember'    => 'rememberme',
					'id_submit'      => 'wp-submit',
					'label_username' => __( 'Tên đăng nhập' ),
					'label_password' => __( 'Mật khẩu' ),
					'label_remember' => __( 'Nhớ đăng nhập' ),
					'label_log_in'   => __( 'Đăng nhập' ),
					'value_username' => '',
					'value_remember' => false
				);
				wp_login_form($args);
				?>
				<a class="simplemodal-register" href="<?php echo wp_registration_url(); ?>"><?php echo _x('Đăng ký'); ?></a>
			</div>
			<?php endif; ?>
		</div><!-- #content -->
	</div><!-- #primary --><?php get_footer(); ?>