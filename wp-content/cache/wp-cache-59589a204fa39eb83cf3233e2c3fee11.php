<?php die(); ?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="vi">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="vi">
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html lang="vi">
<!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<title>Thư viện video | Sân Đình Game Studio</title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="http://home.sandinh.com/xmlrpc.php">
	<!--[if lt IE 9]>
	<script src="http://home.sandinh.com/wp-content/themes/twentythirteen/js/html5.js"></script>
	<![endif]-->
	<link rel="alternate" type="application/rss+xml" title="Dòng thông tin Sân Đình Game Studio &raquo;" href="http://home.sandinh.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Dòng phản hồi Sân Đình Game Studio &raquo;" href="http://home.sandinh.com/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/home.sandinh.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.3.1"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext%2Cvietnamese&#038;ver=4.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='http://home.sandinh.com/wp-includes/css/dashicons.min.css?ver=4.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='admin-bar-css'  href='http://home.sandinh.com/wp-includes/css/admin-bar.min.css?ver=4.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='http://home.sandinh.com/wp-content/plugins/js_composer/assets/css/js_composer.css?ver=4.7.4' type='text/css' media='all' />
<link rel='stylesheet' id='twentythirteen-fonts-css'  href='https://fonts.googleapis.com/css?family=Source+Sans+Pro%3A300%2C400%2C700%2C300italic%2C400italic%2C700italic%7CBitter%3A400%2C700&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='genericons-css'  href='http://home.sandinh.com/wp-content/themes/twentythirteen/genericons/genericons.css?ver=3.03' type='text/css' media='all' />
<link rel='stylesheet' id='twentythirteen-style-css'  href='http://home.sandinh.com/wp-content/themes/twentythirteen/style.css?ver=2013-07-18' type='text/css' media='all' />
<!--[if lt IE 9]>
<link rel='stylesheet' id='twentythirteen-ie-css'  href='http://home.sandinh.com/wp-content/themes/twentythirteen/css/ie.css?ver=2013-07-18' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='js_composer_custom_css-css'  href='//home.sandinh.com/wp-content/uploads/js_composer/custom.css?ver=4.7.4' type='text/css' media='screen' />
<link rel='stylesheet' id='bsf-Defaults-css'  href='http://home.sandinh.com/wp-content/uploads/smile_fonts/Defaults/Defaults.css?ver=4.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='__EPYT__style-css'  href='http://home.sandinh.com/wp-content/plugins/youtube-embed-plus/styles/ytprefs.min.css?ver=4.3.1' type='text/css' media='all' />
<style id='__EPYT__style-inline-css' type='text/css'>

                .epyt-gallery-thumb {
                        width: 33.333%;
                }
</style>
<link rel='stylesheet' id='simple-pagination-css-css'  href='http://home.sandinh.com/wp-content/plugins/simple-pagination/css/default.css?ver=2.1.7' type='text/css' media='screen' />
<link rel='stylesheet' id='simplemodal-login-css'  href='http://home.sandinh.com/wp-content/plugins/simplemodal-login/css/default.css?ver=1.1' type='text/css' media='screen' />
<script type='text/javascript' src='http://home.sandinh.com/wp-includes/js/jquery/jquery.js?ver=1.11.3'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='http://dev.sandinh.com/xf_api/index.php?assets/sdk&#038;prefix=xfac&#038;ver=4.3.1'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-content/plugins/xenforo-api-consumer/js/script.js?ver=4.3.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _EPYT_ = {"ajaxurl":"http:\/\/home.sandinh.com\/wp-admin\/admin-ajax.php","security":"52a1c524a9","gallery_scrolloffset":"20","eppathtoscripts":"http:\/\/home.sandinh.com\/wp-content\/plugins\/youtube-embed-plus\/scripts\/","epresponsiveselector":"[\"iframe.__youtube_prefs_widget__\"]","epdovol":"1","version":"10.8"};
/* ]]> */
</script>
<script type='text/javascript' src='http://home.sandinh.com/wp-content/plugins/youtube-embed-plus/scripts/ytprefs.min.js?ver=4.3.1'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://home.sandinh.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://home.sandinh.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.3.1" />
<link rel='canonical' href='http://home.sandinh.com/thu-vien-video/' />
<link rel='shortlink' href='http://home.sandinh.com/?p=3150' />
<script type="text/javascript">var $TS_VCSC_Lightbox_Activated = true;var $TS_VCSC_Lightbox_Thumbs = "bottom";var $TS_VCSC_Lightbox_Thumbsize = 50;var $TS_VCSC_Lightbox_Animation = "random";var $TS_VCSC_Lightbox_Captions = "data-title";var $TS_VCSC_Lightbox_Closer = true;var $TS_VCSC_Lightbox_Durations = 5000;var $TS_VCSC_Lightbox_Share = false;var $TS_VCSC_Lightbox_LoadAPIs = true;var $TS_VCSC_Lightbox_Social = "fb,tw,gp,pin";var $TS_VCSC_Lightbox_NoTouch = false;var $TS_VCSC_Lightbox_BGClose = true;var $TS_VCSC_Lightbox_NoHashes = true;var $TS_VCSC_Lightbox_Keyboard = true;var $TS_VCSC_Lightbox_FullScreen = true;var $TS_VCSC_Lightbox_Zoom = true;var $TS_VCSC_Lightbox_FXSpeed = 300;var $TS_VCSC_Lightbox_Scheme = "dark";var $TS_VCSC_Lightbox_Backlight = "#ffffff";var $TS_VCSC_Lightbox_UseColor = false;var $TS_VCSC_Lightbox_Overlay = "#000000";var $TS_VCSC_Lightbox_Background = "";var $TS_VCSC_Lightbox_Repeat = "no-repeat";var $TS_VCSC_Lightbox_Noise = "";var $TS_VCSC_Lightbox_CORS = false;var $TS_VCSC_Lightbox_Tapping = true;var $TS_VCSC_Lightbox_ScrollBlock = "css";var $TS_VCSC_Lightbox_LastScroll = 0;var $TS_VCSC_Countdown_DaysLabel = "Days";var $TS_VCSC_Countdown_DayLabel = "Day";var $TS_VCSC_Countdown_HoursLabel = "Hours";var $TS_VCSC_Countdown_HourLabel = "Hour";var $TS_VCSC_Countdown_MinutesLabel = "Minutes";var $TS_VCSC_Countdown_MinuteLabel = "Minute";var $TS_VCSC_Countdown_SecondsLabel = "Seconds";var $TS_VCSC_Countdown_SecondLabel = "Second";var $TS_VCSC_SmoothScrollActive = false;</script><script>window.xfacClientId = "zyhdrie68o";window.xfacWpLogout = "http:\/\/home.sandinh.com\/wp-login.php?action=logout&_wpnonce=23205904a6";</script><style type="text/css">
.qtranxs_flag_vi {background-image: url(http://home.sandinh.com/wp-content/plugins/qtranslate-x/flags/vn.png); background-repeat: no-repeat;}
</style>
<link hreflang="vi" href="http://home.sandinh.com/thu-vien-video/?lang=vi" rel="alternate" />
<link hreflang="x-default" href="http://home.sandinh.com/thu-vien-video/" rel="alternate" />
<meta name="generator" content="qTranslate-X 3.4.6.4" />
<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://home.sandinh.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="http://home.sandinh.com/wp-content/plugins/js_composer/assets/css/vc-ie8.css" media="screen"><![endif]-->	<style type="text/css" id="twentythirteen-header-css">
			.site-title,
		.site-description {
			position: absolute;
			clip: rect(1px 1px 1px 1px); /* IE7 */
			clip: rect(1px, 1px, 1px, 1px);
		}
			.site-header .home-link {
			min-height: 0;
		}
		</style>
	<style type="text/css" media="print">#wpadminbar { display:none; }</style>
<style type="text/css" media="screen">
	html { margin-top: 32px !important; }
	* html body { margin-top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 46px !important; }
		* html body { margin-top: 46px !important; }
	}
</style>
<noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>	<link rel="stylesheet" href="http://home.sandinh.com/wp-content/themes/twentythirteen/sidr/stylesheets/jquery.sidr.dark.css">
	<script src="http://home.sandinh.com/wp-content/themes/twentythirteen/sidr/jquery.sidr.min.js"></script>
	<!--<script src="http://home.sandinh.com/wp-content/themes/twentythirteen/js/modal.js"></script>-->
</head>

<body class="page page-id-3150 page-template page-template-template-nosidebar page-template-template-nosidebar-php logged-in admin-bar no-customize-support sidebar wpb-js-composer js-comp-ver-4.7.4 vc_responsive">
	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">
			<a id="simple-menu" href="#sidr" style="display: none;">Toggle menu</a>
			<div id="sidr" style="display: none;">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<div class="menu-main-menu-container"><ul id="sidebar-menu" class="sidebar-menu"><li id="menu-item-3057" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3057"><a href="http://home.sandinh.com/">Trang chủ</a></li>
<li id="menu-item-3067" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3067"><a href="http://home.sandinh.com/category/tong-hop/tin-tuc/">Tin tức</a></li>
<li id="menu-item-3129" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3129"><a href="http://dev.sandinh.com">Cộng đồng</a>
<ul class="sub-menu">
	<li id="menu-item-3058" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3058"><a href="http://dev.sandinh.com">Diễn đàn</a></li>
	<li id="menu-item-3130" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3130"><a href="https://www.facebook.com/sandinh.game/">Fanpage</a></li>
</ul>
</li>
<li id="menu-item-3060" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3060"><a href="#">Chơi game</a>
<ul class="sub-menu">
	<li id="menu-item-3073" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3073"><a href="http://home.sandinh.com/choi-chan/">Chơi chắn</a></li>
	<li id="menu-item-3076" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3076"><a href="http://home.sandinh.com/choi-phom/">Chơi phỏm</a></li>
</ul>
</li>
<li id="menu-item-3062" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3062"><a href="http://home.sandinh.com/ngan-hang/">Ngân hàng</a></li>
<li id="menu-item-3061" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3061"><a href="http://home.sandinh.com/bang-vang/">Bảng vàng</a></li>
<li id="menu-item-3065" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3065"><a href="http://home.sandinh.com/category/tong-hop/huong-dan/">Hướng Dẫn</a></li>
</ul></div>				</nav>
			</div>

			<div id="navigation" class="navbar">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<button class="menu-toggle">Trình đơn</button>
					<a class="screen-reader-text skip-link" href="#content" title="Chuyển đến phần nội dung">Chuyển đến phần nội dung</a>
					<div class="menu-main-menu-container"><ul id="primary-menu" class="nav-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3057"><a href="http://home.sandinh.com/">Trang chủ</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3067"><a href="http://home.sandinh.com/category/tong-hop/tin-tuc/">Tin tức</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3129"><a href="http://dev.sandinh.com">Cộng đồng</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3058"><a href="http://dev.sandinh.com">Diễn đàn</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3130"><a href="https://www.facebook.com/sandinh.game/">Fanpage</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3060"><a href="#">Chơi game</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3073"><a href="http://home.sandinh.com/choi-chan/">Chơi chắn</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3076"><a href="http://home.sandinh.com/choi-phom/">Chơi phỏm</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3062"><a href="http://home.sandinh.com/ngan-hang/">Ngân hàng</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3061"><a href="http://home.sandinh.com/bang-vang/">Bảng vàng</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3065"><a href="http://home.sandinh.com/category/tong-hop/huong-dan/">Hướng Dẫn</a></li>
</ul></div>									</nav><!-- #site-navigation -->
			</div><!-- #navbar -->

			<script>
			jQuery.noConflict();
			jQuery(document).ready(function() {
			  jQuery('#simple-menu').sidr();
			});
			</script>
		</header><!-- #masthead -->

		<div id="main" class="site-main">
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
		<iframe  id="_ytid_25823" width="604" height="368" src="//www.youtube.com/embed/?enablejsapi=1&listType=playlist&list=UUIMmvyinwDgAiOyKll_BsLQ&autoplay=0&cc_load_policy=0&iv_load_policy=1&loop=0&modestbranding=0&rel=1&showinfo=1&playsinline=0&autohide=2&theme=dark&color=red&wmode=opaque&vq=&controls=2&" frameborder="0" type="text/html" class="__youtube_prefs__" allowfullscreen webkitallowfullscreen mozallowfullscreen ></iframe>
		</div><!-- #content -->
	</div><!-- #primary -->

﻿
				</div><!-- #main -->

		<footer id="colophon" class="site-footer" role="contentinfo">	
			<aside id="epx_vcsb_widget_2639" class="widget ERROPiX\VC_Sidebar_Editor\Main"><div class="vc_row wpb_row vc_row-fluid seo-text"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  seo-text">
		<div class="wpb_wrapper">
			<p>Game danh chan online hay nhat game poker online mien phi choi tien len mien nam online mien phi game bai ta la, danh phom truc tuyen choi game danh co tuong truc tuyen mien phi Choi game xi to online mien phi game bai sam loc online hay nhat mau binh, binh xap xam online hay nhat danh co tuong up online mien phi.</p>

		</div>
	</div>
</div></div></div><div class="vc_row wpb_row vc_row-fluid footer-text"><div class="wpb_column vc_column_container vc_col-sm-3"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  copyright">
		<div class="wpb_wrapper">
			<p>Phát triển bởi sandinh.com @ 2012-2015</p>

		</div>
	</div>
</div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="wpb_wrapper"><div class="vc_wp_custommenu wpb_content_element footer-menu"><div class="widget widget_nav_menu"><div class="menu-footer-menu-container"><ul id="menu-footer-menu" class="menu"><li id="menu-item-2977" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2977"><a href="http://home.sandinh.com/">Trang chủ</a></li>
<li id="menu-item-2632" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2632"><a href="#">Tin tức</a></li>
<li id="menu-item-2633" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2633"><a>Chơi game</a></li>
<li id="menu-item-2634" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2634"><a href="#">Ngân hàng</a></li>
<li id="menu-item-2635" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2635"><a href="#">Bản Mobile</a></li>
<li id="menu-item-2636" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2636"><a href="#">Đấu trường</a></li>
<li id="menu-item-2637" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2637"><a href="#">Quy định</a></li>
</ul></div></div></div></div></div><div class="footer-logo-wrap wpb_column vc_column_container vc_col-sm-3"><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_center   footer-logo">
		<div class="wpb_wrapper">
			
			<a href="index.php" target="_self"><div class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img " src="http://home.sandinh.com/wp-content/uploads/2015/09/footer-logo.png" width="147" height="30" alt="footer-logo" title="footer-logo" /></div></a>
		</div>
	</div>
</div></div></div>
</aside>		</footer><!-- #colophon -->
	</div><!-- #page -->

	<div id="simplemodal-login-form" style="display:none">
<form name="registerform" id="registerform" action="http://home.sandinh.com/wp-login.php?action=register" id="loginform" method="post">
	<div class="modal-header">
	<div class="title">Đăng ký</div>
	<input type="button" class="simplemodal-close" value="x" tabindex="101">
	</div>
	<div class="simplemodal-login-fields">
	<p>
		<label>Tên đăng nhập<br />
		<input type="text" name="user_login" class="user_login input" value="" size="20" tabindex="10" /></label>
	</p>
	<p>
		<label>E-mail<br />
		<input type="text" name="user_email" class="user_email input" value="" size="25" tabindex="20" /></label>
	</p><!--	Start code from Cimy User Extra Fields 2.6.5	Copyright (c) 2006-2015 Marco Cimmino	http://www.marcocimmino.net/cimy-wordpress-plugins/cimy-user-extra-fields/	-->
	<input type="hidden" name="cimy_post" value="1" />
	<p id="cimy_uef_wp_p_field_1">
		<label for="cimy_uef_wp_1">Mật khẩu</label><input type="password" name="cimy_uef_wp_PASSWORD" id="cimy_uef_wp_1" class="cimy_uef_input_27" value="" maxlength="100" />
	</p>
	<p id="cimy_uef_wp_p_field_2">
		<label for="cimy_uef_wp_2">Xác nhận mật khẩu</label><input type="password" name="cimy_uef_wp_PASSWORD2" id="cimy_uef_wp_2" class="cimy_uef_input_27" value="" maxlength="100" />
	</p>
	<p id="cimy_uef_p_field_1">
		<label for="cimy_uef_1">Số điện thoại</label><input type="text" name="cimy_uef_PHONE" id="cimy_uef_1" class="cimy_uef_input_27" value="" maxlength="20" />
	</p>
	<p id="cimy_uef_p_field_4">
		<label for="cimy_uef_4">Giới tính</label><select name="cimy_uef_GENDER" id="cimy_uef_4" class="cimy_uef_input_27">
			<option value="nam">nam</option>
			<option value="nữ">nữ</option>
			<option value="không xác định">không xác định</option></select>
	</p>
	<br />
<!--	End of code from Cimy User Extra Fields	-->
<div class='g-recaptcha' data-sitekey='6LcqYg4TAAAAAKj8kFm76J4QsTLX0MaALdNUcp-d' data-theme='light' data-size='normal'></div>
	<p class="reg_passmail"></p>
	<p class="submit">
		<input type="submit" name="wp-submit" value="Đăng ký" tabindex="100" />
		<input type="button" class="simplemodal-close" value="Hủy" tabindex="101" />
	</p>
	<p class="nav">
		<a class="simplemodal-login" href=""></a>
	</p>
	</div>
	<div class="simplemodal-login-activity" style="display:none;"></div>
</form></div><script type='text/javascript' src='http://home.sandinh.com/wp-includes/js/admin-bar.min.js?ver=4.3.1'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-content/plugins/youtube-embed-plus/scripts/fitvids.min.js?ver=4.3.1'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-includes/js/masonry.min.js?ver=3.1.2'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-includes/js/jquery/jquery.masonry.min.js?ver=3.1.2'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-content/themes/twentythirteen/js/functions.js?ver=20150330'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-content/plugins/simplemodal-login/js/jquery.simplemodal.js?ver=1.4.3'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var SimpleModalLoginL10n = {"shortcut":"false","logged_in":"true","admin_url":"http:\/\/home.sandinh.com\/wp-admin\/","empty_username":"<strong>ERROR<\/strong>: The username field is empty.","empty_password":"<strong>ERROR<\/strong>: The password field is empty.","empty_email":"<strong>ERROR<\/strong>: The email field is empty.","empty_all":"<strong>ERROR<\/strong>: All fields are required."};
/* ]]> */
</script>
<script type='text/javascript' src='http://home.sandinh.com/wp-content/plugins/simplemodal-login/js/default.js?ver=1.1'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-content/plugins/js_composer/assets/js/js_composer_front.js?ver=4.7.4'></script>
<script type='text/javascript' src='https://www.google.com/recaptcha/api.js?ver=2.0'></script>
	<script type="text/javascript">
		(function() {
			var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

			request = true;

			b[c] = b[c].replace( rcs, ' ' );
			b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
		}());
	</script>
			<div id="wpadminbar" class="nojq nojs">
							<a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">Chuyển đến thanh công cụ</a>
						<div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Thanh công cụ" tabindex="0">
				<ul id="wp-admin-bar-root-default" class="ab-top-menu">
		<li id="wp-admin-bar-wp-logo" class="menupop"><a class="ab-item"  aria-haspopup="true" href="http://home.sandinh.com/wp-admin/about.php"><span class="ab-icon"></span><span class="screen-reader-text">Giới thiệu về WordPress</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wp-logo-default" class="ab-submenu">
		<li id="wp-admin-bar-about"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/about.php">Giới thiệu về WordPress</a>		</li></ul><ul id="wp-admin-bar-wp-logo-external" class="ab-sub-secondary ab-submenu">
		<li id="wp-admin-bar-wporg"><a class="ab-item"  href="https://wordpress.org/">WordPress.org</a>		</li>
		<li id="wp-admin-bar-documentation"><a class="ab-item"  href="https://codex.wordpress.org/">Tài liệu</a>		</li>
		<li id="wp-admin-bar-support-forums"><a class="ab-item"  href="https://wordpress.org/support/">Diễn đàn hỗ trợ</a>		</li>
		<li id="wp-admin-bar-feedback"><a class="ab-item"  href="https://wordpress.org/support/forum/requests-and-feedback">Thông tin phản hồi</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-site-name" class="menupop"><a class="ab-item"  aria-haspopup="true" href="http://home.sandinh.com/wp-admin/">Sân Đình Game Studio</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-site-name-default" class="ab-submenu">
		<li id="wp-admin-bar-dashboard"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/">Bảng tin</a>		</li></ul><ul id="wp-admin-bar-appearance" class="ab-submenu">
		<li id="wp-admin-bar-themes"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/themes.php">Giao diện</a>		</li>
		<li id="wp-admin-bar-widgets"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/widgets.php">Widget</a>		</li>
		<li id="wp-admin-bar-menus"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/nav-menus.php">Menu</a>		</li>
		<li id="wp-admin-bar-header" class="hide-if-customize"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/themes.php?page=custom-header">Header</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-customize" class="hide-if-no-customize"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/customize.php?url=http%3A%2F%2Fhome.sandinh.com%2Fthu-vien-video%2F">Tùy chỉnh</a>		</li>
		<li id="wp-admin-bar-updates"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/update-core.php" title="1 cập nhật cho plugin"><span class="ab-icon"></span><span class="ab-label">1</span><span class="screen-reader-text">1 cập nhật cho plugin</span></a>		</li>
		<li id="wp-admin-bar-comments"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/edit-comments.php" title="0 bình luận đang đợi xét duyệt"><span class="ab-icon"></span><span id="ab-awaiting-mod" class="ab-label awaiting-mod pending-count count-0">0</span></a>		</li>
		<li id="wp-admin-bar-new-content" class="menupop"><a class="ab-item"  aria-haspopup="true" href="http://home.sandinh.com/wp-admin/post-new.php"><span class="ab-icon"></span><span class="ab-label">Mới</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-new-content-default" class="ab-submenu">
		<li id="wp-admin-bar-new-post"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/post-new.php">Bài viết</a>		</li>
		<li id="wp-admin-bar-new-media"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/media-new.php">Tập tin</a>		</li>
		<li id="wp-admin-bar-new-page"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/post-new.php?post_type=page">Trang</a>		</li>
		<li id="wp-admin-bar-new-user"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/user-new.php">Thành viên</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-edit" class="menupop"><a class="ab-item"  aria-haspopup="true" href="http://home.sandinh.com/wp-admin/post.php?post=3150&#038;action=edit">Sửa trang</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-edit-default" class="ab-submenu">
		<li id="wp-admin-bar-new_draft"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/admin.php?action=duplicate_post_save_as_new_post_draft&#038;post=3150">Copy to a new draft</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-TS_VCSC_Notification"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/index.php?page=TS_VCSC_Notification"><span>Composium - Visual Composer Extensions <span id="ab-updates" style="color: #ffffff; background-color: #d54e21;">New Version!</span></span></a>		</li>
		<li id="wp-admin-bar-vc_inline-admin-bar-link" class="vc_inline-link"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/post.php?vc_action=vc_inline&#038;post_id=3150&#038;post_type=page">Edit with Visual Composer</a>		</li></ul><ul id="wp-admin-bar-top-secondary" class="ab-top-secondary ab-top-menu">
		<li id="wp-admin-bar-search" class="admin-bar-search"><div class="ab-item ab-empty-item" tabindex="-1"><form action="http://home.sandinh.com/" method="get" id="adminbarsearch"><input class="adminbar-input" name="s" id="adminbar-search" type="text" value="" maxlength="150" /><label for="adminbar-search" class="screen-reader-text">Tìm kiếm</label><input type="submit" class="adminbar-button" value="Tìm kiếm"/></form></div>		</li>
		<li id="wp-admin-bar-my-account" class="menupop with-avatar"><a class="ab-item"  aria-haspopup="true" href="http://home.sandinh.com/wp-admin/profile.php">Chào bạn, sdio<img alt='' src='http://1.gravatar.com/avatar/70d8e1a1c3cd561a8f0be227962263ff?s=26&#038;d=mm&#038;r=g' srcset='http://1.gravatar.com/avatar/70d8e1a1c3cd561a8f0be227962263ff?s=52&amp;d=mm&amp;r=g 2x' class='avatar avatar-26 photo' height='26' width='26' /></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-user-actions" class="ab-submenu">
		<li id="wp-admin-bar-user-info"><a class="ab-item" tabindex="-1" href="http://home.sandinh.com/wp-admin/profile.php"><img alt='' src='http://1.gravatar.com/avatar/70d8e1a1c3cd561a8f0be227962263ff?s=64&#038;d=mm&#038;r=g' srcset='http://1.gravatar.com/avatar/70d8e1a1c3cd561a8f0be227962263ff?s=128&amp;d=mm&amp;r=g 2x' class='avatar avatar-64 photo' height='64' width='64' /><span class='display-name'>sdio</span><span class='username'>cpradmin</span></a>		</li>
		<li id="wp-admin-bar-edit-profile"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/profile.php">Chỉnh sửa Hồ sơ của tôi</a>		</li>
		<li id="wp-admin-bar-logout"><a class="ab-item"  href="http://home.sandinh.com/wp-login.php?action=logout&#038;_wpnonce=23205904a6">Đăng xuất</a>		</li></ul></div>		</li></ul>			</div>
						<a class="screen-reader-shortcut" href="http://home.sandinh.com/wp-login.php?action=logout&#038;_wpnonce=23205904a6">Đăng xuất</a>
					</div>

		</body>
</html>
<!-- Dynamic page generated in 0.953 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2015-11-04 11:16:59 -->
