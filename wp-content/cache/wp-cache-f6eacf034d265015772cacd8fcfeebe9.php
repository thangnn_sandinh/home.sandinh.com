<?php die(); ?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="vi">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="vi">
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html lang="vi">
<!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<title>Sân Đình Game Studio | Sân Đình Game Studio</title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="http://home.sandinh.com/xmlrpc.php">
	<!--[if lt IE 9]>
	<script src="http://home.sandinh.com/wp-content/themes/twentythirteen/js/html5.js"></script>
	<![endif]-->
	<script type="text/javascript">var baseurl = "http://home.sandinh.com";var adminurl = "http://home.sandinh.com/wp-admin/";</script><link rel="alternate" type="application/rss+xml" title="Dòng thông tin Sân Đình Game Studio &raquo;" href="http://home.sandinh.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Dòng phản hồi Sân Đình Game Studio &raquo;" href="http://home.sandinh.com/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/home.sandinh.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.3.1"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext%2Cvietnamese&#038;ver=4.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='http://home.sandinh.com/wp-includes/css/dashicons.min.css?ver=4.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='admin-bar-css'  href='http://home.sandinh.com/wp-includes/css/admin-bar.min.css?ver=4.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='videogallery_css-css'  href='http://home.sandinh.com/wp-content/plugins/contus-video-gallery/css/style.min.css?ver=4.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='http://home.sandinh.com/wp-content/plugins/js_composer/assets/css/js_composer.css?ver=4.7.4' type='text/css' media='all' />
<link rel='stylesheet' id='twentythirteen-fonts-css'  href='https://fonts.googleapis.com/css?family=Source+Sans+Pro%3A300%2C400%2C700%2C300italic%2C400italic%2C700italic%7CBitter%3A400%2C700&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='genericons-css'  href='http://home.sandinh.com/wp-content/themes/twentythirteen/genericons/genericons.css?ver=3.03' type='text/css' media='all' />
<link rel='stylesheet' id='twentythirteen-style-css'  href='http://home.sandinh.com/wp-content/themes/twentythirteen/style.css?ver=2013-07-18' type='text/css' media='all' />
<!--[if lt IE 9]>
<link rel='stylesheet' id='twentythirteen-ie-css'  href='http://home.sandinh.com/wp-content/themes/twentythirteen/css/ie.css?ver=2013-07-18' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='js_composer_custom_css-css'  href='//home.sandinh.com/wp-content/uploads/js_composer/custom.css?ver=4.7.4' type='text/css' media='screen' />
<link rel='stylesheet' id='bsf-Defaults-css'  href='http://home.sandinh.com/wp-content/uploads/smile_fonts/Defaults/Defaults.css?ver=4.3.1' type='text/css' media='all' />
<link rel='stylesheet' id='ultimate-style-css'  href='http://home.sandinh.com/wp-content/plugins/Ultimate_VC_Addons/assets/min-css/style.min.css?ver=3.13.5' type='text/css' media='all' />
<link rel='stylesheet' id='ult-btn-css'  href='http://home.sandinh.com/wp-content/plugins/Ultimate_VC_Addons/modules/../assets/min-css/advanced-buttons.min.css?ver=3.13.5' type='text/css' media='' />
<link rel='stylesheet' id='simple-pagination-css-css'  href='http://home.sandinh.com/wp-content/plugins/simple-pagination/css/default.css?ver=2.1.7' type='text/css' media='screen' />
<link rel='stylesheet' id='simplemodal-login-css'  href='http://home.sandinh.com/wp-content/plugins/simplemodal-login/css/default.css?ver=1.1' type='text/css' media='screen' />
<script type='text/javascript' src='http://home.sandinh.com/wp-content/plugins/contus-video-gallery/js/script.min.js?ver=4.3.1'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-includes/js/jquery/jquery.js?ver=1.11.3'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='http://dev.sandinh.com/xf_api/index.php?assets/sdk&#038;prefix=xfac&#038;ver=4.3.1'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-content/plugins/xenforo-api-consumer/js/script.js?ver=4.3.1'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://home.sandinh.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://home.sandinh.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.3.1" />
<link rel='canonical' href='http://home.sandinh.com/' />
<link rel='shortlink' href='http://home.sandinh.com/' />
<script type="text/javascript">var $TS_VCSC_Lightbox_Activated = true;var $TS_VCSC_Lightbox_Thumbs = "bottom";var $TS_VCSC_Lightbox_Thumbsize = 50;var $TS_VCSC_Lightbox_Animation = "random";var $TS_VCSC_Lightbox_Captions = "data-title";var $TS_VCSC_Lightbox_Closer = true;var $TS_VCSC_Lightbox_Durations = 5000;var $TS_VCSC_Lightbox_Share = false;var $TS_VCSC_Lightbox_LoadAPIs = true;var $TS_VCSC_Lightbox_Social = "fb,tw,gp,pin";var $TS_VCSC_Lightbox_NoTouch = false;var $TS_VCSC_Lightbox_BGClose = true;var $TS_VCSC_Lightbox_NoHashes = true;var $TS_VCSC_Lightbox_Keyboard = true;var $TS_VCSC_Lightbox_FullScreen = true;var $TS_VCSC_Lightbox_Zoom = true;var $TS_VCSC_Lightbox_FXSpeed = 300;var $TS_VCSC_Lightbox_Scheme = "dark";var $TS_VCSC_Lightbox_Backlight = "#ffffff";var $TS_VCSC_Lightbox_UseColor = false;var $TS_VCSC_Lightbox_Overlay = "#000000";var $TS_VCSC_Lightbox_Background = "";var $TS_VCSC_Lightbox_Repeat = "no-repeat";var $TS_VCSC_Lightbox_Noise = "";var $TS_VCSC_Lightbox_CORS = false;var $TS_VCSC_Lightbox_Tapping = true;var $TS_VCSC_Lightbox_ScrollBlock = "css";var $TS_VCSC_Lightbox_LastScroll = 0;var $TS_VCSC_Countdown_DaysLabel = "Days";var $TS_VCSC_Countdown_DayLabel = "Day";var $TS_VCSC_Countdown_HoursLabel = "Hours";var $TS_VCSC_Countdown_HourLabel = "Hour";var $TS_VCSC_Countdown_MinutesLabel = "Minutes";var $TS_VCSC_Countdown_MinuteLabel = "Minute";var $TS_VCSC_Countdown_SecondsLabel = "Seconds";var $TS_VCSC_Countdown_SecondLabel = "Second";var $TS_VCSC_SmoothScrollActive = false;</script><script>window.xfacClientId = "zyhdrie68o";window.xfacWpLogout = "http:\/\/home.sandinh.com\/wp-login.php?action=logout&_wpnonce=6c8037ef53";</script><style type="text/css">
.qtranxs_flag_vi {background-image: url(http://home.sandinh.com/wp-content/plugins/qtranslate-x/flags/vn.png); background-repeat: no-repeat;}
</style>
<link hreflang="vi" href="http://home.sandinh.com/?lang=vi" rel="alternate" />
<link hreflang="x-default" href="http://home.sandinh.com/" rel="alternate" />
<meta name="generator" content="qTranslate-X 3.4.6.4" />
<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://home.sandinh.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="http://home.sandinh.com/wp-content/plugins/js_composer/assets/css/vc-ie8.css" media="screen"><![endif]-->	<style type="text/css" id="twentythirteen-header-css">
			.site-title,
		.site-description {
			position: absolute;
			clip: rect(1px 1px 1px 1px); /* IE7 */
			clip: rect(1px, 1px, 1px, 1px);
		}
			.site-header .home-link {
			min-height: 0;
		}
		</style>
	<style type="text/css" media="print">#wpadminbar { display:none; }</style>
<style type="text/css" media="screen">
	html { margin-top: 32px !important; }
	* html body { margin-top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 46px !important; }
		* html body { margin-top: 46px !important; }
	}
</style>
<style type="text/css" data-type="vc_custom-css">.menu_banner{
    margin-left:-60px; 
    margin-right:-60px; 
    
}
.menu_butom{
    margin-left:-20px; 
    margin-right:-20px; 
    margin-top:-55px;

}
.content-section-detail {
    /*background : #eeeeee; */
    padding : 10px;
}
.content-section-sidebar{
    /*background : #eeeeee;*/
}
.ubtn1 {
    margin-top : -10px;
}
.ubtn_login {
    width:100%;
}
.ubtn_support{
     width:100%;
}
.ubtn_topuser{
     width :100%;
     margin-top :-100px;
}

/* mobile first */
.header1 { display: block; }
#body> section {
    background-repeat:no-repeat;
    background-position: center center;
    background-image:url("http://home.sandinh.com//wp-content/uploads/2015/09/bg.jpg");
    min-height:100%;
    
}
@media (max-width: 599px) {
    .header1 { display: none; }
    #body> section { background : #ffffff;}
    .footer1 {display:none;}
}
</style><style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1443062306184{background-color: #eeeeee !important;}.vc_custom_1442999210985{margin-top: 0px !important;}.vc_custom_1443070452639{margin-bottom: 10px !important;}.vc_custom_1443070465078{margin-bottom: 10px !important;}</style><noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>	<link rel="stylesheet" href="http://home.sandinh.com/wp-content/themes/twentythirteen/sidr/stylesheets/jquery.sidr.dark.css">
	<script src="http://home.sandinh.com/wp-content/themes/twentythirteen/sidr/jquery.sidr.min.js"></script>
	<!--<script src="http://home.sandinh.com/wp-content/themes/twentythirteen/js/modal.js"></script>-->
</head>

<body class="home page page-id-2114 page-template-default logged-in admin-bar no-customize-support sidebar wpb-js-composer js-comp-ver-4.7.4 vc_responsive">
	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">
			<a id="simple-menu" href="#sidr" style="display: none;">Toggle menu</a>
			<div id="sidr" style="display: none;">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<div class="menu-main-menu-container"><ul id="sidebar-menu" class="sidebar-menu"><li id="menu-item-3057" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2114 current_page_item menu-item-3057"><a href="http://home.sandinh.com/">Trang chủ</a></li>
<li id="menu-item-3067" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3067"><a href="http://home.sandinh.com/category/tong-hop/tin-tuc/">Tin tức</a></li>
<li id="menu-item-3129" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3129"><a href="http://dev.sandinh.com">Cộng đồng</a>
<ul class="sub-menu">
	<li id="menu-item-3058" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3058"><a href="http://dev.sandinh.com">Diễn đàn</a></li>
	<li id="menu-item-3130" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3130"><a href="https://www.facebook.com/sandinh.game/">Fanpage</a></li>
</ul>
</li>
<li id="menu-item-3060" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3060"><a href="#">Chơi game</a>
<ul class="sub-menu">
	<li id="menu-item-3073" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3073"><a href="http://home.sandinh.com/choi-chan/">Chơi chắn</a></li>
	<li id="menu-item-3076" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3076"><a href="http://home.sandinh.com/choi-phom/">Chơi phỏm</a></li>
</ul>
</li>
<li id="menu-item-3062" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3062"><a href="http://home.sandinh.com/ngan-hang/">Ngân hàng</a></li>
<li id="menu-item-3061" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3061"><a href="http://home.sandinh.com/bang-vang/">Bảng vàng</a></li>
<li id="menu-item-3065" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3065"><a href="http://home.sandinh.com/category/tong-hop/huong-dan/">Hướng Dẫn</a></li>
</ul></div>				</nav>
			</div>

			<div id="navigation" class="navbar">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<button class="menu-toggle">Trình đơn</button>
					<a class="screen-reader-text skip-link" href="#content" title="Chuyển đến phần nội dung">Chuyển đến phần nội dung</a>
					<div class="menu-main-menu-container"><ul id="primary-menu" class="nav-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2114 current_page_item menu-item-3057"><a href="http://home.sandinh.com/">Trang chủ</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3067"><a href="http://home.sandinh.com/category/tong-hop/tin-tuc/">Tin tức</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3129"><a href="http://dev.sandinh.com">Cộng đồng</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3058"><a href="http://dev.sandinh.com">Diễn đàn</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3130"><a href="https://www.facebook.com/sandinh.game/">Fanpage</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3060"><a href="#">Chơi game</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3073"><a href="http://home.sandinh.com/choi-chan/">Chơi chắn</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3076"><a href="http://home.sandinh.com/choi-phom/">Chơi phỏm</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3062"><a href="http://home.sandinh.com/ngan-hang/">Ngân hàng</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3061"><a href="http://home.sandinh.com/bang-vang/">Bảng vàng</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3065"><a href="http://home.sandinh.com/category/tong-hop/huong-dan/">Hướng Dẫn</a></li>
</ul></div>									</nav><!-- #site-navigation -->
			</div><!-- #navbar -->

			<script>
			jQuery.noConflict();
			jQuery(document).ready(function() {
			  jQuery('#simple-menu').sidr();
			});
			</script>
		</header><!-- #masthead -->

		<div id="main" class="site-main">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
													<div class="vc_row wpb_row vc_row-fluid slideshow vc_custom_1443062306184"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="wpb_wrapper"><div class="wpb_gallery wpb_content_element vc_clearfix vc_custom_1442999210985"><div class="wpb_wrapper"><div class="wpb_gallery_slides wpb_flexslider flexslider_slide flexslider" data-interval="3" data-flex_fx="slide"><ul class="slides"><li><a href="#" target="_self"><img class="" src="http://home.sandinh.com/wp-content/uploads/2015/09/banner-home-1-616x262.png" width="616" height="262" alt="banner-home -1" title="banner-home -1" /></a></li><li><a href="#" target="_self"><img class="" src="http://home.sandinh.com/wp-content/uploads/2015/09/banner-home1-616x262.png" width="616" height="262" alt="banner-home" title="banner-home" /></a></li><li><a href="#" target="_self"><img class="" src="http://home.sandinh.com/wp-content/uploads/2015/09/banner-home-2-616x262.png" width="616" height="262" alt="banner-home-2" title="banner-home-2" /></a></li></ul></div></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid games"><div class="game-chan wpb_column vc_column_container vc_col-sm-6"><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid game-chan-content"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_center">
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="130" height="131" src="http://home.sandinh.com/wp-content/uploads/2015/09/chan.png" class="vc_single_image-img attachment-full" alt="chan" /></div>
		</div>
	</div>
</div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  vc_custom_1443070452639">
		<div class="wpb_wrapper">
			<h6>Chắn Sân Đình</h6>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p><a href="http://home.sandinh.com/choi-chan/"><img class="alignnone size-full wp-image-2692" src="http://home.sandinh.com//wp-content/uploads/2015/09/choi-ngay-btn.png" alt="choi-ngay-btn" width="127" height="34" /></a><a href="http://home.sandinh.com//?page_id=2774"><img class="alignnone size-full wp-image-2694" src="http://home.sandinh.com//wp-content/uploads/2015/09/huong-dan-btn.png" alt="huong-dan-btn" width="127" height="34" /></a></p>

		</div>
	</div>
</div></div></div><div class="vc_row wpb_row vc_inner vc_row-fluid game-chan-buttons"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="wpb_wrapper"><div class="ubtn-img-container"><img src="http://home.sandinh.com/wp-content/uploads/2015/10/chan-android.png"/><div class="ubtn-ctn-center"><button type="button" class="ubtn ubtn-custom ubtn-no-hover-bg  ulta-grow  ubtn-center   tooltip-563983837af22"  data-hover="" data-border-color="" data-bg="" data-hover-bg="" data-border-hover="" data-shadow-hover="" data-shadow-click="none" data-shadow="" data-shd-shadow="" style="font-weight:normal;width:142px;min-height:48px;padding:px px;border:none;color: #000000;"><span class="ubtn-hover" style="background-color:"></span><span class="ubtn-data ubtn-text"></span></button></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="wpb_wrapper"><div class="ubtn-img-container"><img src="http://home.sandinh.com/wp-content/uploads/2015/10/chan-ios.png"/><div class="ubtn-ctn-center"><button type="button" class="ubtn ubtn-custom ubtn-no-hover-bg  none  ubtn-center   tooltip-563983837b219"  data-hover="" data-border-color="" data-bg="" data-hover-bg="" data-border-hover="" data-shadow-hover="" data-shadow-click="none" data-shadow="" data-shd-shadow="" style="font-weight:normal;width:142px;min-height:48px;padding:px px;border:none;color: #000000;"><span class="ubtn-hover" style="background-color:"></span><span class="ubtn-data ubtn-text"></span></button></div></div></div></div></div></div></div><div class="game-phom wpb_column vc_column_container vc_col-sm-6"><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid game-phom-content"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_center">
		<div class="wpb_wrapper">
			
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="130" height="131" src="http://home.sandinh.com/wp-content/uploads/2015/09/phom.png" class="vc_single_image-img attachment-full" alt="phom" /></div>
		</div>
	</div>
</div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  vc_custom_1443070465078">
		<div class="wpb_wrapper">
			<h6>Phỏm Sân Đình</h6>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p><a href="http://home.sandinh.com/choi-phom/"><img class="alignnone size-full wp-image-2692" src="http://home.sandinh.com//wp-content/uploads/2015/09/choi-ngay-btn.png" alt="choi-ngay-btn" width="127" height="34" /></a><a href="http://home.sandinh.com//?page_id=2790"><img class="alignnone size-full wp-image-2694" src="http://home.sandinh.com//wp-content/uploads/2015/09/huong-dan-btn.png" alt="huong-dan-btn" width="127" height="34" /></a></p>

		</div>
	</div>
</div></div></div><div class="vc_row wpb_row vc_inner vc_row-fluid game-phom-buttons"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="wpb_wrapper"><div class="ubtn-img-container"><img src="http://home.sandinh.com/wp-content/uploads/2015/10/phom-android.png"/><div class="ubtn-ctn-center"><button type="button" class="ubtn ubtn-custom ubtn-no-hover-bg  none  ubtn-center   tooltip-563983837cac3"  data-hover="" data-border-color="" data-bg="" data-hover-bg="" data-border-hover="" data-shadow-hover="" data-shadow-click="none" data-shadow="" data-shd-shadow="" style="font-weight:normal;width:142px;min-height:48px;padding:px px;border:none;color: #000000;"><span class="ubtn-hover" style="background-color:"></span><span class="ubtn-data ubtn-text"></span></button></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="wpb_wrapper"><div class="ubtn-img-container"><img src="http://home.sandinh.com/wp-content/uploads/2015/10/phom-ios.png"/><div class="ubtn-ctn-center"><button type="button" class="ubtn ubtn-custom ubtn-no-hover-bg  none  ubtn-center   tooltip-563983837cdc0"  data-hover="" data-border-color="" data-bg="" data-hover-bg="" data-border-hover="" data-shadow-hover="" data-shadow-click="none" data-shadow="" data-shd-shadow="" style="font-weight:normal;width:142px;min-height:48px;padding:px px;border:none;color: #000000;"><span class="ubtn-hover" style="background-color:"></span><span class="ubtn-data ubtn-text"></span></button></div></div></div></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid news-tab"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="wpb_wrapper"><div class="vc_wp_search wpb_content_element"><div class="widget widget_search"><form role="search" method="get" class="search-form" action="http://home.sandinh.com/">
				<label>
					<span class="screen-reader-text">Tìm kiếm cho:</span>
					<input type="search" class="search-field" placeholder="Nhập thông tin tìm kiếm &hellip;" value="" name="s" title="Tìm kiếm cho:" />
				</label>
				<input type="submit" class="search-submit" value="Tìm kiếm" />
			</form></div></div><style>
.rpwe-block ul{list-style:none!important;margin-left:0!important;padding-left:0!important;}.rpwe-block li{border-bottom:1px solid #eee;margin-bottom:10px;padding-bottom:10px;list-style-type: none;}.rpwe-block a{display:inline!important;text-decoration:none;}.rpwe-block h3{background:none!important;clear:none;margin-bottom:0!important;margin-top:0!important;font-weight:400;font-size:12px!important;line-height:1.5em;}.rpwe-thumb{border:1px solid #EEE!important;box-shadow:none!important;margin:2px 10px 2px 0;padding:3px!important;}.rpwe-summary{font-size:12px;}.rpwe-time{color:#bbb;font-size:11px;}.rpwe-comment{color:#bbb;font-size:11px;padding-left:5px;}.rpwe-alignleft{display:inline;float:left;}.rpwe-alignright{display:inline;float:right;}.rpwe-aligncenter{display:block;margin-left: auto;margin-right: auto;}.rpwe-clearfix:before,.rpwe-clearfix:after{content:"";display:table !important;}.rpwe-clearfix:after{clear:both;}.rpwe-clearfix{zoom:1;}
</style>
	<style>
.rpwe-block ul{list-style:none!important;margin-left:0!important;padding-left:0!important;}.rpwe-block li{border-bottom:1px solid #eee;margin-bottom:10px;padding-bottom:10px;list-style-type: none;}.rpwe-block a{display:inline!important;text-decoration:none;}.rpwe-block h3{background:none!important;clear:none;margin-bottom:0!important;margin-top:0!important;font-weight:400;font-size:12px!important;line-height:1.5em;}.rpwe-thumb{border:1px solid #EEE!important;box-shadow:none!important;margin:2px 10px 2px 0;padding:3px!important;}.rpwe-summary{font-size:12px;}.rpwe-time{color:#bbb;font-size:11px;}.rpwe-comment{color:#bbb;font-size:11px;padding-left:5px;}.rpwe-alignleft{display:inline;float:left;}.rpwe-alignright{display:inline;float:right;}.rpwe-aligncenter{display:block;margin-left: auto;margin-right: auto;}.rpwe-clearfix:before,.rpwe-clearfix:after{content:"";display:table !important;}.rpwe-clearfix:after{clear:both;}.rpwe-clearfix{zoom:1;}
</style>
	<style>
.rpwe-block ul{list-style:none!important;margin-left:0!important;padding-left:0!important;}.rpwe-block li{border-bottom:1px solid #eee;margin-bottom:10px;padding-bottom:10px;list-style-type: none;}.rpwe-block a{display:inline!important;text-decoration:none;}.rpwe-block h3{background:none!important;clear:none;margin-bottom:0!important;margin-top:0!important;font-weight:400;font-size:12px!important;line-height:1.5em;}.rpwe-thumb{border:1px solid #EEE!important;box-shadow:none!important;margin:2px 10px 2px 0;padding:3px!important;}.rpwe-summary{font-size:12px;}.rpwe-time{color:#bbb;font-size:11px;}.rpwe-comment{color:#bbb;font-size:11px;padding-left:5px;}.rpwe-alignleft{display:inline;float:left;}.rpwe-alignright{display:inline;float:right;}.rpwe-aligncenter{display:block;margin-left: auto;margin-right: auto;}.rpwe-clearfix:before,.rpwe-clearfix:after{content:"";display:table !important;}.rpwe-clearfix:after{clear:both;}.rpwe-clearfix{zoom:1;}
</style>
	<style>
.rpwe-block ul{list-style:none!important;margin-left:0!important;padding-left:0!important;}.rpwe-block li{border-bottom:1px solid #eee;margin-bottom:10px;padding-bottom:10px;list-style-type: none;}.rpwe-block a{display:inline!important;text-decoration:none;}.rpwe-block h3{background:none!important;clear:none;margin-bottom:0!important;margin-top:0!important;font-weight:400;font-size:12px!important;line-height:1.5em;}.rpwe-thumb{border:1px solid #EEE!important;box-shadow:none!important;margin:2px 10px 2px 0;padding:3px!important;}.rpwe-summary{font-size:12px;}.rpwe-time{color:#bbb;font-size:11px;}.rpwe-comment{color:#bbb;font-size:11px;padding-left:5px;}.rpwe-alignleft{display:inline;float:left;}.rpwe-alignright{display:inline;float:right;}.rpwe-aligncenter{display:block;margin-left: auto;margin-right: auto;}.rpwe-clearfix:before,.rpwe-clearfix:after{content:"";display:table !important;}.rpwe-clearfix:after{clear:both;}.rpwe-clearfix{zoom:1;}
</style>
	
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="responsive-tabs">
<h2 class="tabtitle">Tổng hợp</h2>
<div class="tabcontent responsive-tabs__panel--active">
<div  class="rpwe-block "><ul class="rpwe-ul"><li class="rpwe-li rpwe-clearfix category-tinh-nang-dac-sac"><h3 class="rpwe-title"><a href="http://home.sandinh.com/johann-wolfgang-von-goethe/" title="Permalink to Johann Wolfgang von Goethe" rel="bookmark">Johann Wolfgang von Goethe</a><time class="rpwe-time published" datetime="2015-10-05T11:58:24+00:00">05/10/2015</time></h3><a class="rpwe-img" href="http://home.sandinh.com/johann-wolfgang-von-goethe/"  rel="bookmark"><img class="rpwe-alignleft rpwe-thumb" src="http://home.sandinh.com/wp-content/uploads/2014/09/leigh_ann-123x70.jpg" alt="Johann Wolfgang von Goethe"></a><div class="rpwe-summary">Every day we should hear at least one little song, read one good poem, see one exquisite picture, and, if possible, speak a few sensible words.<br><a href="http://home.sandinh.com/johann-wolfgang-von-goethe/" class="more-link">chi tiết</a></div></li><li class="rpwe-li rpwe-clearfix category-huong-dan"><h3 class="rpwe-title"><a href="http://home.sandinh.com/buddha/" title="Permalink to Buddha" rel="bookmark">Buddha</a><time class="rpwe-time published" datetime="2015-10-05T11:57:57+00:00">05/10/2015</time></h3></li><li class="rpwe-li rpwe-clearfix category-tin-tuc"><h3 class="rpwe-title"><a href="http://home.sandinh.com/top-5-ways-to-unwind-2-14/" title="Permalink to Top 5 Ways To Unwind 2" rel="bookmark">Top 5 Ways To Unwind 2</a><time class="rpwe-time published" datetime="2015-10-05T11:22:22+00:00">05/10/2015</time></h3></li><li class="rpwe-li rpwe-clearfix category-tin-tuc"><h3 class="rpwe-title"><a href="http://home.sandinh.com/top-5-ways-to-unwind-2-15/" title="Permalink to Top 5 Ways To Unwind 2" rel="bookmark">Top 5 Ways To Unwind 2</a><time class="rpwe-time published" datetime="2015-10-05T11:22:22+00:00">05/10/2015</time></h3></li><li class="rpwe-li rpwe-clearfix category-tin-tuc"><h3 class="rpwe-title"><a href="http://home.sandinh.com/top-5-ways-to-unwind-2-13/" title="Permalink to Top 5 Ways To Unwind 2" rel="bookmark">Top 5 Ways To Unwind 2</a><time class="rpwe-time published" datetime="2015-10-05T11:22:20+00:00">05/10/2015</time></h3></li></ul></div><!-- Generated by http://wordpress.org/plugins/recent-posts-widget-extended/ --></p>
<div class="tab-xem-them"><a class="xem-them" href="http://home.sandinh.com//?cat=37">Xem Thêm</a></div>
<p>
</div><h2 class="tabtitle">Tin tức</h2>
<div class="tabcontent">
<div  class="rpwe-block "><ul class="rpwe-ul"><li class="rpwe-li rpwe-clearfix category-tin-tuc"><h3 class="rpwe-title"><a href="http://home.sandinh.com/top-5-ways-to-unwind-2-14/" title="Permalink to Top 5 Ways To Unwind 2" rel="bookmark">Top 5 Ways To Unwind 2</a><time class="rpwe-time published" datetime="2015-10-05T11:22:22+00:00">05/10/2015</time></h3><a class="rpwe-img" href="http://home.sandinh.com/top-5-ways-to-unwind-2-14/"  rel="bookmark"><img class="rpwe-alignleft rpwe-thumb" src="http://home.sandinh.com/wp-content/uploads/2014/09/photodune-8386880-business-conversation-m-123x70.jpg" alt="Top 5 Ways To Unwind 2"></a><div class="rpwe-summary">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.<br><a href="http://home.sandinh.com/top-5-ways-to-unwind-2-14/" class="more-link">chi tiết</a></div></li><li class="rpwe-li rpwe-clearfix category-tin-tuc"><h3 class="rpwe-title"><a href="http://home.sandinh.com/top-5-ways-to-unwind-2-15/" title="Permalink to Top 5 Ways To Unwind 2" rel="bookmark">Top 5 Ways To Unwind 2</a><time class="rpwe-time published" datetime="2015-10-05T11:22:22+00:00">05/10/2015</time></h3></li><li class="rpwe-li rpwe-clearfix category-tin-tuc"><h3 class="rpwe-title"><a href="http://home.sandinh.com/top-5-ways-to-unwind-2-13/" title="Permalink to Top 5 Ways To Unwind 2" rel="bookmark">Top 5 Ways To Unwind 2</a><time class="rpwe-time published" datetime="2015-10-05T11:22:20+00:00">05/10/2015</time></h3></li><li class="rpwe-li rpwe-clearfix category-tin-tuc"><h3 class="rpwe-title"><a href="http://home.sandinh.com/top-5-ways-to-unwind-2-10/" title="Permalink to Top 5 Ways To Unwind 2" rel="bookmark">Top 5 Ways To Unwind 2</a><time class="rpwe-time published" datetime="2015-10-05T11:22:17+00:00">05/10/2015</time></h3></li><li class="rpwe-li rpwe-clearfix category-tin-tuc"><h3 class="rpwe-title"><a href="http://home.sandinh.com/top-5-ways-to-unwind-2-11/" title="Permalink to Top 5 Ways To Unwind 2" rel="bookmark">Top 5 Ways To Unwind 2</a><time class="rpwe-time published" datetime="2015-10-05T11:22:17+00:00">05/10/2015</time></h3></li></ul></div><!-- Generated by http://wordpress.org/plugins/recent-posts-widget-extended/ --></p>
<div class="tab-xem-them"><a class="xem-them" href="http://home.sandinh.com//?cat=34">Xem Thêm</a></div>
<p>
</div><h2 class="tabtitle">hướng dẫn</h2>
<div class="tabcontent">
<div  class="rpwe-block "><ul class="rpwe-ul"><li class="rpwe-li rpwe-clearfix category-huong-dan"><h3 class="rpwe-title"><a href="http://home.sandinh.com/buddha/" title="Permalink to Buddha" rel="bookmark">Buddha</a><time class="rpwe-time published" datetime="2015-10-05T11:57:57+00:00">05/10/2015</time></h3><a class="rpwe-img" href="http://home.sandinh.com/buddha/"  rel="bookmark"><img class="rpwe-alignleft rpwe-thumb" src="http://home.sandinh.com/wp-content/uploads/2014/09/sparklers-123x70.jpg" alt="Buddha"></a><div class="rpwe-summary">Thousands of candles can be lighted from a single candle, and the life of the candle will not be shortened. Happiness never decreases by being shared.<br><a href="http://home.sandinh.com/buddha/" class="more-link">chi tiết</a></div></li><li class="rpwe-li rpwe-clearfix category-huong-dan"><h3 class="rpwe-title"><a href="http://home.sandinh.com/mark-twain/" title="Permalink to Buddha" rel="bookmark">Buddha</a><time class="rpwe-time published" datetime="2013-12-13T03:17:52+00:00">13/12/2013</time></h3></li><li class="rpwe-li rpwe-clearfix category-huong-dan"><h3 class="rpwe-title"><a href="http://home.sandinh.com/grapes-always-grow-better-in-the-sunlight/" title="Permalink to 6 Secrets To Succcess" rel="bookmark">6 Secrets To Succcess</a><time class="rpwe-time published" datetime="2013-09-15T02:45:30+00:00">15/09/2013</time></h3></li></ul></div><!-- Generated by http://wordpress.org/plugins/recent-posts-widget-extended/ --></p>
<div class="tab-xem-them"><a class="xem-them" href="http://home.sandinh.com//?cat=35">Xem Thêm</a></div>
<p>
</div><h2 class="tabtitle">Tính năng</h2>
<div class="tabcontent">
<div  class="rpwe-block "><ul class="rpwe-ul"><li class="rpwe-li rpwe-clearfix category-tinh-nang-dac-sac"><h3 class="rpwe-title"><a href="http://home.sandinh.com/khuyen-mai-tang-bao/" title="Permalink to Khuyến Mãi &#8211; Tặng Bảo" rel="bookmark">Khuyến Mãi &#8211; Tặng Bảo</a><time class="rpwe-time published" datetime="2015-11-03T10:56:32+00:00">03/11/2015</time></h3><a class="rpwe-img" href="http://home.sandinh.com/khuyen-mai-tang-bao/"  rel="bookmark"><img class="rpwe-alignleft rpwe-thumb" src="http://home.sandinh.com/wp-content/uploads/2015/11/khoa-cu-bang-vang-123x70.png" alt="Khuyến Mãi &#8211; Tặng Bảo"></a><div class="rpwe-summary">Mauris et lacinia diam. Donec interdum faucibus nisl, a viverra arcu feugiat id. Nulla felis nulla, suscipit vel sollicitudin vitae, auctor semper urna. Morbi nec erat id leo lacinia tincidunt nec quis felis. Mauris aliquam efficitur lectus, eget placerat ex pretium eu. Aliquam placerat neque &hellip;<br><a href="http://home.sandinh.com/khuyen-mai-tang-bao/" class="more-link">chi tiết</a></div></li><li class="rpwe-li rpwe-clearfix category-tinh-nang-dac-sac"><h3 class="rpwe-title"><a href="http://home.sandinh.com/su-kien-hang-tuan/" title="Permalink to Sự kiện hàng tuần" rel="bookmark">Sự kiện hàng tuần</a><time class="rpwe-time published" datetime="2015-11-03T10:55:20+00:00">03/11/2015</time></h3></li><li class="rpwe-li rpwe-clearfix category-tinh-nang-dac-sac"><h3 class="rpwe-title"><a href="http://home.sandinh.com/tinh-nang-ban-be/" title="Permalink to Tính năng bạn bè" rel="bookmark">Tính năng bạn bè</a><time class="rpwe-time published" datetime="2015-11-03T10:54:18+00:00">03/11/2015</time></h3></li><li class="rpwe-li rpwe-clearfix category-tinh-nang-dac-sac"><h3 class="rpwe-title"><a href="http://home.sandinh.com/cong-than-pham-ham/" title="Permalink to Công thần &#8211; Phẩm hàm" rel="bookmark">Công thần &#8211; Phẩm hàm</a><time class="rpwe-time published" datetime="2015-11-03T10:52:52+00:00">03/11/2015</time></h3></li><li class="rpwe-li rpwe-clearfix category-tinh-nang-dac-sac"><h3 class="rpwe-title"><a href="http://home.sandinh.com/xin-choi-tinh-diem/" title="Permalink to Xin chơi &#8211; tính điểm" rel="bookmark">Xin chơi &#8211; tính điểm</a><time class="rpwe-time published" datetime="2015-11-03T10:49:44+00:00">03/11/2015</time></h3></li></ul></div><!-- Generated by http://wordpress.org/plugins/recent-posts-widget-extended/ --></p>
<div class="tab-xem-them"><a class="xem-them" href="http://home.sandinh.com//?cat=36">Xem Thêm</a></div>
</div></div>

		</div>
	</div>
</div></div></div>
						
		</div><!-- #content -->
	</div><!-- #primary -->

	<div id="tertiary" class="sidebar-container" role="complementary">
		<div class="sidebar-inner">
			<div class="widget-area">
				<aside class="widget">
					<div class="vc_row wpb_row vc_row-fluid">
						<div class="wpb_column vc_column_container vc_col-sm-12">
							<div class="wpb_wrapper">							
															<div class="wpb_text_column wpb_content_element user-box">
									<div class="wpb_wrapper">
										<img alt='' src='http://1.gravatar.com/avatar/70d8e1a1c3cd561a8f0be227962263ff?s=56&#038;d=mm&#038;r=g' srcset='http://1.gravatar.com/avatar/70d8e1a1c3cd561a8f0be227962263ff?s=112&amp;d=mm&amp;r=g 2x' class='avatar avatar-56 photo' height='56' width='56' />										<h4 class="account-name"><a
												href="http://home.sandinh.com/wp-admin/profile.php">sdio
</a>
										</h4>

										<div class="account-detail-link"><a
												href="http://home.sandinh.com/ngan-hang">Xem thông tin cá nhân</a>
										</div>
										<div class="logout"><a
												href="http://home.sandinh.com/wp-login.php?action=logout&amp;redirect_to=http%3A%2F%2Fhome.sandinh.com&amp;_wpnonce=6c8037ef53">Đăng xuất</a>
										</div>
									</div>
								</div>
															</div>
						</div>
					</div>
					<div class="vc_row wpb_row vc_row-fluid box-title">
						<div class="cu-phu-box-title wpb_column vc_column_container vc_col-sm-12">
							<div class="wpb_wrapper">
								<div id="ultimate-heading5620aa131e172" class="uvc-heading ultimate-heading5620aa131e172 uvc-427 " data-hspacer="no_spacer" data-halign="center" style="text-align:center">
									<div class="uvc-heading-spacer no_spacer" style="top"></div>
									<div class="uvc-main-heading ult-responsive" data-ultimate-target=".uvc-heading.ultimate-heading5620aa131e172 h3" data-responsive-json-new="{&quot;font-size&quot;:&quot;&quot;,&quot;line-height&quot;:&quot;&quot;}">
										<h3 style="font-weight:normal;">Bảng xếp hạng Sân Đình</h3>
									</div>
								</div>
							</div>
						</div>
					</div>				
					<div class="vc_row wpb_row vc_row-fluid box-content">
						<div class="cu-phu-box-content wpb_column vc_column_container vc_col-sm-12">
							<div class="wpb_wrapper">
								<div class="wpb_text_column wpb_content_element  cu-phu-san-dinh-content">
									<div class="wpb_wrapper">
										<select id="bang-xep-hang-type">
											<option value="top-cu-phu">Top cự phú</option>
											<option value="top-kinh-nghiem">Top kinh nghiệm</option>
											<option value="top-cuoc-u">Top cước ù to</option>
										</select>
										<div id="bang-vang">
																						<div id="top-cu-phu">
												<ul>
											<li><span class="gamer">doku</span> <span class="amount">10156735438</span></li><li><span class="gamer">test1</span> <span class="amount">7944430000</span></li><li><span class="gamer">test4</span> <span class="amount">5161594457</span></li><li><span class="gamer">Củ Khoai Lang</span> <span class="amount">5061175033</span></li><li><span class="gamer">test10</span> <span class="amount">5016307300</span></li><li><span class="gamer">giang2</span> <span class="amount">5015000000</span></li><li><span class="gamer">test14</span> <span class="amount">5000349500</span></li><li><span class="gamer">u666139</span> <span class="amount">5000099999</span></li><li><span class="gamer">test13</span> <span class="amount">5000011450</span></li><li><span class="gamer">test12</span> <span class="amount">5000000000</span></li>												</ul>
											</div>

											<div id="top-kinh-nghiem" style="display: none;">
												<ul>
											<li><span class="gamer">test10</span> <span class="amount">1000112</span></li><li><span class="gamer">test15</span> <span class="amount">999992</span></li><li><span class="gamer">test3</span> <span class="amount">333330</span></li><li><span class="gamer">test1</span> <span class="amount">333330</span></li><li><span class="gamer">getTopTest</span> <span class="amount">200000</span></li>												</ul>
											</div>

											<div id="top-cuoc-u" style="display: none;">
												<ul>
											<li><span class="gamer">nguyentuanviet</span> <span class="amount">45</span></li><li><span class="gamer">31102014</span> <span class="amount">45</span></li><li><span class="gamer">SandyBridgeG860</span> <span class="amount">38</span></li><li><span class="gamer">C_Phong_77777</span> <span class="amount">38</span></li><li><span class="gamer">quanduido</span> <span class="amount">37</span></li><li><span class="gamer">quanduido</span> <span class="amount">37</span></li><li><span class="gamer">xuanngan08</span> <span class="amount">37</span></li><li><span class="gamer">QLKDDIEN</span> <span class="amount">36</span></li><li><span class="gamer">31102014</span> <span class="amount">36</span></li><li><span class="gamer">Thiên_Anh_Tinh</span> <span class="amount">36</span></li>												</ul>
											</div>
										</div>
										<script>
										jQuery('#bang-xep-hang-type').change(function() {
											if(jQuery('#bang-xep-hang-type').val()=='top-kinh-nghiem'){
												jQuery('#top-cu-phu').hide();
												jQuery('#top-kinh-nghiem').show();
												jQuery('#top-cuoc-u').hide();
											}else if(jQuery('#bang-xep-hang-type').val()=='top-cuoc-u'){
												jQuery('#top-cu-phu').hide();
												jQuery('#top-kinh-nghiem').hide();
												jQuery('#top-cuoc-u').show();
											}else{
												jQuery('#top-cu-phu').show();
												jQuery('#top-kinh-nghiem').hide();
												jQuery('#top-cuoc-u').hide();
											}
										});
										</script>

									</div>
								</div>
							</div>
						</div>
					</div>
				</aside>
				<aside id="epx_vcsb_widget_2618" class="widget ERROPiX\VC_Sidebar_Editor\Main"><div class="vc_row wpb_row vc_row-fluid box-title"><div class="ho-tro-box-title wpb_column vc_column_container vc_col-sm-12"><div class="wpb_wrapper"><div id="ultimate-heading56398383c89ec" class="uvc-heading ultimate-heading56398383c89ec uvc-378 " data-hspacer="no_spacer"  data-halign="center" style="text-align:center"><div class="uvc-heading-spacer no_spacer" style="top"></div><div class="uvc-main-heading ult-responsive"  data-ultimate-target='.uvc-heading.ultimate-heading56398383c89ec h3'  data-responsive-json-new='{"font-size":"","line-height":""}' ><h3 style="font-weight:normal;">Điện Thoại Hỗ Trợ</h3></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid box-content"><div class="ho-tro-box-content wpb_column vc_column_container vc_col-sm-12"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<ul>
<li>Mod 03: 0909.282.215 (<span class="status-online">Đang trực</span>)</li>
<li>Mod 01: 0938.283.265 (<span class="status-online">Đang trực</span>)</li>
<li>Mod 02: 0928.083.265 (<span class="status-offline">Đang nghỉ</span>)</li>
</ul>

		</div>
	</div>
</div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  facebook-like-box">
		<div class="wpb_wrapper">
				<div id="fb-root"></div>
	<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1257537750939997&version=v2.0";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<div class="fb-like-box" data-height="207" data-href="https://www.facebook.com/sandinh.game" data-show-border="true" data-show-faces="true" data-stream="true" data-width="254" data-force-wall="false"></div>
	<div style="display:none;">Facebook By Weblizar Powered By Weblizar</div>
	

		</div>
	</div>
</div></div></div>
</aside>			</div><!-- .widget-area -->
		</div><!-- .sidebar-inner -->
	</div><!-- #tertiary -->
﻿
				<div class="tinh-nang-dac-sac">
			<div class="tab-tinh-nang xem-lai-content">
				<div class="tinh-nang-detail">
								<h3>Xem lại ván chơi</h3>
				<div class="summary">
					Mauris et lacinia diam. Donec interdum faucibus nisl, a viverra arcu feugiat id. Nulla felis nulla, suscipit vel sollicitudin vitae, auctor semper urna. Morbi nec erat id leo lacinia tincidunt nec quis felis. Mauris aliquam efficitur lectus, eget placerat ex pretium eu. Aliquam placerat neque cursus diam rhoncus dapibus. Mauris imperdiet, enim vel varius rhoncus, neque augue consequat ipsum, id pulvinar massa nisl eu nisl. Vivamus fringilla, purus sit amet hendrerit elementum, odio leo rutrum quam, quis laoreet mauris ligula porta urna. Nam dignissim urna vitae purus aliquet, ut pharetra urna blandit. Quisque efficitur risus libero, in faucibus				</div>
				<a href="http://home.sandinh.com/xem-lai-van-choi/" class="xem-them">xem thêm</a>
				</div>
				<img width="373" height="330" src="http://home.sandinh.com/wp-content/uploads/2015/11/khoa-cu-bang-vang.png" class="attachment-full wp-post-image" alt="khoa-cu-bang-vang" />			</div>
			<div class="tab-tinh-nang cong-than-content" style="display: none;">
				<div class="tinh-nang-detail">
								<h3>Công thần - Phẩm hàm</h3>
				<div class="summary">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sed ultricies dui, eget iaculis leo. Ut auctor enim in ante aliquam, quis sollicitudin ex fermentum. Sed fringilla lectus non enim aliquet hendrerit. Donec iaculis vel neque convallis tempor. Nunc id purus quis mauris dapibus dictum. Vestibulum in nibh eget ligula fermentum malesuada. Cras ornare mattis magna, sed congue quam. Sed nisi quam, fermentum ac tempor ac, bibendum quis dui. Curabitur facilisis suscipit orci, vel ornare libero efficitur sit amet. Proin non lacus sed velit eleifend vulputate nec quis sem. Nam at finibus nisi. Nunc gravida, mauris vitae maximus mattis,				</div>
				<a href="http://home.sandinh.com/cong-than-pham-ham/" class="xem-them">xem thêm</a>
				</div>
				<img width="373" height="330" src="http://home.sandinh.com/wp-content/uploads/2015/11/khoa-cu-bang-vang.png" class="attachment-full wp-post-image" alt="khoa-cu-bang-vang" />			</div>
			<div class="tab-tinh-nang khoa-cu-content" style="display: none;">
				<div class="tinh-nang-detail">
								<h3>Khoa cử - Bảng vàng</h3>
				<div class="summary">
					Là người dân Việt Nam, chắc hẳn không ai không biết truyền thống hiếu học ngàn đời của dân tộc ta. Tuy nhiên không phải ai cũng có thể tiếp cận được với lịch sử Khoa Bảng, những cuộc thi để chọn ra những nhân tài ưu tú nhất. Chính vì thế Chắn Pro đã ra mắt tính năng Khoa Cử áp dụng giống hệt với các cuộc thi của cha ông ta ngày trước. Những người chiến thắng đều được vinh danh lưu lại tên tuổi của mình. Các bạn có thể làm theo hướng dẫn sau để xem được những người đã xuất sắc vượt qua hàng trăm người chơi khác Vinh Danh Bảng Vàng.				</div>
				<a href="http://home.sandinh.com/khoa-cu-bang-vang/" class="xem-them">xem thêm</a>
				</div>
				<img width="373" height="330" src="http://home.sandinh.com/wp-content/uploads/2015/11/khoa-cu-bang-vang.png" class="attachment-full wp-post-image" alt="khoa-cu-bang-vang" />			</div>
			<div class="tab-tinh-nang xin-choi-content" style="display: none;">
				<div class="tinh-nang-detail">
								<h3>Xin chơi - tính điểm</h3>
				<div class="summary">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sed ultricies dui, eget iaculis leo. Ut auctor enim in ante aliquam, quis sollicitudin ex fermentum. Sed fringilla lectus non enim aliquet hendrerit. Donec iaculis vel neque convallis tempor. Nunc id purus quis mauris dapibus dictum. Vestibulum in nibh eget ligula fermentum malesuada. Cras ornare mattis magna, sed congue quam. Sed nisi quam, fermentum ac tempor ac, bibendum quis dui. Curabitur facilisis suscipit orci, vel ornare libero efficitur sit amet. Proin non lacus sed velit eleifend vulputate nec quis sem. Nam at finibus nisi. Nunc gravida, mauris vitae maximus mattis,				</div>
				<a href="http://home.sandinh.com/xin-choi-tinh-diem/" class="xem-them">xem thêm</a>
				</div>
				<img width="373" height="330" src="http://home.sandinh.com/wp-content/uploads/2015/11/khoa-cu-bang-vang.png" class="attachment-full wp-post-image" alt="khoa-cu-bang-vang" />			</div>
			<div class="tab-tinh-nang tinh-nang-content" style="display: none;">
				<div class="tinh-nang-detail">
								<h3>Tính năng bạn bè</h3>
				<div class="summary">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sed ultricies dui, eget iaculis leo. Ut auctor enim in ante aliquam, quis sollicitudin ex fermentum. Sed fringilla lectus non enim aliquet hendrerit. Donec iaculis vel neque convallis tempor. Nunc id purus quis mauris dapibus dictum. Vestibulum in nibh eget ligula fermentum malesuada. Cras ornare mattis magna, sed congue quam. Sed nisi quam, fermentum ac tempor ac, bibendum quis dui. Curabitur facilisis suscipit orci, vel ornare libero efficitur sit amet. Proin non lacus sed velit eleifend vulputate nec quis sem. Nam at finibus nisi. Nunc gravida, mauris vitae maximus mattis,				</div>
				<a href="http://home.sandinh.com/tinh-nang-ban-be/" class="xem-them">xem thêm</a>
				</div>
				<img width="373" height="330" src="http://home.sandinh.com/wp-content/uploads/2015/11/khoa-cu-bang-vang.png" class="attachment-full wp-post-image" alt="khoa-cu-bang-vang" />			</div>
			<div class="tab-tinh-nang su-kien-content" style="display: none;">
				<div class="tinh-nang-detail">
										<h3>Sự kiện hàng tuần</h3>
					<div class="summary">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sed ultricies dui, eget iaculis leo. Ut auctor enim in ante aliquam, quis sollicitudin ex fermentum. Sed fringilla lectus non enim aliquet hendrerit. Donec iaculis vel neque convallis tempor. Nunc id purus quis mauris dapibus dictum. Vestibulum in nibh eget ligula fermentum malesuada. Cras ornare mattis magna, sed congue quam. Sed nisi quam, fermentum ac tempor ac, bibendum quis dui. Curabitur facilisis suscipit orci, vel ornare libero efficitur sit amet. Proin non lacus sed velit eleifend vulputate nec quis sem. Nam at finibus nisi. Nunc gravida, mauris vitae maximus mattis,					</div>
					<a href="http://home.sandinh.com/su-kien-hang-tuan/" class="xem-them">xem thêm</a>
				</div>
				<img width="373" height="330" src="http://home.sandinh.com/wp-content/uploads/2015/11/khoa-cu-bang-vang.png" class="attachment-full wp-post-image" alt="khoa-cu-bang-vang" />			</div>
			<div class="tab-tinh-nang khuyen-mai-content" style="display: none;">
				<div class="tinh-nang-detail">
										<h3>Khuyến Mãi - Tặng Bảo</h3>
					<div class="summary">
						Mauris et lacinia diam. Donec interdum faucibus nisl, a viverra arcu feugiat id. Nulla felis nulla, suscipit vel sollicitudin vitae, auctor semper urna. Morbi nec erat id leo lacinia tincidunt nec quis felis. Mauris aliquam efficitur lectus, eget placerat ex pretium eu. Aliquam placerat neque cursus diam rhoncus dapibus. Mauris imperdiet, enim vel varius rhoncus, neque augue consequat ipsum, id pulvinar massa nisl eu nisl. Vivamus fringilla, purus sit amet hendrerit elementum, odio leo rutrum quam, quis laoreet mauris ligula porta urna. Nam dignissim urna vitae purus aliquet, ut pharetra urna blandit. Quisque efficitur risus libero, in faucibus					</div>
					<a href="http://home.sandinh.com/khuyen-mai-tang-bao/" class="xem-them">xem thêm</a>
				</div>
				<img width="373" height="330" src="http://home.sandinh.com/wp-content/uploads/2015/11/khoa-cu-bang-vang.png" class="attachment-full wp-post-image" alt="khoa-cu-bang-vang" />			</div>
			<ul class="images-tabs">
				<li class="btn-tinh-nang xem-lai-btn"><a data-link='xem-lai-content' class="hover" href="javascript: void();">Xem lại</a></li>
				<li class="btn-tinh-nang cong-than-btn"><a data-link='cong-than-content' href="javascript: void();">Công thần</a></li>
				<li class="btn-tinh-nang khoa-cu-btn"><a data-link='khoa-cu-content' href="javascript: void();">Khoa cử</a></li>
				<li class="btn-tinh-nang xin-choi-btn"><a data-link='xin-choi-content' href="javascript: void();">Xin chơi</a></li>
				<li class="btn-tinh-nang tinh-nang-btn"><a data-link='tinh-nang-content' href="javascript: void();">Tính năng</a></li>
				<li class="btn-tinh-nang su-kien-btn"><a data-link='su-kien-content' href="javascript: void();">Sự kiện</a></li>
				<li class="btn-tinh-nang khuyen-mai-btn"><a data-link='khuyen-mai-content' href="javascript: void();">Khuyến mại</a></li>
			</ul>
		</div>
			<script>
				//Change content of Tabs
				jQuery('.btn-tinh-nang a').hover(function(){
					var link = jQuery(this).data('link');
					jQuery('.tab-tinh-nang').hide();
					jQuery("."+link).show();
					jQuery('.btn-tinh-nang a').removeClass('hover');
					jQuery(this).addClass('hover');
				});
				// Change image of tabs thumbnail when hover
				/*jQuery('.btn-tinh-nang img').each(function(){
					var t=jQuery(this);
					var src1= t.attr('src'); // initial src
					var newSrc = src1.substring(0, src1.lastIndexOf('.')); // let's get file name without extension
					t.hover(function(){
						jQuery(this).attr('src', newSrc+ '-hover.' + /[^.]+$/.exec(src1)); //last part is for extension
					}, function(){
						jQuery(this).attr('src', newSrc + '.' + /[^.]+$/.exec(src1)); //removing '-over' from the name
					});
				});*/
			</script>
			<div id="secondary" class="sidebar-container" role="complementary">
		<div class="widget-area">
			<aside id="epx_vcsb_widget_2625" class="widget ERROPiX\VC_Sidebar_Editor\Main"><h3 class="widget-title">Sân Đình &#8211; Game Việt cho người Việt</h3><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p>Sân Đình ra đời với mong muốn đem văn hóa trò chơi dân gian lên mobile, web, kết nối cộng đồng Game Thủ khắp mọi miền đất nước. Game Sân Đình có đồ họa tuyệt vời mang đậm dấu ấn của làng quê Việt xưa. Hệ thống chăm sóc khách hàng, tính năng đặc sắc giúp bạn thỏa sức tranh tài và giao lưu với hàng ngàn chắn thủ khác trên cả nước.</p>

		</div>
	</div>
</div></div></div>
</aside>		</div><!-- .widget-area -->
	</div><!-- #secondary -->
		</div><!-- #main -->

		<footer id="colophon" class="site-footer" role="contentinfo">	
			<aside id="epx_vcsb_widget_2639" class="widget ERROPiX\VC_Sidebar_Editor\Main"><div class="vc_row wpb_row vc_row-fluid seo-text"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  seo-text">
		<div class="wpb_wrapper">
			<p>Game danh chan online hay nhat game poker online mien phi choi tien len mien nam online mien phi game bai ta la, danh phom truc tuyen choi game danh co tuong truc tuyen mien phi Choi game xi to online mien phi game bai sam loc online hay nhat mau binh, binh xap xam online hay nhat danh co tuong up online mien phi.</p>

		</div>
	</div>
</div></div></div><div class="vc_row wpb_row vc_row-fluid footer-text"><div class="wpb_column vc_column_container vc_col-sm-3"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  copyright">
		<div class="wpb_wrapper">
			<p>Phát triển bởi sandinh.com @ 2012-2015</p>

		</div>
	</div>
</div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="wpb_wrapper"><div class="vc_wp_custommenu wpb_content_element footer-menu"><div class="widget widget_nav_menu"><div class="menu-footer-menu-container"><ul id="menu-footer-menu" class="menu"><li id="menu-item-2977" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2114 current_page_item menu-item-2977"><a href="http://home.sandinh.com/">Trang chủ</a></li>
<li id="menu-item-2632" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2632"><a href="#">Tin tức</a></li>
<li id="menu-item-2633" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2633"><a>Chơi game</a></li>
<li id="menu-item-2634" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2634"><a href="#">Ngân hàng</a></li>
<li id="menu-item-2635" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2635"><a href="#">Bản Mobile</a></li>
<li id="menu-item-2636" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2636"><a href="#">Đấu trường</a></li>
<li id="menu-item-2637" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2637"><a href="#">Quy định</a></li>
</ul></div></div></div></div></div><div class="footer-logo-wrap wpb_column vc_column_container vc_col-sm-3"><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_center   footer-logo">
		<div class="wpb_wrapper">
			
			<a href="index.php" target="_self"><div class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img " src="http://home.sandinh.com/wp-content/uploads/2015/09/footer-logo.png" width="147" height="30" alt="footer-logo" title="footer-logo" /></div></a>
		</div>
	</div>
</div></div></div>
</aside>		</footer><!-- #colophon -->
	</div><!-- #page -->

	<div id="simplemodal-login-form" style="display:none">
<form name="registerform" id="registerform" action="http://home.sandinh.com/wp-login.php?action=register" id="loginform" method="post">
	<div class="modal-header">
	<div class="title">Đăng ký</div>
	<input type="button" class="simplemodal-close" value="x" tabindex="101">
	</div>
	<div class="simplemodal-login-fields">
	<p>
		<label>Tên đăng nhập<br />
		<input type="text" name="user_login" class="user_login input" value="" size="20" tabindex="10" /></label>
	</p>
	<p>
		<label>E-mail<br />
		<input type="text" name="user_email" class="user_email input" value="" size="25" tabindex="20" /></label>
	</p><!--	Start code from Cimy User Extra Fields 2.6.5	Copyright (c) 2006-2015 Marco Cimmino	http://www.marcocimmino.net/cimy-wordpress-plugins/cimy-user-extra-fields/	-->
	<input type="hidden" name="cimy_post" value="1" />
	<p id="cimy_uef_wp_p_field_1">
		<label for="cimy_uef_wp_1">Mật khẩu</label><input type="password" name="cimy_uef_wp_PASSWORD" id="cimy_uef_wp_1" class="cimy_uef_input_27" value="" maxlength="100" />
	</p>
	<p id="cimy_uef_wp_p_field_2">
		<label for="cimy_uef_wp_2">Xác nhận mật khẩu</label><input type="password" name="cimy_uef_wp_PASSWORD2" id="cimy_uef_wp_2" class="cimy_uef_input_27" value="" maxlength="100" />
	</p>
	<p id="cimy_uef_p_field_1">
		<label for="cimy_uef_1">Số điện thoại</label><input type="text" name="cimy_uef_PHONE" id="cimy_uef_1" class="cimy_uef_input_27" value="" maxlength="20" />
	</p>
	<p id="cimy_uef_p_field_4">
		<label for="cimy_uef_4">Giới tính</label><select name="cimy_uef_GENDER" id="cimy_uef_4" class="cimy_uef_input_27">
			<option value="nam">nam</option>
			<option value="nữ">nữ</option>
			<option value="không xác định">không xác định</option></select>
	</p>
	<br />
<!--	End of code from Cimy User Extra Fields	-->
<div class='g-recaptcha' data-sitekey='6LcqYg4TAAAAAKj8kFm76J4QsTLX0MaALdNUcp-d' data-theme='light' data-size='normal'></div>
	<p class="reg_passmail"></p>
	<p class="submit">
		<input type="submit" name="wp-submit" value="Đăng ký" tabindex="100" />
		<input type="button" class="simplemodal-close" value="Hủy" tabindex="101" />
	</p>
	<p class="nav">
		<a class="simplemodal-login" href=""></a>
	</p>
	</div>
	<div class="simplemodal-login-activity" style="display:none;"></div>
</form></div><link rel='stylesheet' id='flexslider-css'  href='http://home.sandinh.com/wp-content/plugins/js_composer/assets/lib/bower/flexslider/flexslider.css?ver=4.7.4' type='text/css' media='screen' />
<link rel='stylesheet' id='tabby-css'  href='http://home.sandinh.com/wp-content/plugins/tabby-responsive-tabs/css/tabby.css?ver=1.2.2' type='text/css' media='all' />
<link rel='stylesheet' id='tabby-print-css'  href='http://home.sandinh.com/wp-content/plugins/tabby-responsive-tabs/css/tabby-print.css?ver=1.2.2' type='text/css' media='all' />
<script type='text/javascript' src='http://home.sandinh.com/wp-includes/js/admin-bar.min.js?ver=4.3.1'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-includes/js/masonry.min.js?ver=3.1.2'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-includes/js/jquery/jquery.masonry.min.js?ver=3.1.2'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-content/themes/twentythirteen/js/functions.js?ver=20150330'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-content/plugins/simplemodal-login/js/jquery.simplemodal.js?ver=1.4.3'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var SimpleModalLoginL10n = {"shortcut":"false","logged_in":"true","admin_url":"http:\/\/home.sandinh.com\/wp-admin\/","empty_username":"<strong>ERROR<\/strong>: The username field is empty.","empty_password":"<strong>ERROR<\/strong>: The password field is empty.","empty_email":"<strong>ERROR<\/strong>: The email field is empty.","empty_all":"<strong>ERROR<\/strong>: All fields are required."};
/* ]]> */
</script>
<script type='text/javascript' src='http://home.sandinh.com/wp-content/plugins/simplemodal-login/js/default.js?ver=1.1'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-content/plugins/js_composer/assets/js/js_composer_front.js?ver=4.7.4'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-content/plugins/js_composer/assets/lib/bower/flexslider/jquery.flexslider-min.js?ver=4.7.4'></script>
<script type='text/javascript' src='http://home.sandinh.com/wp-content/plugins/tabby-responsive-tabs/js/tabby.js?ver=1.2.2'></script>
<script type='text/javascript' src='https://www.google.com/recaptcha/api.js?ver=2.0'></script>

<script>jQuery(document).ready(function($) { RESPONSIVEUI.responsiveTabs(); })</script>
	<script type="text/javascript">
		(function() {
			var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

			request = true;

			b[c] = b[c].replace( rcs, ' ' );
			b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
		}());
	</script>
			<div id="wpadminbar" class="nojq nojs">
							<a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">Chuyển đến thanh công cụ</a>
						<div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Thanh công cụ" tabindex="0">
				<ul id="wp-admin-bar-root-default" class="ab-top-menu">
		<li id="wp-admin-bar-wp-logo" class="menupop"><a class="ab-item"  aria-haspopup="true" href="http://home.sandinh.com/wp-admin/about.php"><span class="ab-icon"></span><span class="screen-reader-text">Giới thiệu về WordPress</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wp-logo-default" class="ab-submenu">
		<li id="wp-admin-bar-about"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/about.php">Giới thiệu về WordPress</a>		</li></ul><ul id="wp-admin-bar-wp-logo-external" class="ab-sub-secondary ab-submenu">
		<li id="wp-admin-bar-wporg"><a class="ab-item"  href="https://wordpress.org/">WordPress.org</a>		</li>
		<li id="wp-admin-bar-documentation"><a class="ab-item"  href="https://codex.wordpress.org/">Tài liệu</a>		</li>
		<li id="wp-admin-bar-support-forums"><a class="ab-item"  href="https://wordpress.org/support/">Diễn đàn hỗ trợ</a>		</li>
		<li id="wp-admin-bar-feedback"><a class="ab-item"  href="https://wordpress.org/support/forum/requests-and-feedback">Thông tin phản hồi</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-site-name" class="menupop"><a class="ab-item"  aria-haspopup="true" href="http://home.sandinh.com/wp-admin/">Sân Đình Game Studio</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-site-name-default" class="ab-submenu">
		<li id="wp-admin-bar-dashboard"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/">Bảng tin</a>		</li></ul><ul id="wp-admin-bar-appearance" class="ab-submenu">
		<li id="wp-admin-bar-themes"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/themes.php">Giao diện</a>		</li>
		<li id="wp-admin-bar-widgets"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/widgets.php">Widget</a>		</li>
		<li id="wp-admin-bar-menus"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/nav-menus.php">Menu</a>		</li>
		<li id="wp-admin-bar-header" class="hide-if-customize"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/themes.php?page=custom-header">Header</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-customize" class="hide-if-no-customize"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/customize.php?url=http%3A%2F%2Fhome.sandinh.com%2F">Tùy chỉnh</a>		</li>
		<li id="wp-admin-bar-updates"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/update-core.php" title="1 cập nhật cho plugin"><span class="ab-icon"></span><span class="ab-label">1</span><span class="screen-reader-text">1 cập nhật cho plugin</span></a>		</li>
		<li id="wp-admin-bar-comments"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/edit-comments.php" title="0 bình luận đang đợi xét duyệt"><span class="ab-icon"></span><span id="ab-awaiting-mod" class="ab-label awaiting-mod pending-count count-0">0</span></a>		</li>
		<li id="wp-admin-bar-new-content" class="menupop"><a class="ab-item"  aria-haspopup="true" href="http://home.sandinh.com/wp-admin/post-new.php"><span class="ab-icon"></span><span class="ab-label">Mới</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-new-content-default" class="ab-submenu">
		<li id="wp-admin-bar-new-post"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/post-new.php">Bài viết</a>		</li>
		<li id="wp-admin-bar-new-media"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/media-new.php">Tập tin</a>		</li>
		<li id="wp-admin-bar-new-page"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/post-new.php?post_type=page">Trang</a>		</li>
		<li id="wp-admin-bar-new-user"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/user-new.php">Thành viên</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-edit" class="menupop"><a class="ab-item"  aria-haspopup="true" href="http://home.sandinh.com/wp-admin/post.php?post=2114&#038;action=edit">Sửa trang</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-edit-default" class="ab-submenu">
		<li id="wp-admin-bar-new_draft"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/admin.php?action=duplicate_post_save_as_new_post_draft&#038;post=2114">Copy to a new draft</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-TS_VCSC_Notification"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/index.php?page=TS_VCSC_Notification"><span>Composium - Visual Composer Extensions <span id="ab-updates" style="color: #ffffff; background-color: #d54e21;">New Version!</span></span></a>		</li>
		<li id="wp-admin-bar-vc_inline-admin-bar-link" class="vc_inline-link"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/post.php?vc_action=vc_inline&#038;post_id=2114&#038;post_type=page">Edit with Visual Composer</a>		</li></ul><ul id="wp-admin-bar-top-secondary" class="ab-top-secondary ab-top-menu">
		<li id="wp-admin-bar-search" class="admin-bar-search"><div class="ab-item ab-empty-item" tabindex="-1"><form action="http://home.sandinh.com/" method="get" id="adminbarsearch"><input class="adminbar-input" name="s" id="adminbar-search" type="text" value="" maxlength="150" /><label for="adminbar-search" class="screen-reader-text">Tìm kiếm</label><input type="submit" class="adminbar-button" value="Tìm kiếm"/></form></div>		</li>
		<li id="wp-admin-bar-my-account" class="menupop with-avatar"><a class="ab-item"  aria-haspopup="true" href="http://home.sandinh.com/wp-admin/profile.php">Chào bạn, sdio<img alt='' src='http://1.gravatar.com/avatar/70d8e1a1c3cd561a8f0be227962263ff?s=26&#038;d=mm&#038;r=g' srcset='http://1.gravatar.com/avatar/70d8e1a1c3cd561a8f0be227962263ff?s=52&amp;d=mm&amp;r=g 2x' class='avatar avatar-26 photo' height='26' width='26' /></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-user-actions" class="ab-submenu">
		<li id="wp-admin-bar-user-info"><a class="ab-item" tabindex="-1" href="http://home.sandinh.com/wp-admin/profile.php"><img alt='' src='http://1.gravatar.com/avatar/70d8e1a1c3cd561a8f0be227962263ff?s=64&#038;d=mm&#038;r=g' srcset='http://1.gravatar.com/avatar/70d8e1a1c3cd561a8f0be227962263ff?s=128&amp;d=mm&amp;r=g 2x' class='avatar avatar-64 photo' height='64' width='64' /><span class='display-name'>sdio</span><span class='username'>cpradmin</span></a>		</li>
		<li id="wp-admin-bar-edit-profile"><a class="ab-item"  href="http://home.sandinh.com/wp-admin/profile.php">Chỉnh sửa Hồ sơ của tôi</a>		</li>
		<li id="wp-admin-bar-logout"><a class="ab-item"  href="http://home.sandinh.com/wp-login.php?action=logout&#038;_wpnonce=6c8037ef53">Đăng xuất</a>		</li></ul></div>		</li></ul>			</div>
						<a class="screen-reader-shortcut" href="http://home.sandinh.com/wp-login.php?action=logout&#038;_wpnonce=6c8037ef53">Đăng xuất</a>
					</div>

		</body>
</html>
<!-- Dynamic page generated in 2.804 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2015-11-04 11:03:17 -->
