<?php
/*
Plugin Name: WordPress Admin Bar Modifications
Plugin URI: http://www.layneheiny.com
Description: Remove WP Logo from the admin bar, remove my sites, add new links for tuxreports network.
Author: Layne P. Heiny
Author URI: http://www.layneheiny.com/

Version 0.4
Removed the "top" css for themes.
Changed the min-height to 28px from 50px

Version 0.3
Added admin bar height function

Version 0.2
Replaced ending , with ;
removed extra commas

Version 0.1
Initial release
*/

/*  Copyright Layne Heiny (http://www.layneheiny.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* Always show admin bar */

add_filter( 'show_admin_bar', '__return_true' , 1000 );

/* Removes parts of the admin bar */

function trn_admin_bar_remove() {
	global $wp_admin_bar;

	/* Remove their stuff */
	$wp_admin_bar->remove_menu('wp-logo');
	$wp_admin_bar->remove_menu('my-sites');
	$wp_admin_bar->remove_node('search');
	$wp_admin_bar->remove_node('my-account');
}

add_action('wp_before_admin_bar_render', 'trn_admin_bar_remove', 0);


/* Fix the height of the admin bar */

function add_wpadmin_styles() {
	echo '<style>#wpadminbar {
		font-size: 14px;
		font-family: Arial, sans-serif;
		color: #ffffff;
		background: rgba(0, 0, 0, 0.8);
		min-height: 28px;
		line-height: 28px;
		left: 0;
		min-width: 600px;
		position: fixed;
		top: 0;
		width: 100%;
		z-index: 99999999;
		padding-left: 0px;
		/* padding-top: 15px; */
		!important;
	}</style>';

/* Shift labels to the right */

echo '<style>
#wpadminbar .quicklinks {
    border-left: 1px solid transparent;
    margin: 0 auto;
    width: 960px !important;
}
</style>';

echo '<style>
#wpadminbar .quicklinks ul {
    margin: 0 auto;
    text-align: left;
}
</style>';

echo '<style>
#wp-admin-bar ul {
	margin:0; padding:0
}
</style>';

echo '<style>
#wp-admin-bar ul li {
	list-style-type:none;
	display:inline;
	margin:0 10px;
	padding:0
}
</style>';

}

add_action('wp_before_admin_bar_render', 'add_wpadmin_styles');


/* Builds the menu */

function add_trn_admin_bar_link() {
	global $wp_admin_bar;

if (function_exists('xenforo_thread_url')) {

	if(is_user_logged_in())
	{

		$wp_admin_bar->add_menu( array(
		'id' => 'trn_logout',
		'title' => __('Logout'),
		'href' => __('/community/logout')
		));

	} else {

		$wp_admin_bar->add_menu( array(
		'id' => 'trn_login',
		'title' => __('Log in or Sign up'),
		'href' => __('/community/login')
		));

	}
} else {

	if(is_user_logged_in())
	{

		$wp_admin_bar->add_menu( array(
		'id' => 'trn_logout',
		'title' => __('Logout'),
		'href' => __( wp_logout_url( home_url() ) )
		));

		$wp_admin_bar->add_menu( array(
		'id' => 'trn_forums',
		'title' => __('Community'),
		'href' => __( '/community' )
		));


	} else {

		$wp_admin_bar->add_menu( array(
		'id' => 'trn_login',
		'title' => __('Log in or Sign up'),
		'href' => __( wp_login_url( get_permalink() ) )
		));

	}

}


	$wp_admin_bar->add_menu( array(
	'id' => 'trn_link',
	'title' => __('Tux Reports Network'),
	'href' => __('http://www.tuxreportsnetwork.com')
	));

	// Add sub menu link "Community"
	$wp_admin_bar->add_menu( array(
		'parent' => 'trn_link',
		'id'     => 'trn_community',
		'title' => __('Community'),
		'href' => __('http://www.tuxreportsdebates.com/community')
	));



	// Add sub menu link "Education"
	$wp_admin_bar->add_menu( array(
		'parent' => 'trn_link',
		'id'     => 'trn_education',
		'title' => __('Education'),
		'href' => __('http://www.tuxreportsdebates.com/community'),
		'meta'   => array(
			'class' => 'st_menu_download')
	));
		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_education',
			'id'     => 'trn_apenviro',
			'title' => __('AP Environmental Science'),
			'href' => __('http://www.apenvironmentalscience.com'),
			'meta' => false
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_education',
			'id'     => 'trn_bio',
			'title' => __('The Biology Book'),
			'href' => __('http://www.thebiologybook.com'),
			'meta' => false
		));


		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_education',
			'id'     => 'trn_chem',
			'title' => __('The Chem Book'),
			'href' => __('http://www.thechembook.com'),
			'meta' => false
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_education',
			'id'     => 'trn_classic',
			'title' => __('Classic Education'),
			'href' => __('http://www.Educlassics.com'),
			'meta' => false
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_education',
			'id'     => 'trn_green',
			'title' => __('Green Enterprise'),
			'href' => __('http://www.avgreen.org')
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_education',
			'id'     => 'trn_retaining',
			'title' => __('Retaining Teachers'),
			'href' => __('http://www.retainingteachers.com')
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_education',
			'id'     => 'trn_secondary',
			'title' => __('Secondary Classroom'),
			'href' => __('http://www.secondaryclassroom.com')
		));

	// Add sub menu link "Entertainment"

	$wp_admin_bar->add_menu( array(
		'parent' => 'trn_link',
		'id'     => 'trn_entertainment',
		'title' => __('Entertainment'),
		'href' => __('http://www.dogstimes.com'),
		'meta'   => array(
			'class' => 'st_menu_download')
	));


	$wp_admin_bar->add_menu( array(
		'parent' => 'trn_entertainment',
		'id'     => 'trn_dogtimes',
		'title' => __('Dogs Times'),
		'href' => __('http://www.dogstimes.com')
	));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_entertainment',
			'id'     => 'trn_thebeatles',
			'title' => __('The Beatles Chat'),
			'href' => __('http://www.thebeatleschat.com')
		));

	// Add sub menu link "News"
	$wp_admin_bar->add_menu( array(
		'parent' => 'trn_link',
		'id'     => 'trn_news',
		'title' => __('News'),
		'href' => __('http://www.tuxreportsnews.com')
	));


	// Add sub menu link "Politics"
	$wp_admin_bar->add_menu( array(
		'parent' => 'trn_link',
		'id'     => 'trn_politics',
		'title' => __('Politics'),
		'href' => __('http://www.insanepolitics.com')
	));

	// Add sub menu link "Technology"
	$wp_admin_bar->add_menu( array(
		'parent' => 'trn_link',
		'id'     => 'trn_technology',
		'title' => __('Technology'),
		'href' => __('http://www.tuxreportstech.com'),
		'meta'   => array(
			'class' => 'st_menu_download')
	));
		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_technology',
			'id'     => 'trn_amd',
			'title' => __('AMD Views'),
			'href' => __('http://www.amdviews.com')
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_technology',
			'id'     => 'trn_android',
			'title' => __('The Android Buzz'),
			'href' => __('http://www.theandroidbuzz.com')
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_technology',
			'id'     => 'trn_apple',
			'title' => __('Apple iPad Buzz'),
			'href' => __('http://www.appleipadbuzz.com')
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_technology',
			'id'     => 'trn_kindlebuzz',
			'title' => __('Kindle Buzz'),
			'href' => __('http://www.kindlebuzz.com')
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_technology',
			'id'     => 'trn_mobile',
			'title' => __('Mobile Questions'),
			'href' => __('http://www.mobilequestions.com')
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_technology',
			'id'     => 'trn_notebooks',
			'title' => __('Notebook Questions'),
			'href' => __('http://www.notebookquestions.com')
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_technology',
			'id'     => 'trn_tabletpceducation',
			'title' => __('Tablet PC Education'),
			'href' => __('http://www.tabletpceducation.com')
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_technology',
			'id'     => 'trn_tabletpcquestions',
			'title' => __('Tablet PC Questions'),
			'href' => __('http://www.tabletpcquestions.com')
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_technology',
			'id'     => 'trn_techpcquestions',
			'title' => __('Technology Questions'),
			'href' => __('http://www.technologyquestions.com')
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_technology',
			'id'     => 'trn_linux',
			'title' => __('Tux Reports'),
			'href' => __('http://www.tuxreports.com')
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_technology',
			'id'     => 'trn_tech_aggregate',
			'title' => __('Tux Reports Tech'),
			'href' => __('http://www.tuxreportstech.com')
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_technology',
			'id'     => 'trn_tech_win',
			'title' => __('What Is New'),
			'href' => __('http://www.whatisnew.com')
		));

	// Add sub menu link "Unemployed"
	$wp_admin_bar->add_menu( array(
		'parent' => 'trn_link',
		'id'     => 'trn_unemployed',
		'title' => __('Unemployed Buzz'),
		'href' => __('http://www.unemployedbuzz.com')
	));

	// Add sub menu link "Staff"
	$wp_admin_bar->add_menu( array(
		'parent' => 'trn_link',
		'id'     => 'trn_staff',
		'title' => __('Staff'),
		'href' => __('http://www.tuxreportsdebates.com'),
		'meta'   => array(
			'class' => 'st_menu_download')
	));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_staff',
			'id'     => 'trn_EMH',
			'title' => __('Elizabeth M. Heiny'),
			'href' => __('http://www.eheiny.com')
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_staff',
			'id'     => 'trn_layne',
			'title' => __('Layne P. Heiny'),
			'href' => __('http://www.layneheiny.com')
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_staff',
			'id'     => 'trn_lora',
			'title' => __('Lora J. Heiny'),
			'href' => __('http://www.loraheiny.com')
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_staff',
			'id'     => 'trn_loren',
			'title' => __( 'Loren C. Heiny'),
			'href' => __('http://www.lorenheiny.com')
		));

		$wp_admin_bar->add_menu( array(
			'parent' => 'trn_staff',
			'id'     => 'trn_robert',
			'title' => __('Robert W. Heiny'),
			'href' => __('http://www.robertheiny.com')
		));



}
add_action('wp_before_admin_bar_render', 'add_trn_admin_bar_link', 1);


?>