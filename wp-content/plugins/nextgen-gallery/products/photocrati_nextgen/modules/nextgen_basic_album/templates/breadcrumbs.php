<header class="entry-header">
    <div class="entry-meta">
        <h1 class="entry-title"><?php the_title(); ?></h1>
        <ul id="breadcrumbs">
            <li class="ngg-breadcrumb"><a href="<?php echo get_site_url(); ?>" title="<?php echo 'Trang chủ'; ?>"><?php echo 'Trang chủ'; ?></a> <span class="ngg-breadcrumb-divisor"><?php echo $divisor; ?></span> </li>
            <?php
            $end = end($breadcrumbs);
            reset($breadcrumbs);
            foreach ($breadcrumbs as $crumb) { ?>
                <li class="ngg-breadcrumb">
                    <?php if (!is_null($crumb['url'])) { ?>
                        <a href="<?php echo $crumb['url']; ?>"><?php esc_html_e($crumb['name']); ?></a>
                    <?php } else { ?>
                        <?php esc_html_e($crumb['name']); ?>
                    <?php } ?>
                    <?php if ($crumb !== $end) { ?>
                        <span class="ngg-breadcrumb-divisor"><?php echo $divisor; ?></span>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
    </div><!-- .entry-meta -->
</header><!-- .entry-header -->