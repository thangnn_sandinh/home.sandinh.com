msgid ""
msgstr ""
"Project-Id-Version: Captcha Plus\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-12-16 14:02+0300\n"
"PO-Revision-Date: 2015-12-16 14:02+0300\n"
"Last-Translator: BestWebSoft team <wp@bestwebsoft.com>\n"
"Language-Team: bestwebsoft.com <http://support.bestwebsoft.com>\n"
"Language: ru_RU\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Generator: Poedit 1.5.4\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Poedit-SearchPath-0: .\n"

#: captcha.php:33 captcha.php:491
msgid "Captcha Settings"
msgstr "Настройки Captcha"

#: captcha.php:33
msgid "Captcha"
msgstr "Captcha"

#: captcha.php:184
msgid "Please enter a CAPTCHA value."
msgstr "Пожалуйста, заполните CAPTCHA"

#: captcha.php:185
msgid "Please enter a valid CAPTCHA value."
msgstr "Пожалуйста, введите корректное значение CAPTCHA."

#: captcha.php:186
msgid "Time limit is exhausted. Please enter CAPTCHA value again."
msgstr "Лимит Времени истёк. Пожалуйста, введите значение для CAPTCHA опять."

#: captcha.php:187
msgid "You are in the white list"
msgstr "Вы в белом списке"

#: captcha.php:368
msgid "Login form"
msgstr "Форма логина"

#: captcha.php:369 includes/pro_banners.php:48
msgid "Registration form"
msgstr "Форма регистрации"

#: captcha.php:370
msgid "Reset Password form"
msgstr "Форма восстановления пароля"

#: captcha.php:371 includes/pro_banners.php:49
msgid "Comments form"
msgstr "Форма комментариев"

#: captcha.php:374
msgid "in Comments form for registered users"
msgstr "в форме комментариев WordPress для зарегистрированных пользователей"

#: captcha.php:378
msgid "Plus (&#43;)"
msgstr "Плюс (&#43;)"

#: captcha.php:378
msgid "Plus"
msgstr "Плюс"

#: captcha.php:379
msgid "Minus (&minus;)"
msgstr "Минус (&minus;)"

#: captcha.php:379
msgid "Minus"
msgstr "Минус"

#: captcha.php:380
msgid "Multiplication (&times;)"
msgstr "Умножение (&times;)"

#: captcha.php:380
msgid "Multiply"
msgstr "Умножить"

#: captcha.php:385
msgid "Numbers"
msgstr "Цифры"

#: captcha.php:386
msgid "Words"
msgstr "Слова"

#: captcha.php:387
msgid "Images"
msgstr "Изображения"

#: captcha.php:455
msgid ""
"Please select one item in the block Arithmetic and Complexity for CAPTCHA"
msgstr ""
"Пожалуйста, выберите 1 пункт из блока Арифметические дейтсвия и Сложность "
"для CAPTCHA"

#: captcha.php:458
msgid "Please select one item in the block Enable image packages"
msgstr "Пожалуйста, выберите 1 пункт из блока Включить пакеты изображений"

#: captcha.php:464
msgid "Settings saved."
msgstr "Настройки сохранены."

#: captcha.php:476
msgid "All plugin settings were restored."
msgstr "Все настройки плагина восстановлены."

#: captcha.php:493
msgid "How to Use Step-by-step Instruction"
msgstr "Инструкция по использованию"

#: captcha.php:496
msgid "Basic"
msgstr "Базовые"

#: captcha.php:497
msgid "Advanced"
msgstr "Расширенные"

#: captcha.php:498
msgid "Whitelist"
msgstr "Белый список"

#: captcha.php:499
msgid "Go PRO"
msgstr "Перейти на PRO-версию"

#: captcha.php:514 captcha.php:517 includes/pro_banners.php:36
#: includes/pro_banners.php:44 includes/pro_banners.php:47
msgid "Enable CAPTCHA for"
msgstr "Отображать CAPTCHA для"

#: captcha.php:518
msgid "WordPress default"
msgstr "стандартных форм Wordpress"

#: captcha.php:524
msgid "This option is available only for main blog"
msgstr "Эта опция доступна только для главного блога"

#: captcha.php:539
msgid "Plugins"
msgstr "форм плагинов"

#: captcha.php:546 captcha.php:552
msgid "You should"
msgstr "Вы должны"

#: captcha.php:546 includes/whitelist.php:399
msgid "activate"
msgstr "активировать"

#: captcha.php:546
msgid "for network"
msgstr "для сети"

#: captcha.php:546 captcha.php:554
msgid "to use this functionality"
msgstr "чтобы использовать этот функционал"

#: captcha.php:553
msgid "download"
msgstr "скачать"

#: captcha.php:566
msgid "If you would like to add Captcha to a custom form, please see"
msgstr "Если вы хотите добавить Captcha в кастомную форму, смотрите"

#: captcha.php:575
msgid "Hide CAPTCHA"
msgstr "Скрыть CAPTCHA"

#: captcha.php:582
msgid "Title"
msgstr "Заголовок"

#: captcha.php:586
msgid "Required symbol"
msgstr "Символ для отображения обязательных полей"

#: captcha.php:592
msgid "Show \"Reload\" button"
msgstr "Показать кнопку \"Перезагрузить\""

#: captcha.php:598
msgid "Arithmetic actions"
msgstr "Арифметические действия"

#: captcha.php:601
msgid "Arithmetic actions for CAPTCHA"
msgstr "Арифметические действия для CAPTCHA"

#: captcha.php:613 captcha.php:615
msgid "Complexity"
msgstr "Уровень сложности"

#: captcha.php:630
msgid "Enable image packages"
msgstr "Включить пакеты изображений"

#: captcha.php:640
msgid "Enlarge images on mouseover"
msgstr "Увеличивать изображения при наведении мышью"

#: captcha.php:651
msgid "Enable time limit"
msgstr "Включить ограничение по времени"

#: captcha.php:657
msgid "Set time limit"
msgstr "Установить ограничение по времени"

#: captcha.php:660
msgid "seconds"
msgstr "секунды"

#: captcha.php:666
msgid "Notification messages"
msgstr "Сообщения"

#: captcha.php:668 captcha.php:961 captcha.php:967 captcha.php:1163
#: captcha.php:1179
msgid "Error"
msgstr "Ошибка"

#: captcha.php:669
msgid "If CAPTCHA field is empty"
msgstr "Если CAPTCHA не заполнена"

#: captcha.php:670
msgid "If CAPTCHA is incorrect"
msgstr "Если CAPTCHA заполнена некорректно"

#: captcha.php:671
msgid "If time limit is exhausted"
msgstr "Если лимит времени исчерпан"

#: captcha.php:672
msgid "Info"
msgstr "Информационные"

#: captcha.php:673
msgid ""
"If the user IP is added to the whitelist (this message will be displayed "
"instead of CAPTCHA)."
msgstr ""
"Если IP в белом списоке, то указанное сообщение будет отображаться вместо "
"капчи."

#: captcha.php:679
msgid "Save Changes"
msgstr "Сохранить изменения"

#: captcha.php:818 captcha.php:825 captcha.php:840 captcha.php:842
#: captcha.php:850 captcha.php:1054 captcha.php:1062 captcha.php:1078
#: captcha.php:1082 captcha.php:1091 captcha.php:1113 captcha.php:1123
#: captcha.php:1138 captcha.php:1141 captcha.php:1149
msgid "ERROR"
msgstr "Ошибка"

#: captcha.php:961 captcha.php:967 captcha.php:1163 captcha.php:1179
msgid "Click the BACK button on your browser, and try again."
msgstr "Нажмите НАЗАД в браузере и попробуйте еще раз."

#: captcha.php:1394
msgid "zero"
msgstr "ноль"

#: captcha.php:1395 captcha.php:1780
msgid "one"
msgstr "один"

#: captcha.php:1396
msgid "two"
msgstr "два"

#: captcha.php:1397
msgid "three"
msgstr "три"

#: captcha.php:1398
msgid "four"
msgstr "четыре"

#: captcha.php:1399
msgid "five"
msgstr "пять"

#: captcha.php:1400 captcha.php:1780
msgid "six"
msgstr "шесть"

#: captcha.php:1401 captcha.php:1768 captcha.php:1774
msgid "seven"
msgstr "семь"

#: captcha.php:1402 captcha.php:1771
msgid "eight"
msgstr "восемь"

#: captcha.php:1403
msgid "nine"
msgstr "девять"

#: captcha.php:1406
msgid "eleven"
msgstr "одиннадцать"

#: captcha.php:1407
msgid "twelve"
msgstr "двенадцать"

#: captcha.php:1408
msgid "thirteen"
msgstr "тринадцать"

#: captcha.php:1409
msgid "fourteen"
msgstr "четырнадцать"

#: captcha.php:1410
msgid "fifteen"
msgstr "пятнадцать"

#: captcha.php:1411
msgid "sixteen"
msgstr "шестнадцать"

#: captcha.php:1412
msgid "seventeen"
msgstr "семнадцать"

#: captcha.php:1413
msgid "eighteen"
msgstr "восемнадцать"

#: captcha.php:1414
msgid "nineteen"
msgstr "девятнадцать"

#: captcha.php:1417
msgid "ten"
msgstr "десять"

#: captcha.php:1418
msgid "twenty"
msgstr "двадцать"

#: captcha.php:1419
msgid "thirty"
msgstr "тридцать"

#: captcha.php:1420
msgid "forty"
msgstr "сорок"

#: captcha.php:1421
msgid "fifty"
msgstr "пятьдесят"

#: captcha.php:1422
msgid "sixty"
msgstr "шестьдесят"

#: captcha.php:1423
msgid "seventy"
msgstr "семьдесят"

#: captcha.php:1424
msgid "eighty"
msgstr "восемьдесят"

#: captcha.php:1425
msgid "ninety"
msgstr "девяносто"

#: captcha.php:1434
msgid "and"
msgstr "и"

#: captcha.php:1500
msgid "Encryption password is not set"
msgstr "Пароль для шифрования не установлен"

#: captcha.php:1520
msgid "Decryption password is not set"
msgstr "Пароль для дешифровки не установлен"

#: captcha.php:1867 captcha.php:1880
msgid "Settings"
msgstr "Настройки"

#: captcha.php:1881
msgid "FAQ"
msgstr "FAQ"

#: captcha.php:1882
msgid "Support"
msgstr "Поддержка"

#: captcha.php:1912
#, php-format
msgid "Try New %s: with Pictures Now!"
msgstr "Попробуйте новую %s: теперь с изображениями!"

#: includes/whitelist.php:70
msgid "For IP addresses from the whitelist CAPTCHA will not be displayed"
msgstr "Для IP-адресов из белого списка CAPTCHA не будет отображаться"

#: includes/whitelist.php:82
#, php-format
msgid ""
"With this option, CAPTCHA will not be displayed for IP-addresses from the "
"whitelist of %s"
msgstr ""
"Если опция включена, CAPTCHA не будет отображаться для IP-адресов из белого "
"списка плагина %s"

#: includes/whitelist.php:93 includes/whitelist.php:129
msgid "Add IP to whitelist"
msgstr "Добавить IP в белый список"

#: includes/whitelist.php:96
msgid "Load IP to whitelist"
msgstr "Загрузить  IP в белый список"

#: includes/whitelist.php:99
#, php-format
msgid ""
"By click on this button, all IP-addresses from the whitelist of %s will be "
"loaded to the whitelist of %s"
msgstr ""
"При нажатии на эту кнопку, все IP-адреса из белого списка плагина %s будут "
"загружены в белый список плагина %s"

#: includes/whitelist.php:108
msgid "Save changes"
msgstr "Сохранить изменения"

#: includes/whitelist.php:125
msgid "My IP"
msgstr "Мой IP"

#: includes/whitelist.php:133 includes/pro_banners.php:91
msgid "Allowed formats:"
msgstr "Доступные форматы:"

#: includes/whitelist.php:134
msgid "Allowed diapason:"
msgstr "Доступные диапазоны:"

#: includes/whitelist.php:140
msgid "Search IP"
msgstr "Искать IP"

#: includes/whitelist.php:172
msgid "Nothing found"
msgstr "Ничего не найдено"

#: includes/whitelist.php:172
msgid "No IP in whitelist"
msgstr "Список IP пустой "

#: includes/whitelist.php:179
msgid "IP address"
msgstr "IP-адрес"

#: includes/whitelist.php:180
msgid "Date added"
msgstr "Дата добавления"

#: includes/whitelist.php:229 includes/whitelist.php:238
msgid "Remove from whitelist"
msgstr "Удалить"

#: includes/whitelist.php:307
msgid "IP added to the whitelist successfully"
msgstr "IP успешно добавлен в белый список"

#: includes/whitelist.php:309 includes/whitelist.php:331
#: includes/whitelist.php:343
msgid "Some errors occured"
msgstr "Возникли некоторые ошибки"

#: includes/whitelist.php:311
msgid "IP is already in the whitelist"
msgstr "IP уже в белом списке"

#: includes/whitelist.php:314
msgid "Invalid IP. See allowed formats"
msgstr "Неверный IP. Смотите доступные форматы"

#: includes/whitelist.php:325 includes/whitelist.php:337
#, php-format
msgid "One IP was deleted successfully"
msgid_plural "%s IPs were deleted successfully"
msgstr[0] "%s IP был успешно удалён"
msgstr[1] "%s IP были успешно удалены"
msgstr[2] "%s IP были успешно удалены"

#: includes/whitelist.php:346
msgid "You have not entered any value"
msgstr "Вы не ввели ни какой IP"

#: includes/whitelist.php:349
msgid "You have not entered any value in to the search form"
msgstr "Вы не ввели никакого значения в форму поиска"

#: includes/whitelist.php:351
msgid "Search results for"
msgstr "Результаты поиска для"

#: includes/whitelist.php:365
msgid "IP-address(es) successfully copied to the whitelist"
msgstr "IP-адрес(а) успешно скопированы в белый список"

#: includes/whitelist.php:390
msgid "use"
msgstr "использовать"

#: includes/whitelist.php:390
msgid "whitelist of"
msgstr "Белый список плагина"

#: includes/whitelist.php:398 includes/whitelist.php:406
#, php-format
msgid "use whitelist of %s"
msgstr "использовать белый список плагина %s"

#: includes/whitelist.php:399
#, php-format
msgid "you should %s to use this functionality"
msgstr "Вам необходимо %s для того, чтобы использовать этот функционал"

#: includes/whitelist.php:407
#, php-format
msgid "you should instal %s to use this functionality"
msgstr ""
"Вам необходимо установить %s для того, чтобы использовать этот функционал"

#: includes/pro_banners.php:17
msgid "This options is available in Pro version of plugin"
msgstr "Эта опция доступна только для Pro-версии плагина"

#: includes/pro_banners.php:19
msgid "Close"
msgstr "Закрыть"

#: includes/pro_banners.php:24
msgid "Unlock premium options by upgrading to Pro version"
msgstr "Активируйте премиум опции обновившись до Pro версии "

#: includes/pro_banners.php:25
msgid "Learn More"
msgstr "Подробнее"

#: includes/pro_banners.php:50
msgid "\"Create a Group\" form"
msgstr "Форма \"Создать группу\""

#: includes/pro_banners.php:56
msgid "Enable CAPTCHA"
msgstr "Отображать CAPTCHA"

#: includes/pro_banners.php:64
msgid "If you upgrade to Pro version all your settings will be saved."
msgstr "При установке Pro версии плагина, все ваши настройки сохраняются."

#: includes/pro_banners.php:75
msgid "Use several packages at the same time"
msgstr "Использовать несколько пакетов изображений одновременно"

#: includes/pro_banners.php:78
msgid ""
"If this option is enabled, CAPTCHA will be use pictures from different "
"packages at the same time"
msgstr ""
"Если эта опция включена, CAPTCHA будет использовать изображения из "
"нескольких пакетов одновременно"

#: includes/pro_banners.php:89
msgid "Reason"
msgstr "Причина"

#: includes/pro_banners.php:92
msgid "Allowed separators for IPs: a comma"
msgstr "Доступные разделители для причин: запятая"

#: includes/pro_banners.php:92 includes/pro_banners.php:93
msgid "semicolon"
msgstr "точка с запятой"

#: includes/pro_banners.php:92
msgid "ordinary space, tab, new line or carriage return"
msgstr "обычный пробел, табуляция или новая строка"

#: includes/pro_banners.php:93
msgid "Allowed separators for reasons: a comma"
msgstr "Доступные разделители для причин: запятая"

#: includes/pro_banners.php:93
msgid "tab, new line or carriage return"
msgstr "табуляция или новая строка"

#: includes/package_loader.php:27 includes/package_loader.php:139
msgid ""
"Can not load images in to the \"uploads\" folder. Please, check your "
"permissions."
msgstr ""
"Невозможно загрузить изображения в папку \"uploads\". Пожалуйста, проверьте "
"свои права."

#: includes/package_loader.php:39
msgid "File packages.txt not found. Package not saved."
msgstr "Файл  packages.txt не найден. Пакет изображений не сохранён."

#: includes/package_loader.php:45
msgid "Can not read data from packages.txt. Packages not saved."
msgstr ""
"Невозможно считать данные из пакета изображений. Пакет изображений не "
"сохранен."

#: includes/package_loader.php:167
msgid "Package successfully loaded."
msgstr "Пакет изображений успешно сохранён."

#~ msgid "Notice:"
#~ msgstr "Обратите внимание:"

#~ msgid ""
#~ "Option to display captcha for Contact Form 7 is enabled. For correct "
#~ "work, please, dont forget to add the BWS CAPTCHA block to Contact Form 7 "
#~ "to the needed form (see"
#~ msgstr ""
#~ "Опция отображения CAPTCHA для Contact Form 7 включена, не забудьте "
#~ "добавить блок BWS CAPTCHA в нужной форме Contact Form 7 (см. "

#~ msgid "update"
#~ msgstr "обновите"

#~ msgid "at least up to v3.4 to use this functionality"
#~ msgstr ""
#~ "как миниму до версии 3.4 для того, чтобы использовать этот функционал"

#~ msgid "BWS CAPTCHA"
#~ msgstr "BWS CAPTCHA"

#~ msgid "Name"
#~ msgstr "Имя"

#~ msgid "Copy this code and paste it into the form left."
#~ msgstr "Скопируйте этот код и вставьте его в форму слева."

#~ msgid "Insert Tag"
#~ msgstr "Вставить тэг"

#~ msgid "Network"
#~ msgstr "Сетевые"

#~ msgid "This option will replace all current settings on separate sites."
#~ msgstr "Эта настройка заменит все текущие настройки отдельных сайтов."

#~ msgid ""
#~ "It is prohibited to change Captcha Pro settings on this site in the "
#~ "Captcha Pro network settings."
#~ msgstr ""
#~ "Запрещается изменять настройки Captcha Pro на этом сайте в сетевых "
#~ "настройках Captcha Pro."

#~ msgid ""
#~ "It is prohibited to view Captcha Pro settings on this site in the Captcha "
#~ "Pro network settings."
#~ msgstr ""
#~ "Запрещается просматривать настройки Captcha Pro на этом сайте в сетевых "
#~ "настройках Captcha Pro."

#~ msgid "Apply network settings"
#~ msgstr "Применить сетевые настройки"

#~ msgid "Apply to all sites and use by default"
#~ msgstr "Применить для всех сайтов и использовать по умолчанию"

#~ msgid "All current settings on separate sites will be replaced"
#~ msgstr "Все текущие настройки в отдельных сайтах будут заменены"

#~ msgid "Allow to change the settings on separate websites"
#~ msgstr "Разрешить изменять настройки на отдельных сайтах"

#~ msgid "Allow to view the settings on separate websites"
#~ msgstr "Разрешить просмотр настроек на отдельных сайтах"

#~ msgid "By default"
#~ msgstr "По умолчанию"

#~ msgid "Settings will be applied to newly added websites by default"
#~ msgstr ""
#~ "настройки будут использоваться по умолчанию для вновь добавленных сайтов"

#~ msgid "Do not apply"
#~ msgstr "Не применять"

#~ msgid "Change the settings on separate sites of the multisite only"
#~ msgstr "изменять настройки только на отдельных сайтах"

#~ msgid "Buddypress Registration form"
#~ msgstr "Форма регистрации плагина Buddypress"

#~ msgid "Buddypress Comments form"
#~ msgstr "Форма комментариев плагина Buddypress"

#~ msgid "Buddypress comments form"
#~ msgstr "Форма комментариев плагина Buddypress"

#~ msgid "Buddypress \"Create a Group\" form"
#~ msgstr "Форма \"Создать группу\" плагина Buddypress"

#~ msgid "Set time limit values (in seconds)"
#~ msgstr "Установить время ограничения (в секундах)"

#~ msgid "Contact Form by BestWebSoft"
#~ msgstr "Форма плагина Contact Form by BestWebSoft"

#~ msgid "Subscriber by BestWebSoft form"
#~ msgstr "форма плагина Subscriber by BestWebSoft"

#~ msgid "Contact Form 7"
#~ msgstr "Форма плагина  Contact Form 7"

#~ msgid "Buddypress Registration form "
#~ msgstr "Форма регистрации плагина Buddypress"

#~ msgid "Another Forms"
#~ msgstr "Другие формы"

#~ msgid "Errors"
#~ msgstr "При ошибках"

#~ msgid "Allowed separators: a comma"
#~ msgstr "Доступные разделители для IP-адресов: запятая"

#~ msgid "Save IP to whitelist"
#~ msgstr "Сохранить IP в белый список"

#~ msgid "Range from"
#~ msgstr "Диапазон от"

#~ msgid "Range to"
#~ msgstr "Диапазон до"

#~ msgid "Range"
#~ msgstr "Диапазон"

#~ msgid "not saved to the whitelist."
#~ msgstr "не сохранён в белый список."

#~ msgid "invalid format. See allowed formats."
#~ msgstr "неверный формат. Смотите доступные форматы"

#~ msgid "IP %s was deleted from whitelist successfully"
#~ msgstr "IP %s был успешно удалён"

#~ msgid "Diapason"
#~ msgstr "Диапазон"

#~ msgid "not saved in database."
#~ msgstr "не сохранён в базу данных."

#~ msgid "is allready in database"
#~ msgstr "уже в базе данных"

#~ msgid ""
#~ "The plugin's settings have been changed. In order to save them please "
#~ "don't forget to click the 'Save Changes' button."
#~ msgstr ""
#~ "Настройки плагина были изменены. Для того, чтобы сохранить их, "
#~ "пожалуйста, не забудьте нажать кнопку 'Сохранить изменения'."

#~ msgid "Activate"
#~ msgstr "Активировать"

#~ msgid "Download"
#~ msgstr "Скачать"

#~ msgid "at least up to v3.4 so that Сaptcha could work correctly with it"
#~ msgstr "как минимум до версии 3.4, чтобы Сaptcha корректно с ним работала"

#~ msgid "Title for CAPTCHA in the form"
#~ msgstr "Заголовок для CAPTCHA в форме"

#~ msgid "Error messages"
#~ msgstr "Сообщения об ошибке"

#~ msgid "Display for whitelisted IP"
#~ msgstr "Отображать для IP в белом списке"

#~ msgid "CAPTCHA complexity level"
#~ msgstr "Уровень сложности CAPTCHA"

#~ msgid "activate Buddypress"
#~ msgstr "активировать Buddypress"

#~ msgid "download Buddypress"
#~ msgstr "установить Buddypress"

#~ msgid "Error:"
#~ msgstr "Ошибка:"

#~ msgid "Activate contact form"
#~ msgstr "Активировать контакную форму"

#~ msgid "Download contact form"
#~ msgstr "Скачать контактную форму"

#~ msgid "Activate Subscriber"
#~ msgstr "Активировать плагин Subscriber"

#~ msgid "Download Subscriber"
#~ msgstr "Загрузить плагин Subscriber"

#~ msgid "Diapason till"
#~ msgstr "Диапазон до"

#~ msgid "Input field settings"
#~ msgstr "Настройки поля ввода"

#~ msgid "optional"
#~ msgstr "необязательно"

#~ msgid "powered by"
#~ msgstr "разработано компанией"

#~ msgid "Subscriber"
#~ msgstr "Subscriber"

#~ msgid "requires"
#~ msgstr "требует"

#~ msgid ""
#~ "or higher, that is why it has been deactivated! Please upgrade WordPress "
#~ "and try again."
#~ msgstr ""
#~ "или выше, поэтому плагин был деактивирован! Обновите WordPress и "
#~ "повторите попытку."

#~ msgid "Back to the WordPress"
#~ msgstr "Назад к WordPress"

#~ msgid ""
#~ "Something went wrong. Try again later. If the error will appear again, "
#~ "please, contact us <a href=http://support.bestwebsoft.com>BestWebSoft</"
#~ "a>. We are sorry for inconvenience."
#~ msgstr ""
#~ "Что-то пошло не так. Попробуйте еще раз позже. Если ошибка появится "
#~ "снова, пожалуйста, свяжитесь с нами <a href=http://support.bestwebsoft."
#~ "com>BestWebSoft</a>. Приносим извинения за неудобства."

#~ msgid "Wrong license key!"
#~ msgstr "Неправильный лицензионный ключ!"

#~ msgid "This license key is bind to another site"
#~ msgstr "Этот лицензионный ключ привязан к другому сайту"

#~ msgid ""
#~ "This license key is valid, but Your license has expired. If you want to "
#~ "update our plugin in future, you should extend the license."
#~ msgstr ""
#~ "Лицензионный ключ принят, но срок действия лицензии истек. Если вы хотите "
#~ "обновлять плагин в будущем, вы должны продлить лицензию."

#~ msgid "Unfortunately, you have exceeded the number of available tries."
#~ msgstr "К сожалению, вы превысили количество доступных попыток."

#~ msgid "The license key is valid. Your license will expire on"
#~ msgstr "Лицензионный ключ принят. Ваша лицензия истечет"

#~ msgid "The license key is valid."
#~ msgstr "Лицензионный ключ принят."

#~ msgid "Please, enter your license key"
#~ msgstr "Пожалуйста, введите Ваш лицензионный ключ"

#~ msgid ""
#~ "If needed you can check if the license key is correct or reenter it in "
#~ "the field below. You can find your license key on your personal page - "
#~ "Client area - on our website"
#~ msgstr ""
#~ "При необходимости вы можете проверить валидность лицензионного ключа или "
#~ "повторно ввести его в поле ниже. Вы можете найти ваш лицензионный ключ на "
#~ "своей личной странице - Client area - на нашем сайте"

#~ msgid ""
#~ "(your username is the email you specify when purchasing the product). If "
#~ "necessary, please submit \"Lost your password?\" request."
#~ msgstr ""
#~ "(ваш пользователь это емейл, который вы указывали при покупке продукта). "
#~ "При необходимости нажмите \"Lost your password?\"."

#~ msgid "Check license key"
#~ msgstr "Проверка лицензионного ключа"

#~ msgid "If you enjoy our plugin, please give it 5 stars on WordPress"
#~ msgstr "Если вам понравился плагин, то поставьте нам 5 звезд на WordPress"

#~ msgid "Rate the plugin"
#~ msgstr "Оценить плагин"

#~ msgid "If there is something wrong about it, please contact us"
#~ msgstr "Если у вас есть какие-то вопросы, обращайтесь"

#~ msgid "ERROR:"
#~ msgstr "ОШИБКА:"

#~ msgid ""
#~ "WARNING: We have noticed illegal use of our plugin by you. We strongly "
#~ "recommend you to solve the problem within 24 hours, otherwise the plugin "
#~ "will be deactivated. Please go to your personal"
#~ msgstr ""
#~ "ВНИМАНИЕ: Было замечено незаконное использование вами нашего плагина. Мы "
#~ "настоятельно рекомендуем Вам, решить данную проблему в течение 24 часов, "
#~ "в противном случае плагин будет деактивирован. Пожалуйста, перейдите на "
#~ "вашу личную"

#~ msgid ""
#~ "(your username is the email you specify when purchasing the product), "
#~ "where you can make the necessary changes."
#~ msgstr ""
#~ "(ваш пользователь это емейл, который вы указывали при покупке продукта), "
#~ "где вы можете сделать необходимые изменения."

#~ msgid ""
#~ "Your license has expired. To continue getting top-priority support and "
#~ "plugin updates you should extend it in your"
#~ msgstr ""
#~ "Время использования вашей лицензии истекло. Для обновления вашего плагина "
#~ "вам нужно продлить лицензию перейдя на вашей"

#~ msgid "You license for"
#~ msgstr "Ваша лицензия для"

#~ msgid "expires on"
#~ msgstr "истекает"

#~ msgid ""
#~ "and you won't be granted TOP-PRIORITY SUPPORT or UPDATES. To extend your "
#~ "license please go to your"
#~ msgstr ""
#~ "и у вас не будет возможности обновляться или иметь приоритетную "
#~ "техподдержку. Для продления лицензии, пожалуйста, перейдите на вашу"

#~ msgid "where you'll be able to do that once this period expires."
#~ msgstr "где появится такая возможность по истечении данного срока."

#~ msgid "If you do not fill the CAPTCHA field"
#~ msgstr "Если CAPTCHA не заполнена"

#~ msgid "Invalid CAPTCHA value."
#~ msgstr "Неправильное значение CAPTCHA."

#~ msgid "Please fill the form"
#~ msgstr "Пожалуйста, заполните форму."
