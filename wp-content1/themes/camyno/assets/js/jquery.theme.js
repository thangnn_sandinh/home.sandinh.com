;(function($) {
   "use strict";

   /* ===================================================================== *
    * Responsive Sidebars
    * ===================================================================== */

   var CVResponsiveSidebar = function( element ) {
      this.$element = $(element);
      this.init();
      return this;
   };

   CVResponsiveSidebar.prototype = {

      init: function() {

         var self = this;

         // Make sure responsiveness has been enabled
         if ( ! ( $('body').hasClass('responsive') && $('body').hasClass('sidebar-behavior-normal') ) ) { return; }

         // All widgets in the sidebar
         self.$widgets = self.$element.find('.widget');

         // Split widgets into two columns
         self.splitWidgets();

      },

      splitWidgets: function() {
         var self = this,
             total = self.$widgets.length,
             half = ( total % 2 ) ? (total + 1) / 2 : total / 2;
         self.$widgets.slice(0,half).wrapAll('<div class="responsive-column">');
         self.$widgets.slice(half,total).wrapAll('<div class="responsive-column">');
      },

   };

   /* ===================================================================== *
    * Dropdown Menu with Mega Menu support
    * ===================================================================== */

   var CVDropMenu = function() {
      this.init();
   };

   CVDropMenu.prototype = {

      init: function() {

         var self = this;

         // Set some variables
         self.$header  = $('#header');
         self.$menu    = self.$header.find('.dropdown-menu');
         self.$targets = self.locateTargets();

         if ( ! self.$targets.length ) {
            return;
         }

         // Attach events
         self.attachEvents();

         // Reset mega menus when screen resizes
         $(window).resize( function() {
            self.$menu.find('.width-perfected').removeClass('width-perfected');
            self.$menu.find('.margin-perfected').removeClass('margin-perfected');
         });

      },

      attachEvents: function() {
         var self = this;
         this.$targets.on({
            mouseenter: function() {
               self.openSubMenu( $(this ) );
            },
            mouseleave: function() {
               self.closeSubMenu( $(this ) );
            },
         });
      },

      locateTargets: function() {
         return this.$menu.find('ul').parent();
      },

      openSubMenu: function( $target ) {

         var self = this;

         // prepare for fade in
         $target.addClass('is-active').children('ul').css('opacity', 0);

         // Fade in sub menu
         $target.children('ul').stop().animate({
            opacity: 1
         }, 250, 'swing' );

         // Check if this is a mega menu
         if ( $target.hasClass('mega-menu') ) {
            self.openMegaMenu( $target );
         }

      },

      closeSubMenu: function( $target ) {
         $target.children('ul').stop().animate({
            opacity: 0
         }, 250, 'swing', function() {
            $target.removeClass('is-active');
         } );
      },

      openMegaMenu: function( $target ) {

         var self = this;

         var $megaMenu = $target.children('.sub-menu');

         // Check if width needs to be adjusted
         if ( $target.hasClass('full-width') && ! $target.hasClass('width-perfected') ) {
            $target.children('.sub-menu').css('width', self.$header.find('.cv-user-font').outerWidth() );
            $target.addClass('width-perfected');
         }

         // make sure menu is not off canvas
         if ( ! $target.hasClass('margin-perfected') ) {
            $target.addClass('margin-perfected');
            var megaMenuMarginLeft = parseInt( $megaMenu.css('marginLeft') ),
                megaMenuOffsetLeft = $megaMenu.offset().left,
                megaMenuOffsetRight = megaMenuOffsetLeft + $megaMenu.outerWidth(),
                $container = self.$header.find('.cv-user-font'),
                containerOffsetLeft = $container.offset().left,
                containerOffsetRight = containerOffsetLeft + $container.outerWidth(),
                difference;

            /* Apply correct position */
            if ( megaMenuOffsetRight > containerOffsetRight ) {
               difference = megaMenuOffsetRight - containerOffsetRight;
               $megaMenu.css( 'marginLeft', ( megaMenuMarginLeft - difference ) + 'px' );
            }
            else if ( megaMenuOffsetLeft < containerOffsetLeft ) {
               difference = containerOffsetLeft - megaMenuOffsetLeft;
               $megaMenu.css( 'marginLeft', ( megaMenuMarginLeft + difference ) + 'px' );
            }

         }

         /* Even out height of each column */
         var $columns = $megaMenu.children().css( 'height', false );
         var columnHeights = $columns.map( function() {
            return $(this).height();
         }).get();
         $columns.css( 'height', Math.max.apply( null, columnHeights ) );

      },

   };

   /* ===================================================================== *
    * Modern inline menu
    * ===================================================================== */

   var CVInlineMenu = function() {
      this.init();
   };

   CVInlineMenu.prototype = {

      init: function() {

         var self = this;

         // Set some variables
         self.$header    = $('#header');
         self.$container = self.$header.find('.navigation-container');
         self.$subMenu   = self.locateSubMenu();

         // Align the sub menu initially
         self.alignSubMenu();

         // Update sub menu alignment when screen resizes
         $(window).resize( $.proxy( self.alignSubMenu, self ) );

         // Fade in the sub menu
         self.$subMenu.css( 'opacity', 1 );

      },

      locateSubMenu: function() {
         var self = this,
             query = '.modern-menu > li.current-menu-ancestor > ul,'
                   + '.modern-menu > li.current_page_ancestor > ul,'
                   + '.modern-menu > li.menu-item-has-children.current-menu-item > ul,'
                   + '.modern-menu > li.page_item_has_children.current_page_item > ul';
         return self.$header.find(query).eq(0);
      },

      alignSubMenu: function() {

         var self = this,
             containerWidth = self.$container.width(),
             containerOffsetLeft = self.$container.offset().left,
             subMenuWidth = 10;

         // Determine width of the container
         self.$subMenu.children().each( function() {
            subMenuWidth += $(this).outerWidth();
         });

         // Make sure the correct width is being used
         if ( subMenuWidth > containerWidth ) {
            subMenuWidth = containerWidth;
         }

         // Align the sub menu initially
         self.$subMenu.css({
            width: subMenuWidth,
            marginLeft: -subMenuWidth/2,
         });

         // Modify the submenu margins accordingly
         var subMenuOffsetLeft = self.$subMenu.offset().left,
             subMenuOffsetRight = subMenuOffsetLeft + subMenuWidth,
             containerOffsetRight = containerOffsetLeft + containerWidth,
             difference;

         if ( containerOffsetLeft > subMenuOffsetLeft ) {
            difference = ( containerOffsetLeft - subMenuOffsetLeft ) -5;
            self.$subMenu.css({
               marginLeft: (-subMenuWidth/2)+difference,
            });
         }

         else if ( containerOffsetRight < subMenuOffsetRight ) {
            difference = ( containerOffsetRight - subMenuOffsetRight ) + 5;
            self.$subMenu.css({
               marginLeft: (-subMenuWidth/2)+difference,
            });
         }

      }

   };

   /* ===================================================================== *
    * Overlay menu
    * ===================================================================== */

   var CVOverlayMenu = function() {
      this.init();
   };

   CVOverlayMenu.prototype = {

      init: function() {

         var self = this;

         self.$overlayMenu = $('#cv-overlay-menu');

         // Add the sub menu indicators
         var query = '.menu-item-has-children > a, .page_item_has_children > a';
         self.$overlayMenu.find(query).append('<span class="toggle"></span>');

         // Set sub menu heights
         self.setSubMenuHeights();

         // Attach events
         self.attachEvents();

      },

      setSubMenuHeights: function() {
         var self = this,
             query = '.menu-item-has-children > ul, .page_item_has_children > ul';
         self.$overlayMenu.find(query).each( function() {
            var $this = $(this), height = $this.find('a').length * 40;
            $this.css( 'height', height+'px' );
         });
      },

      attachEvents: function() {
         var self = this,
             query = '.menu-item-has-children > a > .toggle, .page_item_has_children > a > .toggle';
         self.$overlayMenu.find(query).click( function(e) {
            e.preventDefault();
            var $this = $(this).parent(), $li = $this.parent(), $ul = $li.parent();
            if ( $li.hasClass('is-active') ) {
               $li.removeClass('is-active');
               $ul.removeClass('submenu-open');
            }
            else {
               $li.addClass('is-active');
               $ul.addClass('submenu-open');
               $li.siblings('.is-active').each( function() {
                  $(this).removeClass('is-active');
               });
            }
            return false;
         });
      },

   };

   /* ===================================================================== *
    * After document has loaded
    * ===================================================================== */

   $(document).ready( function() {

      // Apply button class to comment forms
      $('#respond #submit').addClass('button');

      // Activate Fit Vids
      $('body').fitVids({ ignore: '.no-fitvids' });

      // Disable fixed backgrounds on mobile devices
      if ( navigator.userAgent.match(/(Android|iPod|iPhone|iPad|IEMobile|Opera Mini)/) ) {
         $('body').addClass('dixable-fixed-backgrounds');
      }

      // Refresh waypoints as container changes size
      $('#container').resize( function() { $.waypoints('refresh'); } );

      // Activate Nivo Lightbox
      $('a[rel^="lightbox"], a[href$=".jpg"], a[href$=".jpg"], a[href$=".png"], a[href$=".gif"], a[href$=".jpeg"], a[href*="youtube.com/watch"], a[href*="vimeo.com/"]').filter(':not(.no-lightbox)').nivoLightbox();

      // Add arrows element after select boxes
      var $selects = $('select:not([multiple]):not([size])');
      $selects.wrap('<div class="cv-select-box"></div>');

      // Apply the correct header scripting
      if ( document.getElementById('header') && document.getElementById('primary-navigation') ) {

         // Activate inline menu
         if ( $('#header').hasClass('has-menu-tree') ) { new CVInlineMenu(); }

         // Activate dropdown menu
         if ( $('#header').find('.dropdown-menu').length ) { new CVDropMenu(); }

      }

      // Set up blank page styling
      if ( $('body').hasClass('page-template-template-blank-php') && ! $('html').hasClass('full-page-slider-active') ) {
         var $wrapAll = $('#container > .wrap-all').addClass('v-align-middle');
         $(window).resize( function() { $wrapAll.css( 'min-height', $(window).height() ); });
         $wrapAll.css( 'min-height', $(window).height() );
         $('#body').addClass('v-align-content');
         $('#container').css('opacity', 1);
      }

      // Fade in the banner when it is loaded
      if ( document.getElementById('top-banner') ) {
         var $banner = $('#top-banner');
         if ( $banner.hasClass('is-loading') ) {
            var src = $banner.css('background-image'),
                url = src.match(/\((.*?)\)/)[1].replace(/('|")/g,''),
                img = new Image();
            img.onload = function() { $banner.removeClass('is-loading'); }
            img.src = url;
            if (img.complete) img.onload();
         }
      }

      // Activate floating back to top link
      if ( document.getElementById('cv-floating-anchor') && 800 < $(window).height() ) {

         var $anchor = $('#cv-floating-anchor');

         $(window).scroll( function() {
            var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
            if ( 300 < scrollTop ) {
               $anchor.addClass('is-visible');
            }
            else {
               $anchor.removeClass('is-visible');
            }
         });

      }

      // Superlinks
      $(document).on('click', '.cv-superlink', function(event) {
         if ( 'a' === event.target.nodeName.toLowerCase() || $(event.target).parents('a').length ) {
            return;
         }
         var $links = $('a', $(this));
         if ( ! $links.length ) {
            return;
         }

         // [0] gets the JavaScript object to make use of the native
         // click method, which simulates an authentic click event.
         $links.first()[0].click();
      });

      // Animated scrolling
      $('.animate-scroll[href^="#"], .cv-button[href^="#"], #header a[href^="#"]').on( 'click', function(e) {

         var id = '#' === $(this).attr('href') ? '#top' : $(this).attr('href'),
             offset = 0;

         // Make sure target exists
         if ( ! document.getElementById(id.replace('#','')) ) { return; }

         e.preventDefault();

         var $target = $(id), offset = $target.offset().top;

         // Check if sticky header is active
         if ( document.getElementById('header-marker') ) {
            offset -= 58;
         }

         // Check if sticky menu is active
         if ( document.getElementById('cv-sticky-nav') && $target.prevAll('#cv-sticky-nav').length ) {
            offset -= 56;
         }

         // Make sure waypoints are triggered
         offset += 2;

         // Scroll to element
         $('html, body').stop().animate({
            scrollTop: offset+'px'
         }, 1000, 'easeInOutExpo' );

      });

      // Resize the header width to match layout
      if ( document.getElementById('header') && ! $('body').hasClass('container-layout-free') ) {
         $(window).resize( function() {
            var width = $('.wrap-all').width();
            $('#header').css( 'max-width', width );
            if ( document.getElementById('primary-tools') ) {
               $('#primary-tools').css( 'width', width );
            }
         }).trigger('resize');
      }

      // Animated Entrances
      $('[data-entrance]').on( 'entrance', function() {
         var $this = $(this), entrance = $this.data('entrance'),
             delay = $this.data('delay') ? $this.data('delay') : 0;
         if ( entrance ) {
            setTimeout( function() {
               $this.addClass(entrance + ' is-visible');
               setTimeout( function() {
                  $this.removeClass(entrance + ' is-visible').removeAttr('data-entrance');
                  $this.attr('data-completed-entrance', entrance);
               }, 1000 );
            }, parseInt( delay ) );
         }
      });

      // Reset Manually Triggered Entrances
      $('[data-manual-trigger]').on( 'reset-entrance', function() {
         var $this = $(this), entrance = $this.data('completed-entrance');
         if ( entrance ) {
            $this.removeAttr('data-completed-entrance').attr('data-entrance', entrance);
         }
      });

      // Attach entrance waypoints
      $('[data-entrance]:not([data-chained]):not([data-manual-trigger])').waypoint( function() {
         var $this = $(this);
         setTimeout( function() {
            $this.trigger('entrance');
         }, 150 );
      }, { offset: '100%' });

      // Chained animated entrances
      $('[data-trigger-entrances]').waypoint( function() {
         var $this = $(this);
         setTimeout( function() {
            $this.find('[data-entrance][data-chained]').each( function() {
               $(this).trigger('entrance');
            });
         }, 150 );
      }, { offset: '100%' });

      // Launching fullscreen overlays
      $('.launch-fullscreen-overlay[data-overlay]').on( 'click', function() {
         var overlayData = $(this).data('overlay'),
             $target = $('#'+overlayData),
             triggerEvent = 'cv-launch-overlay-'+overlayData;
         $('body').addClass('cv-overlay-open no-scroll').trigger(triggerEvent);

         if ( ! $('body').hasClass('container-layout-free') ) {
            var $wrapAll = $('#container .wrap-all').eq(0),
                width = $wrapAll.outerWidth(),
                left = $wrapAll.offset().left,
                css = { width: width, left: left, };
            $target.css(css);
            $target.find('.close-button').css(css);
         }

         $target.addClass('is-active');
         setTimeout( function() {
            $target.addClass('is-open');
         }, 10 );
      });

      // When search overlay is launched
      $('body').on( 'cv-launch-overlay-cv-overlay-search', function() {
         setTimeout( function() {
            $('#cv-overlay-search').find('input[type="text"]').focus().val('');
         }, 250 );
      });

      // Closing Fullscreen Overlays
      $('.cv-fullscreen-overlay .close-button').on( 'click', function() {
         var $overlay = $(this).parent().trigger('close');
         $overlay.removeClass('is-open');
         setTimeout( function() {
            $overlay.removeClass('is-active');
            $('body').removeClass('no-scroll cv-overlay-open');
         }, 500);
      });

      $(document).on( 'keyup', function(e) {

         if ( e.keyCode !== 27 || ! $('body').hasClass('cv-overlay-open') ) {
            return;
         }

         $('.cv-fullscreen-overlay .close-button').trigger('click');

      });

      // Activate the overlay menu
      if ( document.getElementById('cv-overlay-menu') ) {

         // Activate the overlay menu
         new CVOverlayMenu();

      }

      // Activate responsive sidebars
      $('.content-section-sidebar').each( function() {
         var $this = $(this);
         if ( $this.data( 'CVResponsiveSidebar' ) ) { return; }
         $this.data( 'CVResponsiveSidebar', true );
         new CVResponsiveSidebar( this );
      });

   });

})(jQuery);