<?php
    $IP = "123.30.240.109";
    $PORT = 9000;
    $user_id = 4097; $item_id = 56490; $item_quantity = 2; $coin = 123;
    $data = [];

    $part1 = [0x90, 0x91, 0x81, 0xE7, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00];
    $data = array_merge($data, $part1);

    $data = array_merge($data, unpack("C*", pack("N", $user_id))); // user_id

    $part2 = [  0x00, 0x00, 0x00, 0x8C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x02, 0x01, 0x00, 0x53, 0xA5, 0x93, 0x4E, 0x01, 0x00, 0x00, 0x00, 0x00];
    $data = array_merge($data, $part2);

    $data = array_merge($data, unpack("C*", pack("J", $coin))); // coin
    array_push($data, 0x01); // padding or smt else
    $data = array_merge($data, unpack("C*", pack("N", $item_id))); // item_id
    $data = array_merge($data, unpack("C*", pack("N", $item_quantity))); // item_quantity

    $part3 = array_fill(0, 421, 0x00);
    $part3[35] = 0x01;
    $part3[219] = 0xFF;
    $data = array_merge($data, $part3);

    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    if ($socket === false) {
        echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
    } else {
        echo "Init OK.\n";
    }
    $result = socket_connect($socket, $IP, $PORT);
    if ($result === false) {
        echo "socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n";
    } else {
        echo "Connect OK.\n";
    }

    echo "send $item_quantity item #$item_id, $coin coin to user #$user_id \n";
    $len = count($data);
    $sd = "";
    for ($j = 0; $j < count($data); $j++) {
        $sd .= pack("C", $data[$j]);
    }
    socket_write($socket, $sd, $len);
    echo "Done!\n";

?>